CREATE TABLE users (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50),
    password VARCHAR(255),
    role VARCHAR(20) DEFAULT 'staff',
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE customers (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(50),
    father_or_spouse ENUM('F', 'S'),
    lastname VARCHAR(50),
    gender ENUM('M', 'F'),
    photo VARCHAR(250),
    identity_type VARCHAR(50),
    identity_code VARCHAR(50),
    identity_file VARCHAR(250),
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE accounts (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    account_type ENUM('direct', 'indirect') DEFAULT 'direct',
    customer_id INT(11),
    guaranteer_id INT(11),
    ref_no VARCHAR(50),
    amount DECIMAL(10, 4),
    interest DECIMAL(10, 4),
    total_weight DECIMAL(10, 4),
    net_weight DECIMAL(10, 4),
    property_value DECIMAL(10, 4),
    status VARCHAR(50) DEFAULT 'opened',
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE account_details (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    account_id INT(11),
    property_name VARCHAR(50),
    qty INT(11),
    gross_weight DECIMAL(10, 4),
    net_weight DECIMAL(10, 4),
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE receipts (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    account_id INT(11),
    pdate DATETIME NOT NULL,
    duration INT(11),
    payment_type ENUM('interest', 'due', 'settlement', 'loan', 'extra'),
    interest DECIMAL(10, 4),
    principal DECIMAL(10, 4),
    amount DECIMAL(10, 4),
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE customer_address (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    customer_id int(11),
    name VARCHAR(50),
    mobile VARCHAR(50),
    phone VARCHAR(50),
    door_no VARCHAR(50),
    street VARCHAR(150),
    area VARCHAR(150),
    village VARCHAR(150),
    city VARCHAR(150),
    pincode VARCHAR(50),
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE accounts ADD COLUMN loan_date DATETIME;

ALTER TABLE customers 
ADD COLUMN guaranteer_id INT(11) NULL DEFAULT NULL AFTER id;

CREATE TABLE profile (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100) DEFAULT '',
    subtitle VARCHAR(100) DEFAULT '',
    address VARCHAR(150) DEFAULT '',
    mobile1 VARCHAR(50) DEFAULT '',
    mobile2 VARCHAR(50) DEFAULT '',
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO profile (id) VALUES (1);

ALTER TABLE profile 
ADD COLUMN city VARCHAR(50) NULL DEFAULT NULL AFTER address;

ALTER TABLE profile 
ADD COLUMN pincode VARCHAR(50) NULL DEFAULT NULL AFTER city;

ALTER TABLE accounts 
ADD COLUMN created_by INT(11) NULL DEFAULT NULL;

ALTER TABLE accounts 
ADD COLUMN modified_by INT(11) NULL DEFAULT NULL;

ALTER TABLE customers 
ADD COLUMN created_by INT(11) NULL DEFAULT NULL;

ALTER TABLE customers 
ADD COLUMN modified_by INT(11) NULL DEFAULT NULL;

ALTER TABLE receipts 
ADD COLUMN created_by INT(11) NULL DEFAULT NULL;

ALTER TABLE receipts 
ADD COLUMN modified_by INT(11) NULL DEFAULT NULL;


CREATE TABLE account_pledge_detail (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    account_id INT(11),
    pledge_id VARCHAR(50),
    pledge_date DATETIME DEFAULT NULL,
    pledge_vendor VARCHAR (100),
    pledge_amount DECIMAL(10, 4),
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE crdr_transactions (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    transaction_date DATE DEFAULT NULL,
    transaction_type VARCHAR (100),
    transaction_title VARCHAR (100),
    transaction_amount DECIMAL(10, 4),
    transaction_remarks VARCHAR (300),
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
