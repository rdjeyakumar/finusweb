<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CrdrTransactionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CrdrTransactionsTable Test Case
 */
class CrdrTransactionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CrdrTransactionsTable
     */
    public $CrdrTransactions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.crdr_transactions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CrdrTransactions') ? [] : ['className' => CrdrTransactionsTable::class];
        $this->CrdrTransactions = TableRegistry::getTableLocator()->get('CrdrTransactions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CrdrTransactions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
