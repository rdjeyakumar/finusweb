<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AccountPledgeDetailTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AccountPledgeDetailTable Test Case
 */
class AccountPledgeDetailTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AccountPledgeDetailTable
     */
    public $AccountPledgeDetail;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.account_pledge_detail',
        'app.accounts',
        'app.pledges'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AccountPledgeDetail') ? [] : ['className' => AccountPledgeDetailTable::class];
        $this->AccountPledgeDetail = TableRegistry::getTableLocator()->get('AccountPledgeDetail', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AccountPledgeDetail);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
