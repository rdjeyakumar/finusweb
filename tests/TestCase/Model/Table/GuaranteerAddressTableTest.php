<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GuaranteerAddressTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GuaranteerAddressTable Test Case
 */
class GuaranteerAddressTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GuaranteerAddressTable
     */
    public $GuaranteerAddress;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.guaranteer_address',
        'app.customers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('GuaranteerAddress') ? [] : ['className' => GuaranteerAddressTable::class];
        $this->GuaranteerAddress = TableRegistry::getTableLocator()->get('GuaranteerAddress', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GuaranteerAddress);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
