<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CustomerAddres $customerAddres
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Customer Addres'), ['action' => 'edit', $customerAddres->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Customer Addres'), ['action' => 'delete', $customerAddres->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customerAddres->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Customer Address'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customer Addres'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="customerAddress view large-9 medium-8 columns content">
    <h3><?= h($customerAddres->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Customer') ?></th>
            <td><?= $customerAddres->has('customer') ? $this->Html->link($customerAddres->customer->id, ['controller' => 'Customers', 'action' => 'view', $customerAddres->customer->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($customerAddres->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mobile') ?></th>
            <td><?= h($customerAddres->mobile) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($customerAddres->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Door No') ?></th>
            <td><?= h($customerAddres->door_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Street') ?></th>
            <td><?= h($customerAddres->street) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Area') ?></th>
            <td><?= h($customerAddres->area) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Village') ?></th>
            <td><?= h($customerAddres->village) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City') ?></th>
            <td><?= h($customerAddres->city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pincode') ?></th>
            <td><?= h($customerAddres->pincode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($customerAddres->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($customerAddres->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($customerAddres->modified) ?></td>
        </tr>
    </table>
</div>
