<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CustomerAddres[]|\Cake\Collection\CollectionInterface $customerAddress
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Customer Addres'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="customerAddress index large-9 medium-8 columns content">
    <h3><?= __('Customer Address') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('customer_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mobile') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('door_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('street') ?></th>
                <th scope="col"><?= $this->Paginator->sort('area') ?></th>
                <th scope="col"><?= $this->Paginator->sort('village') ?></th>
                <th scope="col"><?= $this->Paginator->sort('city') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pincode') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customerAddress as $customerAddres): ?>
            <tr>
                <td><?= $this->Number->format($customerAddres->id) ?></td>
                <td><?= $customerAddres->has('customer') ? $this->Html->link($customerAddres->customer->id, ['controller' => 'Customers', 'action' => 'view', $customerAddres->customer->id]) : '' ?></td>
                <td><?= h($customerAddres->name) ?></td>
                <td><?= h($customerAddres->mobile) ?></td>
                <td><?= h($customerAddres->phone) ?></td>
                <td><?= h($customerAddres->door_no) ?></td>
                <td><?= h($customerAddres->street) ?></td>
                <td><?= h($customerAddres->area) ?></td>
                <td><?= h($customerAddres->village) ?></td>
                <td><?= h($customerAddres->city) ?></td>
                <td><?= h($customerAddres->pincode) ?></td>
                <td><?= h($customerAddres->created) ?></td>
                <td><?= h($customerAddres->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $customerAddres->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customerAddres->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customerAddres->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customerAddres->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
