<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CustomerAddres $customerAddres
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Customer Address'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="customerAddress form large-9 medium-8 columns content">
    <?= $this->Form->create($customerAddres) ?>
    <fieldset>
        <legend><?= __('Add Customer Addres') ?></legend>
        <?php
            echo $this->Form->control('customer_id', ['options' => $customers, 'empty' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('mobile');
            echo $this->Form->control('phone');
            echo $this->Form->control('door_no');
            echo $this->Form->control('street');
            echo $this->Form->control('area');
            echo $this->Form->control('village');
            echo $this->Form->control('city');
            echo $this->Form->control('pincode');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
