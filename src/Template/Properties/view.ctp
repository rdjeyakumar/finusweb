<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Property $property
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Property'), ['action' => 'edit', $property->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Property'), ['action' => 'delete', $property->id], ['confirm' => __('Are you sure you want to delete # {0}?', $property->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Properties'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Property'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="properties view large-9 medium-8 columns content">
    <h3><?= h($property->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($property->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($property->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($property->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($property->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Account Details') ?></h4>
        <?php if (!empty($property->account_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Account Id') ?></th>
                <th scope="col"><?= __('Property Id') ?></th>
                <th scope="col"><?= __('Net Weight') ?></th>
                <th scope="col"><?= __('Total Weight') ?></th>
                <th scope="col"><?= __('Property Value') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($property->account_details as $accountDetails): ?>
            <tr>
                <td><?= h($accountDetails->id) ?></td>
                <td><?= h($accountDetails->account_id) ?></td>
                <td><?= h($accountDetails->property_id) ?></td>
                <td><?= h($accountDetails->net_weight) ?></td>
                <td><?= h($accountDetails->total_weight) ?></td>
                <td><?= h($accountDetails->property_value) ?></td>
                <td><?= h($accountDetails->created) ?></td>
                <td><?= h($accountDetails->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'AccountDetails', 'action' => 'view', $accountDetails->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'AccountDetails', 'action' => 'edit', $accountDetails->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'AccountDetails', 'action' => 'delete', $accountDetails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountDetails->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
