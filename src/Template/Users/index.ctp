<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<div class="users index large-12 medium-12 columns content">
    <h3><?= __('Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                <th scope="col"><?= $this->Paginator->sort('role') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->username) ?></td>
                <td><?= h($user->role) ?></td>
                <td><?= $this->Time->format($user->created, 'dd-MMM-yyyy hh:mm a') ?></td>
                <td><?= $this->Time->format($user->modified, 'dd-MMM-yyyy hh:mm a') ?></td>
                <td class="actions">
                    <?= $this->Html->link(('<i class="fa fa-eye"></i>' . " View"), ['action' => 'view', $user->id], ['escape'=>false, 'class'=>'label']) ?>
                    <?= $this->Html->link('<i class="fa fa-pencil"></i>' . ' Edit', ['action' => 'edit', $user->id], ['escape'=>false, 'class'=>'label success']) ?> |
                    <?= $this->Form->postLink('<i class="fa fa-trash-o"></i>' . ' Delete', ['action' => 'delete', $user->id], ['escape'=>false, 'class'=>'label alert', 'confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
