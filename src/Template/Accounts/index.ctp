<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
 */
?>

<div class="accounts index large-12 medium-12 columns content">
    <h3><?= __('Accounts') ?></h3>
    <div class="filters">
        <?php echo $this->Form->create("Filter",array('class' => 'filter', 'type' => 'GET'));?>
        <div class="row">
            <div class="large-4 columns">
                <?php echo $this->Form->input("search", array('label' => 'Search', 'placeholder' => "Search...", 'value' => isset($_GET['search']) ? $_GET['search'] : ''));?>
            </div>

            <div class="large-4 columns">
                <?php echo $this->Form->input("search_by", ['label' => 'By', 'options' => [
                    '' => 'Any',
                    'ref_no' => 'Reference No',
                    'Customers.id' => 'Customer ID',
                    'firstname' => 'First name',
                    'lastname' => 'Last name',
                    'identity_code' => 'Identity Code',
                    'CustomerAddress.mobile' => 'Mobile',
                    'CustomerAddress.phone' => 'Phone',
                    'CustomerAddress.door_no' => 'Door No',
                    'CustomerAddress.street' => 'Street',
                    'CustomerAddress.area' => 'Area',
                    'CustomerAddress.village' => 'Village',
                    'CustomerAddress.City' => 'City',
                    'CustomerAddress.pincode' => 'Pin Code'
                ], 'value' => isset($_GET['search_by']) ? $_GET['search_by'] : '']);?>
            </div>
            <div class="large-2 columns">
                <?php echo $this->Form->input("status", ['label' => 'Status', 'options' => [
                    '' => 'All',
                    'opened' => 'Active',
                    'closed' => 'Closed'
                ], 'value' => isset($_GET['status']) ? $_GET['status'] : '']);?>
            </div>
            <div class="large-2 columns">
                <?php echo $this->Form->submit("Search", ['style' => 'margin-top: 18px', 'class'=>'button small info']);?>
            </div>
        </div>
        <?php echo $this->Form->end();?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th width="130"><?php echo $this->Paginator->sort('CAST(Accounts.ref_no AS UNSIGNED)', 'Ref No');?></th>
                <th width="80"><?php echo $this->Paginator->sort('Customers.id', 'Cust-ID');?></th>
                <th width="260"><?php echo $this->Paginator->sort('Customers.firstname', 'Customer');?></th>
                <th width="150">History</th>
                <th width="120"><?php echo $this->Paginator->sort('Accounts.amount', 'Amount (Rs)');?></th>
                <th width="110"><?php echo $this->Paginator->sort('Accounts.loan_date', 'Loan Date');?></th>
                <th width="100"><?php echo $this->Paginator->sort('Accounts.status', 'Status');?></th>
                <th width="280">Actions</th>
                <th width="130"><?php echo $this->Paginator->sort('CreatedUser.username', 'Created');?></th>
                <th width="130"><?php echo $this->Paginator->sort('ModifiedUser.username', 'Modified');?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($accounts as $account): ?>
            <tr style="<?=$account->is_oldest ? 'background: #FBD0D5;' : ''?>">
                <td>
                    <a href="accounts/ajaxview/<?=$account->id?>" data-reveal-id="accountModal" data-reveal-ajax="true">
                        <?php if($account->is_oldest) : ?>
                            <b style="color: red"><?= $account->ref_no;?></b> (<?=$account->duration?> days)
                        <?php else : ?>
                            <?= $account->ref_no;?>
                        <?php endif; ?>
                    </a>
                </td>
                <?php if(!$account->customer) : ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                <?php else : ?>
                    <td><?=h($account->customer->id)?></td>
                    <td>
                        <?= $this->Html->link(($account->customer->min_detail), ['controller' => 'Customers', 'action' => 'loadview', $account->customer->id], ['escape'=>false, 'class'=>'', 'data-reveal-id' => 'customerModal', 'data-reveal-ajax' => 'true']) ?>
                    </td>
                    <td>
                        <?php 
                            $histories = $account->customer->getCustomerHistory();
                            $totalHis = 0;
                            $oldestHis = 0;
                            foreach ($histories as $key => $history) {
                                if($history->is_oldest) {
                                    $oldestHis++;
                                }
                                $totalHis++;
                            }
                        ?>
                        <?php if($oldestHis > 0) : ?>
                            <span class="label alert"><?=$oldestHis?> of <?=$totalHis?> Account(s)</span>
                        <?php else : ?>
                            <span class="label info"><?=$totalHis?> Account(s)</span>
                        <?php endif;?>
                    </td>
                <?php endif;?>
                <td><b><?= $this->Number->format($account->amount)?></b></td>
                <td><?= $this->Time->format($account->loan_date, 'dd-MMM-yyyy') ?></td>
                <td>
                    <?php if($account->status == 'opened') : ?>
                        <span class="label">Active</span>
                    <?php else : ?>
                        <span class="label alert">Closed</span>
                    <?php endif;?>
                </td>
                <td class="actions" style="text-align: center;">
                    <?= $this->Html->link(('<i class="fa fa-print"></i>' . ""), ['controller' => 'Accounts', 'action' => 'print', $account->id], ['escape'=>false, 'class'=>'label info', 'target' => '_new']) ?>
                    <?= $this->Html->link(('<i class="fa fa-book"></i>' . " Receipts"), ['controller' => 'Receipts', 'action' => 'add', 'account_id' => $account->id], ['escape'=>false, 'class'=>'label info']) ?>
                    <?= $this->Html->link(('<i class="fa fa-eye"></i>' . " View"), ['action' => 'view', $account->id], ['escape'=>false, 'class'=>'label']) ?>
                    <?php if($user_role == 'admin') : ?>
                    <?= $this->Html->link('<i class="fa fa-pencil"></i>' . ' Edit', ['action' => 'edit', $account->id], ['escape'=>false, 'class'=>'label success']) ?>
                    <?= $this->Form->postLink('<i class="fa fa-trash-o"></i>' . ' Delete', ['action' => 'delete', $account->id], ['escape'=>false, 'class'=>'label alert', 'confirm' => __('Are you sure you want to delete # {0}?', $account->id)]) ?>
                    <?php endif?>
                </td>

                <td>
                    <?=$account->created_user ? $account->created_user->username : '';?>
                </td>
                <td>
                    <?=$account->modified_user ? $account->modified_user->username : '';?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<div id="customerModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<div id="accountModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>