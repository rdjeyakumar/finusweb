<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account $account
 */
?>

<div class="accounts form large-12 medium-12 columns content">
    <?= $this->Form->create($account, [
        'type' => 'file',
        'horizontal' => true,
        'onsubmit' => "return confirm(\"Are you sure? \");",
    ]); ?>
    <fieldset>
        <legend><?= __('Add Account') ?></legend>
        <div class="row">
            <div class="large-2 columns">
                <?php echo $this->Form->control('ref_no', ['readonly' => false]);?>
            </div>

            <div class="large-4  columns">
                <?php echo $this->Form->control('loan_date', ['type' => 'date', 'label' => 'Date']);?>
            </div>

            <?php if($user_role == 'admin') : ?>
            <div class="large-2 columns">
                <!-- <?php echo $this->Form->control('account_type', ['options' => ['direct'=>'Direct', 'indirect'=>'Indirect']]);?> -->
            </div>
            <?php endif;?>
        </div>

        <h4>
            Customer Details <a href="#" data-reveal-id="myModal">Load Customer</a> OR 
            <a href="#" onclick="newCustomer()">New Customer</a>
        </h4>
        
        <?= $this->element('customer_form', ['isAccount' => true]); ?>
        
        <h4 id="guaranteer_h4">Guaranteer Details - <input type="checkbox" id="show_guaranteer" style="width: 20px; height: 20px;" onclick="showGuaranteer()" />
            <a href="#" data-reveal-id="myModal1">Load Customer</a> OR 
            <a href="#" onclick="loadGuaranteerForm()">New Customer</a>
        </h4>

        <?= $this->element('guaranteer_form', ['isAccount' => true]); ?>

        <div id="page3">
            <div id="propertyDiv">
                <h4>Property Details</h4>
                <div class="row">
                    <div class="large-4 columns">
                        <?php echo $this->Form->control('account_details.0.property_name', ['class' => 'property-name']);?>
                    </div>

                    <div class="large-2 columns">
                        <?php echo $this->Form->control('account_details.0.qty');?>
                    </div>

                    <div class="large-3 columns">
                        <?php echo $this->Form->control('account_details.0.gross_weight', ['onkeyup' => "sumTotalWeight()", 'type'=>'text']);?>
                    </div>

                    <div class="large-3 columns">
                        <?php echo $this->Form->control('account_details.0.net_weight', ['onkeyup' => "sumNetWeight()", 'type'=>'text']);?>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="right">
                    <a id="addPropertyBtn" class="button success tiny">Add</a>
                    <a style="display: none;" id="deletePropertyBtn" class="button alert tiny" onclick="deletePropertyRow(event)">Delete</a>
                </div>
            </div>

            <div class="row">
                <div class="large-6 columns">&nbsp;</div>

                <div class="large-2 columns">
                    <?php echo $this->Form->control('property_value');?>
                </div>
                
                <div class="large-2 columns">
                    <?php echo $this->Form->control('total_weight', ['readonly' => true, 'tabindex' =>-1]);?>
                </div>

                <div class="large-2 columns">
                    <?php echo $this->Form->control('net_weight', ['readonly' => true, 'tabindex' =>-1]);?>
                </div>

                
            </div>
        </div>

        <div id="page4">
            <h4>Account Details</h4>
            <div class="row">
                <div class="large-6 columns">
                    &nbsp;
                </div>

                <div class="large-4 columns">
                    <?php echo $this->Form->control('amount', ['style' => 'text-align: right; font-size: 26px']);?>
                </div>

                <div class="large-2 columns">
                    <?php echo $this->Form->control('interest', ['type'=>'text', 'label' => 'Interest Rate (%)']);?>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="large-6 columns">&nbsp;</div>
            <div class="large-4 columns">
                <div class="right">
                    <?php echo $this->Form->input('collect_first_month_interest', ['type' => 'checkbox']);?>
                </div>
            </div>
            <div class="large-2 columns">
                <!-- <a id="nextBtn" class="button primary tiny" onclick="nextPage(event)">Next</a> -->
                <button id="submitBtn" class="button primary tiny" type="submit">Finish</button>
            </div>
        </div>

    </fieldset>
    <?= $this->Form->end() ?>

    <div id="myModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <h2 id="modalTitle">Customers</h2>
        <form onsubmit="return false;">
            <div class="row">
                <div class="small-6 columns">
                  <?php echo $this->Form->input("search", ['label' => 'Search']);?>
                </div>
                <div class="small-4 columns">
                    <?php echo $this->Form->input("search_by", ['label' => 'By', 'options' => [
                        '' => 'Any',
                        'Customers.id' => 'Customer ID',
                        'firstname' => 'First name',
                        'lastname' => 'Last name',
                        'idendity_code' => 'Identity Code',
                        'CustomerAddress.mobile' => 'Mobile',
                        'CustomerAddress.phone' => 'Phone',
                        'CustomerAddress.door_no' => 'Door No',
                        'CustomerAddress.street' => 'Street',
                        'CustomerAddress.area' => 'Area',
                        'CustomerAddress.village' => 'Village',
                        'CustomerAddress.City' => 'City',
                        'CustomerAddress.pincode' => 'Pin Code'
                    ]]);?>
                </div>
                <div class="small-2 columns">
                    <button id="search_btn" style="margin-top: 12px;">GO</button>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns" id="search_results">
                    <table>
                        <thead>
                            <tr>
                                <th width="80">Cust-ID</th>
                                <th width="50">Customer</th>
                                <th width="300"></th>
                                <th width="200">Acc History</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>

    <div id="myModal1" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <h2 id="modalTitle1">Customers</h2>
        <form onsubmit="return false;">
            <div class="row">
                <div class="small-6 columns">
                  <?php echo $this->Form->input("search1", ['label' => 'Search']);?>
                </div>
                <div class="small-4 columns">
                    <?php echo $this->Form->input("search_by1", ['label' => 'By', 'options' => [
                        '' => 'Any',
                        'Customers.id' => 'Customer ID',
                        'firstname' => 'First name',
                        'lastname' => 'Last name',
                        'idendity_code' => 'Identity Code',
                        'CustomerAddress.mobile' => 'Mobile',
                        'CustomerAddress.phone' => 'Phone',
                        'CustomerAddress.door_no' => 'Door No',
                        'CustomerAddress.street' => 'Street',
                        'CustomerAddress.area' => 'Area',
                        'CustomerAddress.village' => 'Village',
                        'CustomerAddress.City' => 'City',
                        'CustomerAddress.pincode' => 'Pin Code'
                    ]]);?>
                </div>
                <div class="small-2 columns">
                    <button id="search_btn1" style="margin-top: 12px;">GO</button>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns" id="search_results1">
                    <table>
                        <thead>
                            <tr>
                                <th width="80">Cust-ID</th>
                                <th width="50">Customer</th>
                                <th width="300"></th>
                                <th width="200">Acc History</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
</div>

<!-- PROPERTY FORM START -->
<script type="text/javascript">
    function deletePropertyRow(evt) {
        evt.preventDefault();
        if(propertyIndex > 0) {
            $('#propertyRow' + propertyIndex).remove();
        }
        propertyIndex--;

        if(propertyIndex > 0) {
            $('#deletePropertyBtn').show();
        } else {
            $('#deletePropertyBtn').hide();
        }

        sumTotalWeight();
        sumNetWeight();
    };

    function sumTotalWeight() {
        var sumVal = 0;
        for(var i=0; i<=propertyIndex; i++) {
            var val = $('#account-details-' +i+ '-gross-weight').val();
            if(val) {
                sumVal += parseFloat(val);
            }
        }

        $('#total-weight').val(sumVal);
    }

    function sumNetWeight() {
        var sumVal = 0;
        for(var i=0; i<=propertyIndex; i++) {
            var val = $('#account-details-' +i+ '-net-weight').val();
            if(val) {
                sumVal += parseFloat(val);
            }
        }

        $('#net-weight').val(sumVal);
    }

    var propertyIndex = 0;

    $('#addPropertyBtn').on('click', function(evt) {
        evt.preventDefault();
        propertyIndex++;
        var propertyHtml = 
        '<div class="row" id="propertyRow'+propertyIndex+'">'+
            '<div class="large-4 columns">'+
                '<div class="input text required">'+
                    '<label for="account-details-' +propertyIndex+ '-property-name">Property Name</label>'+
                    '<input type="text" class="property-name" required="required" maxlength="50" name="account_details[' +propertyIndex+ '][property_name]" id="account-details-' +propertyIndex+ '-property-name" autocomplete="off">'+
                '</div>'+
            '</div>'+

            '<div class="large-2 columns">'+
                '<div class="input number required">'+
                    '<label for="account-details-' +propertyIndex+ '-qty">Qty</label>'+
                    '<input type="number" required="required" name="account_details[' +propertyIndex+ '][qty]" id="account-details-' +propertyIndex+ '-qty">'+
                '</div>'+
            '</div>'+

            '<div class="large-3 columns">'+
                '<div class="input number required">'+
                    '<label for="account-details-' +propertyIndex+ '-gross-weight">Gross Weight</label>'+
                    '<input type="text" required="required" step="0.0001" name="account_details[' +propertyIndex+ '][gross_weight]" id="account-details-' +propertyIndex+ '-gross-weight" onkeyup="sumTotalWeight()">'+
                '</div>'+
            '</div>'+

            '<div class="large-3 columns">'+
                '<div class="input number">'+
                    '<label for="account-details-' +propertyIndex+ '-net-weight">Net Weight</label>'+
                    '<input type="text" step="0.0001" name="account_details[' +propertyIndex+ '][net_weight]" id="account-details-' +propertyIndex+ '-net-weight" onkeyup="sumNetWeight()">'+
                '</div>'+
            
            '</div>'+

        '</div>';

        if(propertyIndex > 0) {
            $('#deletePropertyBtn').show();
        } else {
            $('#deletePropertyBtn').hide();
        }
        
        $('#propertyDiv').append(propertyHtml);

        sumTotalWeight();
        sumNetWeight();
    });

    $('#propertyDiv').on('change', 'select', function() {
        var index = $(this).attr('data-index');
        var property = $(this).val();
        $("#account-details-"+index+"-property-name").val(property);
    });
        
</script>
<!-- PROPERTY FORM END -->

<!-- GURANTEER FORM START -->
<script type="text/javascript">
    function showGuaranteer() {
        var opt = $('#show_guaranteer').is(':checked');
        if(opt) {
            loadGuaranteerForm();
        } else {
            $("#page2").empty();
        }
    }

    function fetchGuaranteer() {
        $.post("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'search'));?>",
        {
            search: $("#search1").val(),
            search_by: $("#search-by1").val()
        },
        function(data, status) {
            $("#search_results1").html(data);
        });
    }

    function loadGuaranteerForm() {
        $.get("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'guaranteer_form'));?>",{isAccount: true},
        function(data, status) {
            $("#page2").html(data);
        });
    }

    $("button#search_btn1").click(fetchGuaranteer);

    $("#search1").on('keyup', fetchGuaranteer);

    $('#myModal1').on('click', 'input.customer_btn', function() {
        var custId = $(this).attr('data-id');
        $("#page2").empty();
        $("#guaranteer_h4").show();
        $('#myModal1').foundation('reveal', 'close');

        $.get("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'loadview'));?>/"+custId,
        function(data, status) {
            $("#page2").html(data);
            $("#page2").prepend('<input type="hidden" name="customer[guaranteer_id]" id="customer-guaranteer-id" value="' +custId+ '">');
        });
    });

    $('#show_guaranteer').on('change', showGuaranteer);

    showGuaranteer();
</script>
<!-- GURANTEER FORM END -->

<!-- CUSTOMER FORM START -->
<script type="text/javascript">

    function fetchCustomer() {
        $.post("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'search'));?>",
        {
            search: $("#search").val(),
            search_by: $("#search-by").val()
        },
        function(data, status) {
            $("#search_results").html(data);
        });
    }

    function newCustomer() {
        $.get("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'customer_form'));?>",{isAccount: true},
        function(data, status) {
            $("#page1").html(data);
            $("#guaranteer_h4").show();
        });
    }

    $("button#search_btn").click(fetchCustomer);

    $("#search").on('keyup', fetchCustomer);

    $('#myModal').on('click', 'input.customer_btn', function() {
        var custId = $(this).attr('data-id');
        $("#page1").empty();
        $("#page2").empty();
        $("#guaranteer_h4").hide();
        $('#myModal').foundation('reveal', 'close');

        $.get("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'loadview'));?>/"+custId,
        function(data, status) {
            $("#page1").html(data);
            $("#page1").prepend('<input type="hidden" name="customer_id" id="customer-id" value="' +custId+ '">');
        });
    });
</script>
<!-- CUSTOMER FORM END -->

<!-- AutoComplete Starts -->
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script>
 $(function() {
    function fetchAutoCompleteData() {
        $.get("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'autoCompleteData'));?>",
        function(result, status) {
            var data = JSON.parse(result);
            $( "#customer-firstname" ).autocomplete({
               source: data.firstnameList
            });

            $( "#customer-lastname" ).autocomplete({
               source: data.firstnameList
            });

            var options = {
                source: data.property_nameList
            };
            var selector = 'input.property-name';
            $(document).on('keydown.autocomplete', selector, function() {
                $(this).autocomplete(options);
            });

            $( "#customer-customer-addres-street" ).autocomplete({
               source: data.streetList
            });

            $( "#customer-customer-addres-area" ).autocomplete({
               source: data.areaList
            });

            $( "#customer-customer-addres-village" ).autocomplete({
               source: data.villageList
            });

            $( "#customer-customer-addres-city" ).autocomplete({
               source: data.cityList
            });

            $( "#customer-customer-addres-pincode" ).autocomplete({
               source: data.pincodeList
            });
        });
    }
    fetchAutoCompleteData();
 });
</script>
<!-- AutoComplete ends -->