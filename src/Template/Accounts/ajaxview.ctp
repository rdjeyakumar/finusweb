<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account $account
 */
?>
<div class="accounts view large-12 medium-12 columns">
    <h3>Account - <?=$account->ref_no?></h3>
    
    <div class="row">

        <?php if(!empty($account)) : ?>

        <div class="large-12 columns">
            <div class="row">
                <div class="large-2 columns">
                  <label>Amount</label>
                </div>
                <div class="large-4 columns">
                  <b>Rs <?=h($account->amount)?></b> @ <?=$account->interest?>%
                </div>

                <div class="large-2 columns">
                  <label>Date</label>
                </div>
                <div class="large-4 columns">
                  <b><?=$this->Time->format($account->loan_date, 'dd-MMM-yyyy')?></b>
                </div>
            </div>
        </div>
        <?php endif;?>
    </div>
    <?php echo $this->Form->end();?>

    <?php if(!empty($account)) : ?>
    <div class="row">
        <div class="large-6 columns">
            <table>
                <thead>
                    <tr><th>Photo</th><th>Identity File</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php if($account->customer->photo && file_exists("files/Customers/photo/" . $account->customer->photo)) : ?>
                                <?php echo $this->Html->image(
                                    "../files/Customers/photo/" . $account->customer->photo, [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php else : ?>
                                <?php echo $this->Html->image(
                                    "noimage.jpg", [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php endif;?>
                        </td>
                        <td>
                            <?php if($account->customer->identity_file && file_exists("files/Customers/identity_file/" . $account->customer->identity_file)) : ?>
                                <?php echo $this->Html->image(
                                    "../files/Customers/identity_file/" . $account->customer->identity_file, [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php else : ?>
                                <?php echo $this->Html->image(
                                    "noimage.jpg", [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php endif;?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr><th colspan="2">Customer</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ID</td>
                        <td><b><?=h($account->customer->id)?></b></td>
                    </tr>
                    <tr>
                        <td>FirstName</td>
                        <td><b><?=h($account->customer->firstname)?></b></td>
                    </tr>
                    <tr>
                        <td>LastName</td>
                        <td><b><?=h($account->customer->lastname)?></b></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td><b><?=($account->customer->gender == 'M') ? 'Male' : 'Female'?></b></td>
                    </tr>
                    <tr>
                        <td>Identity Type</td>
                        <td><b><?=h($account->customer->identity_type)?></b></td>
                    </tr>
                    <tr>
                        <td>Identity Code</td>
                        <td><b><?=h($account->customer->identity_code)?></b></td>
                    </tr>
                    <tr>
                        <td>Identity File</td>
                        <td><b><?=h($account->customer->identity_file)?></b></td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr><th colspan="2">Customer Address</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Mobile</td>
                        <td><b><?=h($account->customer->customer_addres->mobile)?></b></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><b><?=h($account->customer->customer_addres->phone)?></b></td>
                    </tr>
                    <tr>
                        <td>Door No</td>
                        <td><b><?=h($account->customer->customer_addres->door_no)?></b></td>
                    </tr>
                    <tr>
                        <td>Street</td>
                        <td><b><?=h($account->customer->customer_addres->street)?></b></td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td><b><?=h($account->customer->customer_addres->area)?></b></td>
                    </tr>
                    <tr>
                        <td>Village</td>
                        <td><b><?=h($account->customer->customer_addres->village)?></b></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><b><?=h($account->customer->customer_addres->city)?></b></td>
                    </tr>
                    <tr>
                        <td>Pincode</td>
                        <td><b><?=h($account->customer->customer_addres->pincode)?></b></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="large-6 columns">
            <?php if(!empty($account->account_pledge_detail)) : ?>
                <table>
                  <thead>
                    <tr><th colspan="4">Pledge Details</th></tr>
                    <tr>
                      <th>Pledge ID</th>
                      <th>Date</th>
                      <th>Vendor</th>
                      <th>Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><?=h($account->account_pledge_detail->pledge_id)?></td>
                      <td><?=$this->Time->format($account->account_pledge_detail->pledge_date, 'dd-MMM-yyyy')?></td>
                      <td><?=h($account->account_pledge_detail->pledge_vendor)?></td>
                      <td><?=h($account->account_pledge_detail->pledge_amount)?></td>
                    </tr>
                  </tbody>
                </table>
            <?php endif;?>
            <?php if(!empty($account)) : ?>
                <table>
                  <thead>
                    <tr><th colspan="4">Account Details</th></tr>
                    <tr>
                      <th>Property</th>
                      <th>Qty</th>
                      <th>Gross Weight</th>
                      <th>Net Weight</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($account->account_details as $key => $account_detail) : ?>
                    <tr>
                      <td><?=h($account_detail->property_name)?></td>
                      <td><?=h($account_detail->qty)?></td>
                      <td><?=h($account_detail->gross_weight)?></td>
                      <td><?=h($account_detail->net_weight)?></td>
                    </tr>
                    <?php endforeach;?>
                  </tbody>
                  <tfoot>
                    <th style="text-align: right">Property Value</th>
                    <th>Rs. <?=h($account->property_value)?></th>
                    <th><?=h($account->total_weight)?></th>
                    <th><?=h($account->net_weight)?></th>
                  </tfoot>
                </table>
            <?php endif;?>

            <?php if($account->customer->guaranteer) : ?>
            <table>
                <thead>
                    <tr><th colspan="2">Guaranteer Address</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td><b><?=h($account->customer->guaranteer->firstname)?></b></td>
                    </tr>
                    <tr>
                        <td>Mobile</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->mobile)?></b></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->phone)?></b></td>
                    </tr>
                    <tr>
                        <td>Door No</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->door_no)?></b></td>
                    </tr>
                    <tr>
                        <td>Street</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->street)?></b></td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->area)?></b></td>
                    </tr>
                    <tr>
                        <td>Village</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->village)?></b></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->city)?></b></td>
                    </tr>
                    <tr>
                        <td>Pincode</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->pincode)?></b></td>
                    </tr>
                </tbody>
            </table>
            <?php endif;?>
        </div>
    </div>
    <?php endif;?>
</div>
