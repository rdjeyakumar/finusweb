<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
 */
?>

<div class="accounts index large-12 medium-12 columns content">
    <h3><?= __('Report - Accounts') ?></h3>
    <div class="filters">
        <?php echo $this->Form->create("Filter",array('class' => 'filter', 'type' => 'GET'));?>
        <div class="row">
            <div class="large-2 columns">
                <?php echo $this->Form->input("start_date", array('label' => 'From', 'placeholder' => "", 'value' => isset($_GET['start_date']) ? $_GET['start_date'] : ''));?>
            </div>

            <div class="large-2 columns">
                <?php echo $this->Form->input("end_date", array('label' => 'To', 'placeholder' => "", 'value' => isset($_GET['end_date']) ? $_GET['end_date'] : ''));?>
            </div>

            <div class="large-2 columns">
                <?php echo $this->Form->input("status", array('label' => 'Status', 'options' => ['' => 'All', 'opened' => 'Opened', 'closed' => 'Closed'], 'value' => isset($_GET['status']) ? $_GET['status'] : ''));?>
            </div>
            
            <div class="large-2 columns">
                <?php echo $this->Form->submit("Search", ['style' => 'margin-top: 18px', 'class'=>'button small info']);?>
            </div>

            <div class="large-2 columns">
                <label>Total Amount</label>
                <input type="text" readonly="true" value="<?=$total?>" style="font-size: 24px; text-align: right;">
            </div>

            <div class="large-2 columns">
                <h3>Count: <?= $this->Paginator->counter(['format' => __('{{count}}')]) ?></h3>
            </div>
        </div>
        <?php echo $this->Form->end();?>

        <div class="row">
            <div class="large-2 columns">
                <label>Select All</label>
                <input type="checkbox" style="width: 20px; height: 20px;" id="select_all_chk" />
            </div>

            <div class="large-2 columns">
                <?php echo $this->Form->button("Export", ['style' => 'margin-top: 18px', 'class'=>'button small info', 'id' => 'submit_btn']);?>
            </div>

            <div class="large-2 columns">
                <?php echo $this->Form->input("post_date", array('placeholder' => "", 'value' => isset($_GET['post_date']) ? $_GET['post_date'] : ''));?>
            </div>

            <div class="large-2 columns">
                <?php echo $this->Form->input("auction_date", array('placeholder' => "", 'value' => isset($_GET['auction_date']) ? $_GET['auction_date'] : ''));?>
            </div>

            <div class="large-2 columns">
                <?php echo $this->Form->button("Register POST", ['style' => 'margin-top: 18px', 'class'=>'button small info', 'id' => 'submit_reg_btn']);?>
            </div>
        </div>
    </div>
    
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th width="30"></th>
                <th width="150">Acc Ref No</th>
                <th width="80">Cust-ID</th>
                <th width="280">Customer</th>
                <th width="150">Loan Amount (Rs)</th>
                <th width="180">Loan Date</th>
                <th width="100">Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($accounts as $account): ?>
            <tr style="<?=$account->is_oldest ? 'background: #FBD0D5;' : ''?>">
                <td><input type="checkbox" style="width: 20px; height: 20px;" class="select_chk" name="select_chk[]" value="<?=$account->id?>" /></td>
                <td>
                    <a href="<?= \Cake\Routing\Router::url(['controller' => 'Accounts', 'action' => 'ajaxview']) ?>/<?=$account->id?>" data-reveal-id="accountModal" data-reveal-ajax="true">
                        <?php if($account->is_oldest) : ?>
                            <b style="color: red"><?= $account->ref_no;?></b> (<?=$account->duration?> days)
                        <?php else : ?>
                            <?= $account->ref_no;?>
                        <?php endif; ?>
                    </a>
                </td>
                <?php if(!$account->customer) : ?>
                    <td>-</td>
                    <td>-</td>
                <?php else : ?>
                    <td>
                        <?= $this->Html->link(h($account->customer->id), ['controller' => 'Customers', 'action' => 'view', $account->customer->id]) ?>
                    </td>
                    <td>
                        <?= $this->Html->link(($account->customer->full_detail), ['controller' => 'Customers', 'action' => 'loadview', $account->customer->id], ['escape'=>false, 'class'=>'', 'data-reveal-id' => 'customerModal', 'data-reveal-ajax' => 'true']) ?>
                    </td>
                <?php endif;?>
                <td><b><?= $this->Number->format($account->amount)?></b></td>
                <td><?= $this->Time->format($account->loan_date, 'dd-MMM-yyyy hh:mm a') ?></td>
                <td>
                    <?php if($account->status == 'opened') : ?>
                        <span class="label">Active</span>
                    <?php else : ?>
                        <span class="label alert">Closed</span>
                    <?php endif;?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>

    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>

    <div id="customerModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>

    <div id="accountModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
</div>

<script>
    $( function() {
        if($( "#start-date" ).val() == '') {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }

        if($( "#end-date" ).val() == '') {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }

        if($( "#post-date" ).val() == '') {
            $( "#post-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#post-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }

        if($( "#auction-date" ).val() == '') {
            $( "#auction-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#auction-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }
    });

    $("input#select_all_chk").on('change', function (evt) {
        var checked = $(this).is(':checked');
        $('input.select_chk').prop('checked', checked);
    });

    $("#submit_btn").on('click', function(evt) {
        var data = [];
        $("input.select_chk:checked").each(function(){
            data.push($(this).val());
        });

        if(data.length == 0) {
            $("input.select_chk").each(function(){
                data.push($(this).val());
            });
        }
        
        window.open("<?= \Cake\Routing\Router::url(array('controller' => 'Accounts', 'action' => 'export'));?>?select_chk="+data, "_new");
    });

    $("#submit_reg_btn").on('click', function(evt) {
        var data = [];
        $("input.select_chk:checked").each(function(){
            data.push($(this).val());
        });

        if(data.length == 0) {
            $("input.select_chk").each(function(){
                data.push($(this).val());
            });
        }
        var post_date = $("#post-date").val();
        var auction_date = $("#auction-date").val();
        window.open("<?= \Cake\Routing\Router::url(array('controller' => 'Accounts', 'action' => 'exportregpost'));?>?select_chk="+data+"&post_date="+post_date+"&auction_date="+auction_date, "_new");
    });
</script>