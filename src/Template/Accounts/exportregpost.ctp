<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
 */
$cakeDescription = 'Finus Web';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <!-- <?= $this->Html->css('base.css') ?> -->
    
    <style type="text/css">
        table {
            width: 100%;
        }
        
        table.border-table, .border-table th, .border-table td {
            border: 1px solid black;
        }

        body {
          background: rgb(204,204,204); 
        }
        page {
          background: white;
          display: block;
          margin: 0 auto;
          margin-bottom: 0.5cm;
          /*box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
        }
        page[size="A4"] {  
          width: 21cm;
          height: 29.7cm; 
        }
        page[size="A4"][layout="portrait"] {
          width: 29.7cm;
          height: 21cm;  
        }
        page[size="A3"] {
          width: 29.7cm;
          height: 42cm;
        }
        page[size="A3"][layout="portrait"] {
          width: 42cm;
          height: 29.7cm;  
        }
        page[size="A5"] {
          width: 14.8cm;
          height: 21cm;
        }
        page[size="A5"][layout="portrait"] {
          width: 21cm;
          height: 14.8cm;  
        }
        @media print {
          body, page {
            margin: 0;
            box-shadow: 0;
          }
        }
    </style>
</head>
<body>
    <?php foreach ($accounts as $account): ?>
    <page size="A4">
        <div class="accounts index large-12 medium-12 columns content" style="padding: 30px; margin-top: 15px;">
            <h2 style="text-align: center;"><?= __('ஏல நோட்டீஸ்') ?></h2>
            <table cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td><b>அடகு வைத்தவர் விபரம்</b></td>
                        <td style="text-align: right">
                            <?= $profile->subtitle ? $profile->subtitle . "<br/>" : "" ?>
                            <?= $profile->title ?> <br/>
                            <?= $profile->address ? $profile->address . "<br/>" : "" ?>
                            <?= $profile->city ? $profile->city . " " : "" ?>
                            <?= $profile->pincode ? $profile->pincode . "<br/>" : "" ?>
                            <?= $profile->mobile1 ? $profile->mobile1 . " " : "" ?>
                            <?= $profile->mobile2 ? $profile->mobile2 . " " : "" ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><?= $account->customer->full_detail ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><br/><b>அடகு வைத்ததின் விபரம்</b></td>
                    </tr>
                    <tr>
                        <td>
                            <table class="border-table" style="margin-top: 15px; margin-left: 25%;"  cellpadding="10" cellspacing="0">
                                <thead>
                                    <tr>
                                        <td>எண்</td>
                                        <td>அடகு எண்</td>
                                        <td>அடகுத்தேதி</td>
                                        <td>அடகுத்தொகை</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><b>1</b></td>
                                        <td><b><?= $account->ref_no ?></b></td>
                                        <td><b><?= $this->Time->format($account->loan_date, 'dd-MMM-yyyy') ?></b></td>
                                        <td><b><?= $account->amount ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h3>மதிப்புடையீர்,</h3>
                            <p style="line-height: 2em;">&emsp;&emsp;&emsp;தாங்கள், தங்க நகை ஈட்டின்பேரில் கடன் கொடுக்கும் <b>"<?=$profile->title?>"</b> என்ற எமது நிறுவனத்தில் மேற்படி தேதியில், மேற்படி தொகையை கடனாக பெற்றுள்ளீர்கள். மேலும் கடனை திரும்பச்செலுத்தும் தவணை தேதி முடிவடைந்தும் தாங்கள் எந்தவித அசலும், வட்டியும் செலுத்தவில்லை. இதை தபால் மூலம் தெரியப்படுத்தியும் நகையை திருப்பவில்லை. எனவே தங்கள் நகையை பகீரங்கமாக ஏலம்விட <?= $this->Time->format($auction_date, 'dd-MMM-yyyy') ?> தேதியில் நிர்வாகத்தால் தீர்மானிக்கப்பட்டுள்ளது. ஏலத்தில் நிர்வாகத்திற்கு பாக்கித்தொகை ஏதேனும் வருமாறு இருந்தால் சட்டப்படி தங்களிடம் வசூலிக்கப்படும். </p>

                            <p style="line-height: 2em;">&emsp;&emsp;&emsp;இதனை தவிர்த்திட ஏலத்தேதிக்கு முன் அசல் + வட்டி பாக்கியை செலுத்தி தங்கள் நகையை பெற்றுக்கொள்ளும்படி அறிவுறுத்தப்படுகிறது. இதுவே இறுதி அறிவிப்பு ஆகும்.</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            தேதி: <?= $this->Time->format($post_date, 'dd-MMM-yyyy') ?><br/>
                            இடம்: <?= $profile->city ? $profile->city . " " : "" ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; font-size: 12px;">
                            <p style="margin-top: 45px;">(குறிப்பு: அசல், வட்டி செலுத்தியிருப்பின் ரசீதுடன் நேரில்வரவும், ஏலத்தேதியைமாற்ற நிர்வாகத்திற்கு முழு உரிமை உண்டு.)</p>
                        </td>
                    </tr>
                </tbody>
                </tbody>
            </table>
        </div>
    </page>
    <?php endforeach;?>
</body>
</html>