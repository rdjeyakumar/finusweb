<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account $account
 */
?>
<i class="fi-music"></i>
<div class="accounts form large-12 medium-12 columns content">
    <?= $this->Form->create($account, [
        'type' => 'file',
        'horizontal' => true,
        'onsubmit' => "return confirm(\"Are you sure? \");"
    ]) ?>
    <fieldset>
        <legend><?= __('Edit Account') ?></legend>
        
        <div class="row">
            <div class="large-2 columns">
                <?php echo $this->Form->control('ref_no', ['readonly' => true]);?>
            </div>

            <div class="large-4  columns">
                <?php echo $this->Form->control('loan_date', ['type' => 'date', 'label' => 'Date']);?>
            </div>

        </div>

        <div class="row">
            <h4>Pledge Details</h4>
            <div class="large-3 columns">
                <?php echo $this->Form->control('account_pledge_detail.pledge_date', ['type' => 'date']);?>
            </div>

            <div class="large-3 columns">
                <?php echo $this->Form->control('account_pledge_detail.pledge_id', ['type' => 'text', 'label' => 'Pledge ID']);?>
            </div>

            <div class="large-3 columns">
                <?php echo $this->Form->control('account_pledge_detail.pledge_vendor');?>
            </div>

            <div class="large-3 columns">
                <?php echo $this->Form->control('account_pledge_detail.pledge_amount');?>
            </div>

        </div>

        <div id="page3">
            <div id="propertyDiv">
                <?php foreach ($account->account_details as $key => $account_detail) : ?>
                <div class="row">
                    <?php if($key == 0) : ?><h4>Property Details</h4><?php endif;?>
                    <div class="large-4 columns">
                        <?php echo $this->Form->control('account_details.'.$key.'.id', ['type'=>'hidden']);?>
                        <?php echo $this->Form->control('account_details.'.$key.'.property_name', ['class' => 'property-name']);?>
                    </div>

                    <div class="large-2 columns">
                        <?php echo $this->Form->control('account_details.'.$key.'.qty');?>
                    </div>

                    <div class="large-3 columns">
                        <?php echo $this->Form->control('account_details.'.$key.'.gross_weight', ['onkeyup' => "sumTotalWeight()", 'type'=>'text']);?>
                    </div>

                    <div class="large-3 columns">
                        <?php echo $this->Form->control('account_details.'.$key.'.net_weight', ['onkeyup' => "sumNetWeight()", 'type'=>'text']);?>
                    </div>

                </div>
                <?php endforeach;?>
            </div>

            <div class="row">
                <div class="right">
                    <a id="addPropertyBtn" class="button success tiny">Add</a>
                    <a style="display: none;" id="deletePropertyBtn" class="button alert tiny" onclick="deletePropertyRow(event)">Delete</a>
                </div>
            </div>

            <div class="row">
                <div class="large-6 columns">&nbsp;</div>

                <div class="large-2 columns">
                    <?php echo $this->Form->control('property_value');?>
                </div>
                
                <div class="large-2 columns">
                    <?php echo $this->Form->control('total_weight', ['readonly' => true, 'tabindex' =>-1]);?>
                </div>

                <div class="large-2 columns">
                    <?php echo $this->Form->control('net_weight', ['readonly' => true, 'tabindex' =>-1]);?>
                </div>

                
            </div>
        </div>

        <div class="row">
            <h4>Account Details</h4>
            <div class="large-6 columns">
                &nbsp;
            </div>

            <div class="large-4 columns">
                <?php echo $this->Form->control('amount', ['style' => 'text-align: right; font-size: 26px']);?>
            </div>

            <div class="large-2 columns">
                <?php echo $this->Form->control('interest', ['type'=>'text', 'label' => 'Interest Rate (%)']);?>
            </div>
        </div>
    </fieldset>
    
    <div class="row">
        <div class="large-8 columns">&nbsp;</div>
        <div class="large-2 columns">
            <!-- <a id="previousBtn" class="button primary tiny" onclick="previousPage(event)">Previous</a> -->
            &nbsp;
        </div>
        <div class="large-2 columns">
            <!-- <a id="nextBtn" class="button primary tiny" onclick="nextPage(event)">Next</a> -->
            <button id="submitBtn" class="button primary tiny" type="submit">Finish</button>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    var propertyIndex = 0;
    <?php if(!empty($account->account_details)) : ?>
        <?php echo "propertyIndex=".count($account->account_details)."-1;";?>
    <?php endif;?>
    $('#addPropertyBtn').on('click', function(evt) {
        evt.preventDefault();
        propertyIndex++;
        var propertyHtml = 
        '<div class="row" id="propertyRow'+propertyIndex+'">'+
            '<div class="large-4 columns">'+
                '<div class="input text required">'+
                    '<label for="account-details-' +propertyIndex+ '-property-name">Property Name</label>'+
                    '<input type="text" class="property-name" required="required" maxlength="50" name="account_details[' +propertyIndex+ '][property_name]" id="account-details-' +propertyIndex+ '-property-name" autocomplete="off">'+
                '</div>'+
            '</div>'+

            '<div class="large-2 columns">'+
                '<div class="input number required">'+
                    '<label for="account-details-' +propertyIndex+ '-qty">Qty</label>'+
                    '<input type="number" required="required" name="account_details[' +propertyIndex+ '][qty]" id="account-details-' +propertyIndex+ '-qty">'+
                '</div>'+
            '</div>'+

            '<div class="large-3 columns">'+
                '<div class="input number required">'+
                    '<label for="account-details-' +propertyIndex+ '-gross-weight">Gross Weight</label>'+
                    '<input type="text" required="required" step="0.0001" name="account_details[' +propertyIndex+ '][gross_weight]" id="account-details-' +propertyIndex+ '-gross-weight" onkeyup="sumTotalWeight()">'+
                '</div>'+
            '</div>'+

            '<div class="large-3 columns">'+
                '<div class="input number">'+
                    '<label for="account-details-' +propertyIndex+ '-net-weight">Net Weight</label>'+
                    '<input type="text" step="0.0001" name="account_details[' +propertyIndex+ '][net_weight]" id="account-details-' +propertyIndex+ '-net-weight" onkeyup="sumNetWeight()">'+
                '</div>'+
            
            '</div>'+

        '</div>';

        if(propertyIndex > 0) {
            $('#deletePropertyBtn').show();
        } else {
            $('#deletePropertyBtn').hide();
        }
        
        $('#propertyDiv').append(propertyHtml);

        sumTotalWeight();
        sumNetWeight();
    });

    function deletePropertyRow(evt) {
        evt.preventDefault();
        if(propertyIndex > 0) {
            $('#propertyRow' + propertyIndex).remove();
        }
        propertyIndex--;

        if(propertyIndex > 0) {
            $('#deletePropertyBtn').show();
        } else {
            $('#deletePropertyBtn').hide();
        }

        sumTotalWeight();
        sumNetWeight();
    };

    function sumTotalWeight() {
        var sumVal = 0;
        for(var i=0; i<=propertyIndex; i++) {
            var val = $('#account-details-' +i+ '-gross-weight').val();
            if(val) {
                sumVal += parseFloat(val);
            }
        }

        $('#total-weight').val(sumVal);
    }

    function sumNetWeight() {
        var sumVal = 0;
        for(var i=0; i<=propertyIndex; i++) {
            var val = $('#account-details-' +i+ '-net-weight').val();
            if(val) {
                sumVal += parseFloat(val);
            }
        }

        $('#net-weight').val(sumVal);
    }

    function showImage1() {
        var src = document.getElementById('customer-photo');
        var target = document.getElementById('img1');

        var fr=new FileReader();
        fr.onload = function(e) { target.src = this.result; };
        src.addEventListener("change",function() {
            fr.readAsDataURL(src.files[0]);
        });
    }

    $('#show_guaranteer').on('change', showGuaranteer);

    function showGuaranteer() {
        var opt = $('#show_guaranteer').is(':checked');
        if(opt) {
            $('#guaranteer_form').show();
        } else {
            $('#guaranteer_form').hide();
        }
    }

    showGuaranteer();

    $('#propertyDiv').on('change', 'select', function() {
        var index = $(this).attr('data-index');
        var property = $(this).val();
        $("#account-details-"+index+"-property-name").val(property);
    });
</script>

<script>
  $( function() {
    setTimeout(function() {
        $( "#loan-date" ).datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm TT'
        });
    }, 2000);
  } );
</script>

<!-- AutoComplete Starts -->
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script>
 $(function() {
    function fetchAutoCompleteData() {
        $.get("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'autoCompleteData'));?>",
        function(result, status) {
            var data = JSON.parse(result);
            $( "#customer-firstname" ).autocomplete({
               source: data.firstnameList
            });

            $( "#customer-lastname" ).autocomplete({
               source: data.firstnameList
            });

            var options = {
                source: data.property_nameList
            };
            var selector = 'input.property-name';
            $(document).on('keyup.autocomplete', selector, function() {
                $(this).autocomplete(options);
            });

            $( "#customer-customer-addres-street" ).autocomplete({
               source: data.streetList
            });

            $( "#customer-customer-addres-area" ).autocomplete({
               source: data.areaList
            });

            $( "#customer-customer-addres-village" ).autocomplete({
               source: data.villageList
            });

            $( "#customer-customer-addres-city" ).autocomplete({
               source: data.cityList
            });

            $( "#customer-customer-addres-pincode" ).autocomplete({
               source: data.pincodeList
            });
        });
    }
    fetchAutoCompleteData();
 });
</script>
<!-- AutoComplete ends -->