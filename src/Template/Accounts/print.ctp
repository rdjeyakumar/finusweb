<html>
	<head>
		<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
		<?= $this->Html->css('bootstrap.min.css') ?>
	</head>
	<body>
		<div class="container">
			<div class="panel panel-default">
				<div class="panel-body">
					<h2 class="text-center"><?= $profile->title ?></h2>
					<h4 class="text-center"><?= $profile->subtitle ? $profile->subtitle : "" ?></h4>
					<div class="text-center">
						<?= $profile->address ? $profile->address . ", " : "" ?>
                        <?= $profile->city ? $profile->city . " " : "" ?>
                        <?= $profile->pincode ? $profile->pincode . "<br/>" : "" ?>
                        <?= $profile->mobile1 ? $profile->mobile1 . " " : "" ?>
                        <?= $profile->mobile2 ? $profile->mobile2 . " " : "" ?>
					</div>

					<table class="table table-bordered">
					    <thead>
					      <tr>
					        <th colspan="2">Customer</th>
					        <th colspan="2">Guaranteer</th>
					      </tr>
					    </thead>
					    <tbody>
					      <tr>
					        <td>
					        	<?php if($account->customer->photo && file_exists("files/Customers/photo/" . $account->customer->photo)) : ?>
	                                <?php echo $this->Html->image(
	                                    "../files/Customers/photo/" . $account->customer->photo, [
	                                    "class" => "img-rounded",
	                                    "width" => "150"
	                                ]);?>
	                            <?php else : ?>
	                                <?php echo $this->Html->image(
	                                    "noimage.jpg", [
	                                    "class" => "img-rounded",
	                                    "width" => "150"
	                                ]);?>
	                            <?php endif;?>
					        </td>
					        <td>
					        	<?=$account->customer->full_detail;?><br/>
					        	<?=$account->customer->identity_type . ": <b>" . $account->customer->identity_code . "</b>";?>
							</td>
							<td>
								<?php if($account->customer->guaranteer && $account->customer->guaranteer->photo && file_exists("files/Customers/photo/" . $account->customer->guaranteer->photo)) : ?>
	                                <?php echo $this->Html->image(
	                                    "../files/Customers/photo/" . $account->customer->guaranteer->photo, [
	                                    "class" => "img-rounded",
	                                    "width" => "150"
	                                ]);?>
	                            <?php else : ?>
	                                <?php echo $this->Html->image(
	                                    "noimage.jpg", [
	                                    "class" => "img-rounded",
	                                    "width" => "150"
	                                ]);?>
	                            <?php endif;?>
							</td>
					        <td>
					        	<?=$account->customer->guaranteer ? $account->customer->guaranteer->full_detail : '';?><br/>
					        	<?=$account->customer->guaranteer ? ($account->customer->guaranteer->identity_type . ": <b>" . $account->customer->guaranteer->identity_code . "</b>") : "-";?>
							</td>
					      </tr>
					    </tbody>
					  </table>
					  <h2>Accounts</h2>
					  <table class="table table-bordered">
					    <thead>
					      <tr>
					        <th>Ref no</th>
					        <th>Loan Date</th>
					        <th>Amount</th>
					        <th>Status</th>
					      </tr>
					    </thead>
					    <tbody>
					      <tr>
					        <td><?= h($account->ref_no) ?></td>
	                        <td><?= $this->Time->format($account->loan_date, 'dd-MMM-yyyy') ?></td>
	                        <td><?= h($account->amount) ?></td>
	                        <td>
	                            <?php if($account->status == 'opened') : ?>
	                                <span class="label label-success">Active</span>
	                            <?php else : ?>
	                                <span class="label label-danger">Closed</span>
	                            <?php endif;?>
	                        </td>
					      </tr>
					    </tbody>
					  </table>

					  <table class="table table-bordered">
		                  <thead>
		                    <tr><th colspan="4">Account Details</th></tr>
		                    <tr>
		                      <th>Property</th>
		                      <th>Qty</th>
		                      <th>Gross Weight</th>
		                      <th>Net Weight</th>
		                    </tr>
		                  </thead>
		                  <tbody>
		                    <?php foreach ($account->account_details as $key => $account_detail) : ?>
		                    <tr>
		                      <td><?=h($account_detail->property_name)?></td>
		                      <td><?=h($account_detail->qty)?></td>
		                      <td><?=h($account_detail->gross_weight)?></td>
		                      <td><?=h($account_detail->net_weight)?></td>
		                    </tr>
		                    <?php endforeach;?>
		                  </tbody>
		                  <tfoot>
		                    <th style="text-align: right">Property Value</th>
		                    <th>Rs. <?=h($account->property_value)?></th>
		                    <th><?=h($account->total_weight)?></th>
		                    <th><?=h($account->net_weight)?></th>
		                  </tfoot>
		                </table>
				</div>
			</div>
		</div>
	</body>
</html>