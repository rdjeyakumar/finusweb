<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
 */
$cakeDescription = 'Finus Web';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    
    <style type="text/css">
        table, th, td {
            border: 1px solid black;
        }

        page {
          background: white;
          display: block;
          margin: 0 auto;
          margin-bottom: 0.5cm;
          /*box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
        }
        page[size="A4"] {  
          width: 21cm;
          height: 29.7cm; 
        }
        page[size="A4"][layout="portrait"] {
          width: 29.7cm;
          height: 21cm;  
        }
        page[size="A3"] {
          width: 29.7cm;
          height: 42cm;
        }
        page[size="A3"][layout="portrait"] {
          width: 42cm;
          height: 29.7cm;  
        }
        page[size="A5"] {
          width: 14.8cm;
          height: 21cm;
        }
        page[size="A5"][layout="portrait"] {
          width: 21cm;
          height: 14.8cm;  
        }
        @media print {
          body, page {
            margin: 0;
            box-shadow: 0;
          }
        }
    </style>
</head>
<body>
    <page size="A4">
        <div class="accounts index large-12 medium-12 columns content">
            <h3><?= __('Report - Accounts') ?></h3>
           
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th width="150">Acc Ref No</th>
                        <th width="80">Cust-ID</th>
                        <th width="280">Customer</th>
                        <th width="150" style="text-align: right">Loan Amount (Rs)</th>
                        <th width="180">Loan Date</th>
                        <th width="100">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($accounts as $account): ?>
                    <tr style="<?=$account->is_oldest ? 'background: #FBD0D5;' : ''?>">
                        <td>
                            <?php if($account->is_oldest) : ?>
                                <b style="color: red"><?= $account->ref_no;?></b> (<?=$account->duration?> days)
                            <?php else : ?>
                                <?= $account->ref_no;?>
                            <?php endif; ?>
                        </td>
                        <?php if(!$account->customer) : ?>
                            <td>-</td>
                            <td>-</td>
                        <?php else : ?>
                            <td><?=h($account->customer->id)?></td>
                            <td>
                                <?= ($account->customer->full_detail)?>
                            </td>
                        <?php endif;?>
                        <td style="text-align: right"><b><?= h($account->amount)?></b></td>
                        <td><?= $this->Time->format($account->loan_date, 'dd-MMM-yyyy hh:mm a') ?></td>
                        <td>
                            <?php if($account->status == 'opened') : ?>
                                <span class="label">Active</span>
                            <?php else : ?>
                                <span class="label alert">Closed</span>
                            <?php endif;?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </page>
</body>
</html>