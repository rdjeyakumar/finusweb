<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account $account
 */
?>
<div class="accounts view large-12 medium-12 columns content">
    <h3>Open Account</h3>
    <?php $base_url = array('controller' => 'Accounts', 'action' => 'view'); ?>
    <?php echo $this->Form->create("Filter",array('url' => $base_url, 'class' => 'filter', 'id'=>'accountOpenFrm', 'type' => 'GET'));?>
    <div class="row">
        <div class="large-3 columns">
            <div class="row collapse">
                <div class="small-2 columns">
                    <?php echo $this->Form->button("-", ['class'=>'button prefix', 'id'=>'prevBtn']);?>
                </div>
                <div class="small-8 columns">
                    <?php echo $this->Form->input("ref_no", array('label' => false, 'placeholder' => "Ref No", "value"=>isset($_GET['ref_no']) ? $_GET['ref_no'] : ""));?>
                </div>
                <div class="small-2 columns">
                    <?php echo $this->Form->button("+", ['class'=>'button postfix', 'id'=>'nextBtn']);?>
                </div>
            </div>
        </div>

        <div class="small-2 columns">
            <?php echo $this->Form->submit("Go", ['class'=>'button']);?>
        </div>

        <div class="small-7 columns">&nbsp;</div>
    </div>

    <div class="row">
        <?php if(!$account) : ?>
        <p>No records found</p>
        <?php else : ?>

        <div class="large-4 columns">
            <?= $this->Html->link(('<i class="fa fa-print"></i>' . " Print"), ['controller' => 'Accounts', 'action' => 'print', $account->id], ['escape'=>false, 'class'=>'label info', 'target' => '_new']) ?>
            <?= $this->Html->link(('<i class="fa fa-book"></i>' . " Receipts"), ['controller' => 'Receipts', 'action' => 'add', 'account_id' => $account->id], ['escape'=>false, 'class'=>'label info']) ?>
            <?= $this->Html->link(('<i class="fa fa-eye"></i>' . " View"), ['action' => 'view', $account->id], ['escape'=>false, 'class'=>'label']) ?>
            <?php if($user_role == 'admin') : ?>
            <?= $this->Html->link('<i class="fa fa-pencil"></i>' . ' Edit', ['action' => 'edit', $account->id], ['escape'=>false, 'class'=>'label success']) ?>
            <?= $this->Form->postLink('<i class="fa fa-trash-o"></i>' . ' Delete', ['action' => 'delete', $account->id], ['escape'=>false, 'class'=>'label alert', 'confirm' => __('Are you sure you want to delete # {0}?', $account->id)]) ?>
            <?php endif?>
        </div>
        
        <div class="large-6 columns">
            <div class="row">
                <div class="large-2 columns">
                  <label>Amount</label>
                </div>
                <div class="large-3 columns">
                  <b>Rs <?=h($account->amount)?></b> @ <?=$account->interest?>%
                </div>

                <div class="large-2 columns">
                  <label>Date</label>
                </div>
                <div class="large-3 columns">
                  <b><?=$this->Time->format($account->loan_date, 'dd-MMM-yyyy')?></b>
                </div>
                <div class="large-2 columns">
                    <?php if($account->status == 'opened') : ?>
                        <span class="label">Active</span>
                    <?php else : ?>
                        <span class="label alert">Closed</span>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <?php endif;?>
    </div>
    <?php echo $this->Form->end();?>

    <?php if(!empty($account)) : ?>
    <div class="row">
        <?php if($account->customer) : ?>
        <div class="large-6 columns">
            <table>
                <thead>
                    <tr><th>Photo</th><th>Identity File</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php if($account->customer->photo && file_exists("files/Customers/photo/" . $account->customer->photo)) : ?>
                                <?php echo $this->Html->image(
                                    "../files/Customers/photo/" . $account->customer->photo, [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php else : ?>
                                <?php echo $this->Html->image(
                                    "noimage.jpg", [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php endif;?>
                        </td>
                        <td>
                            <?php if($account->customer->identity_file && file_exists("files/Customers/identity_file/" . $account->customer->identity_file)) : ?>
                                <?php echo $this->Html->image(
                                    "../files/Customers/identity_file/" . $account->customer->identity_file, [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php else : ?>
                                <?php echo $this->Html->image(
                                    "noimage.jpg", [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php endif;?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr><th colspan="2">Customer</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ID</td>
                        <td>
                            <?= $this->Html->link(($account->customer->id), ['controller' => 'Customers', 'action' => 'loadview', $account->customer->id], ['escape'=>false, 'class'=>'', 'data-reveal-id' => 'customerModal', 'data-reveal-ajax' => 'true']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>FirstName</td>
                        <td><b><?=h($account->customer->firstname)?></b></td>
                    </tr>
                    <tr>
                        <td>LastName</td>
                        <td><b><?=h($account->customer->lastname)?></b></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td><b><?=($account->customer->gender == 'M') ? 'Male' : 'Female'?></b></td>
                    </tr>
                    <tr>
                        <td>Identity Type</td>
                        <td><b><?=h($account->customer->identity_type)?></b></td>
                    </tr>
                    <tr>
                        <td>Identity Code</td>
                        <td><b><?=h($account->customer->identity_code)?></b></td>
                    </tr>
                    <tr>
                        <td>Identity File</td>
                        <td><b><?=h($account->customer->identity_file)?></b></td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr><th colspan="2">Customer Address</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Mobile</td>
                        <td><b><?=h($account->customer->customer_addres->mobile)?></b></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><b><?=h($account->customer->customer_addres->phone)?></b></td>
                    </tr>
                    <tr>
                        <td>Door No</td>
                        <td><b><?=h($account->customer->customer_addres->door_no)?></b></td>
                    </tr>
                    <tr>
                        <td>Street</td>
                        <td><b><?=h($account->customer->customer_addres->street)?></b></td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td><b><?=h($account->customer->customer_addres->area)?></b></td>
                    </tr>
                    <tr>
                        <td>Village</td>
                        <td><b><?=h($account->customer->customer_addres->village)?></b></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><b><?=h($account->customer->customer_addres->city)?></b></td>
                    </tr>
                    <tr>
                        <td>Pincode</td>
                        <td><b><?=h($account->customer->customer_addres->pincode)?></b></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <?php else : ?>
        <div class="large-6 columns">
            <table>
                <tr>
                    <td style="text-align: center;">No Customer</td>
                </tr>
            </table>
        </div>
        <?php endif;?>
        <div class="large-6 columns">
            <?php if(!empty($account->account_pledge_detail)) : ?>
                <table>
                  <thead>
                    <tr><th colspan="4">Pledge Details</th></tr>
                    <tr>
                      <th>Pledge ID</th>
                      <th>Date</th>
                      <th>Vendor</th>
                      <th>Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><?=h($account->account_pledge_detail->pledge_id)?></td>
                      <td><?=$this->Time->format($account->account_pledge_detail->pledge_date, 'dd-MMM-yyyy')?></td>
                      <td><?=h($account->account_pledge_detail->pledge_vendor)?></td>
                      <td><?=h($account->account_pledge_detail->pledge_amount)?></td>
                    </tr>
                  </tbody>
                </table>
            <?php endif;?>
            <?php if(!empty($account)) : ?>
                <table>
                  <thead>
                    <tr><th colspan="4">Account Details</th></tr>
                    <tr>
                      <th>Property</th>
                      <th>Qty</th>
                      <th>Gross Weight</th>
                      <th>Net Weight</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($account->account_details as $key => $account_detail) : ?>
                    <tr>
                      <td><?=h($account_detail->property_name)?></td>
                      <td><?=h($account_detail->qty)?></td>
                      <td><?=h($account_detail->gross_weight)?></td>
                      <td><?=h($account_detail->net_weight)?></td>
                    </tr>
                    <?php endforeach;?>
                  </tbody>
                  <tfoot>
                    <th style="text-align: right">Property Value</th>
                    <th>Rs. <?=h($account->property_value)?></th>
                    <th><?=h($account->total_weight)?></th>
                    <th><?=h($account->net_weight)?></th>
                  </tfoot>
                </table>
            <?php endif;?>

            <?php if($account->customer->guaranteer) : ?>
            <table>
                <thead>
                    <tr><th colspan="2">Guaranteer Address</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ID</td>
                        <td>
                            <?= $this->Html->link(($account->customer->guaranteer->id), ['controller' => 'Customers', 'action' => 'loadview', $account->customer->guaranteer->id], ['escape'=>false, 'class'=>'', 'data-reveal-id' => 'customerModal', 'data-reveal-ajax' => 'true']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>First Name</td>
                        <td><b><?=h($account->customer->guaranteer->firstname)?></b></td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><b><?=h($account->customer->guaranteer->lastname)?></b></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td><b><?=($account->customer->guaranteer->gender == 'M') ? 'Male' : 'Female'?></b></td>
                    </tr>
                    <tr>
                        <td>Identity Type</td>
                        <td><b><?=h($account->customer->guaranteer->identity_type)?></b></td>
                    </tr>
                    <tr>
                        <td>Identity Code</td>
                        <td><b><?=h($account->customer->guaranteer->identity_code)?></b></td>
                    </tr>
                    <tr>
                        <td>Identity File</td>
                        <td><b><?=h($account->customer->guaranteer->identity_file)?></b></td>
                    </tr>
                    <tr>
                        <td>Mobile</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->mobile)?></b></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->phone)?></b></td>
                    </tr>
                    <tr>
                        <td>Door No</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->door_no)?></b></td>
                    </tr>
                    <tr>
                        <td>Street</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->street)?></b></td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->area)?></b></td>
                    </tr>
                    <tr>
                        <td>Village</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->village)?></b></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->city)?></b></td>
                    </tr>
                    <tr>
                        <td>Pincode</td>
                        <td><b><?=h($account->customer->guaranteer->customer_addres->pincode)?></b></td>
                    </tr>
                </tbody>
            </table>
            <?php endif;?>
        </div>

        <div class="large-6 columns">
            <div class="related">
                <h4><?= __('Related Accounts') ?></h4>
                <?php if (!empty($account->customer->accounts)): ?>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <th scope="col"><?= __('Ref No') ?></th>
                        <th scope="col"><?= __('Loan Date') ?></th>
                        <th scope="col"><?= __('Amount') ?></th>
                        <th scope="col"><?= __('Status') ?></th>
                    </tr>
                    <?php foreach ($account->customer->accounts as $accounts): ?>
                    <?php if($accounts->id != $account->id) : ?>
                        <?php if($accounts->status == 'opened') : ?>
                            <tr>
                                <td>
                                    <a href="<?= \Cake\Routing\Router::url(['controller' => 'Accounts', 'action' => 'ajaxview']) ?>/<?=$accounts->id?>" data-reveal-id="accountModal" data-reveal-ajax="true">
                                        <?php if($accounts->is_oldest) : ?>
                                            <b style="color: red"><?= $accounts->ref_no;?></b> (<?=$accounts->duration?> days)
                                        <?php else : ?>
                                            <?= $accounts->ref_no;?>
                                        <?php endif; ?>
                                    </a>
                                </td>
                                <td><?= $this->Time->format($accounts->loan_date, 'dd-MMM-yyyy') ?></td>
                                <td><?= h($accounts->amount) ?></td>
                                <td>
                                    <?php if($accounts->status == 'opened') : ?>
                                        <span class="label">Active</span>
                                    <?php else : ?>
                                        <span class="label alert">Closed</span>
                                    <?php endif;?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php endif;?>
</div>
<div id="accountModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<div id="customerModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<script type="text/javascript">
    $('#ref-no').on('keypress', function(e) {
        if (e.keyCode == 13) {
            $('#accountOpenFrm').submit();
            return false; // prevent the button click from happening
        }
    });
    $('#nextBtn').on('click', function(evt) {
        evt.preventDefault();
        var refNo = $('#ref-no').val();
        refNo = refNo == "" ? 0 : refNo;
        if(Math.floor(refNo) == refNo && $.isNumeric(refNo)) {
            refNo++;
        }

        if(refNo > 0) {
            $('#ref-no').val(refNo);
        }
        $('#accountOpenFrm').submit();
    });

    $('#prevBtn').on('click', function(evt) {
        evt.preventDefault();
        var refNo = $('#ref-no').val();
        refNo = refNo == "" ? 2 : refNo;
        if(Math.floor(refNo) == refNo && $.isNumeric(refNo)) {
            refNo--;
        }

        if(refNo > 0) {
            $('#ref-no').val(refNo);
        }
        $('#accountOpenFrm').submit();
    });
</script>