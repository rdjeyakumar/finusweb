<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Finus Web';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    
    <?= $this->Html->css('style.css') ?>
    
    <?= $this->Html->css('jquery-ui.css') ?>  
    <?= $this->Html->css('jquery-ui-timepicker-addon.css') ?>
    <?= $this->Html->css('fontawesome/css/all.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <?= $this->Html->script('jquery.js') ?>

    <?= $this->Html->script('modernizr.js') ?>

    <?= $this->Html->script('jquery-ui.js') ?>

    <?= $this->Html->script('jquery-ui-timepicker-addon.js') ?>
    
    <?= $this->fetch('script') ?>

    <style type="text/css">
        body {
                background: #C0C077;
            }
        <?php if($this->request->getParam('controller') == 'Customers') : ?>
            body {
                background: #ecf0f5;
            }

        <?php elseif($this->request->getParam('controller') == 'Accounts' && $this->request->getParam('action') != 'view') : ?>
            body {
                background: #C0C077;
            }
        <?php endif; ?>
    </style>
</head>
<body>
    <?php $loggedUser = $this->request->getSession()->read('Auth.User');?>
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <!-- <h1><a href=""><?= $this->fetch('title') ?></a></h1> -->
                <h1><a href="">FINUS Web</a></h1>
            </li>
        </ul>
        <section class="top-bar-section">
            <?php if(!empty($loggedUser)) : ?>
            <ul>
                <li class="has-dropdown">
                    <a href="#">Accounts</a>
                    <ul class="dropdown">
                        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?></li>
                        <li><?= $this->Html->link(__('Open Account'), ['controller' => 'Accounts', 'action' => 'view']) ?></li>
                        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?></li>
                    </ul>
                </li>
                <li class="has-dropdown">
                    <a href="#">Customers</a>
                    <ul class="dropdown">
                        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
                        <li><?= $this->Html->link(__('Open Customer'), ['controller' => 'Customers', 'action' => 'view']) ?></li>
                        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
                    </ul>
                </li>
                <?php if($user_role == 'admin') : ?>
                <li class="has-dropdown">
                    <a href="#">Users</a>
                    <ul class="dropdown">
                        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
                        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
                    </ul>
                </li>
                <?php endif;?>
                <li>
                    <?= $this->Html->link(__('Credit / Debit Transactions'), ['controller' => 'CrdrTransactions', 'action' => 'index']) ?>
                </li>
                <li class="has-dropdown">
                    <a href="#">Reports</a>
                    <ul class="dropdown">
                        <li><?= $this->Html->link(__('Receipts'), ['controller' => 'Receipts', 'action' => 'report']) ?></li>
                        <?php if($user_role == 'admin') : ?>
                            <li><?= $this->Html->link(__('Balance Sheet'), ['controller' => 'Receipts', 'action' => 'bsheet']) ?></li>
                            <li><?= $this->Html->link(__('Ledger'), ['controller' => 'CrdrTransactions', 'action' => 'ledger']) ?></li>
                            <li><?= $this->Html->link(__('OD List'), ['controller' => 'Accounts', 'action' => 'report']) ?></li>
                        <?php endif;?>
                    </ul>
                </li>
                <li class="has-dropdown">
                    <a href="#">Settings</a>
                    <ul class="dropdown">
                        <li><?= $this->Html->link(__('Profile'), ['controller' => 'Profile', 'action' => 'edit', 1]) ?></li>
                    </ul>
                </li>
            </ul>

            <ul class="right">
                <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout']) ?></li>
            </ul>
            <?php endif;?>
        </section>
    </nav>

    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>

    <?= $this->Html->script('foundation.min.js') ?>

    <?= $this->Html->script('foundation.topbar.js') ?>

    <?= $this->Html->script('foundation.reveal.js') ?>

    <?= $this->Html->script('foundation.dropdown.js') ?>

    <?= $this->Html->script('foundation.tab.js') ?>
    
    <script>
        $(document).foundation();//.foundation('joyride', 'start');
    </script>
</body>
</html>
