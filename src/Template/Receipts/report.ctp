<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
 */
?>

<div class="accounts index large-12 medium-12 columns content">
    <h3><?= __('Report - Receipts') ?></h3>
    <div class="filters">
        <?php echo $this->Form->create("Filter",array('class' => 'filter', 'type' => 'GET'));?>
        <div class="row">
            <div class="large-3 columns">
                <?php echo $this->Form->input("start_date", array('label' => 'From', 'value' => isset($_GET['start_date']) ? $_GET['start_date'] : ''));?>
            </div>

            <div class="large-3 columns">
                <?php echo $this->Form->input("end_date", array('label' => 'To', 'value' => isset($_GET['end_date']) ? $_GET['end_date'] : ''));?>
            </div>
            
            <div class="large-3 columns">
                <?php echo $this->Form->submit("Search", ['style' => 'margin-top: 18px', 'class'=>'button small info']);?>
            </div>

            <div class="large-3 columns">
                <h3>Count: <?= $this->Paginator->counter(['format' => __('{{count}}')]) ?></h3>
            </div>
        </div>
        <?php echo $this->Form->end();?>

    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th width="300">Customer</th>
                <th width="150">Acc Ref No</th>
                <th width="180">Date</th>
                <th width="80">Duration</th>
                <th width="100">Payment Type</th>
                <th width="100">Interest</th>
                <th width="100">Principal</th>
                <th width="150">Amount</th>

            </tr>
        </thead>
        <tbody>
            <?php foreach ($receipts as $receipt): ?>
            <tr>
                <td>
                    <?= h($receipt->account->customer->min_detail) ?>
                </td>
                <td>
                    <b><?= h($receipt->account->ref_no)?></b>
                    <?php if($receipt->account->status == 'opened') : ?>
                        <span class="label">Active</span>
                    <?php else : ?>
                        <span class="label alert">Closed</span>
                    <?php endif;?>
                </td>
                <td><?= $this->Time->format($receipt->pdate, 'dd-MMM-yyyy') ?></td>
                <td><?= $this->Number->format($receipt->duration)?></td>
                <td><?= h($receipt->payment_type)?></td>
                <td><?= $this->Number->format($receipt->interest)?></td>
                <td><?= $this->Number->format($receipt->principal)?></td>
                <td><?= $this->Number->format($receipt->amount)?></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<script>
    $( function() {
        if($( "#start-date" ).val() == '') {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }

        if($( "#end-date" ).val() == '') {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }
    });
</script>