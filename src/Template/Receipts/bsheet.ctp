<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
 */
?>

<div class="accounts index large-12 medium-12 columns content">
    <h3><?= __('Report - Balance Sheet') ?></h3>
    <div class="filters">
        <?php $base_url = array('controller' => 'Receipts', 'action' => 'bsheet'); ?>
        <?php echo $this->Form->create("Filter",array('url' => $base_url, 'class' => 'filter'));?>
        <div class="row">
            <div class="large-3 columns">
                <?php echo $this->Form->input("start_date", array('label' => 'From', 'placeholder' => ""));?>
            </div>

            <div class="large-3 columns">
                <?php echo $this->Form->input("end_date", array('label' => 'To', 'placeholder' => ""));?>
            </div>
            
            <div class="large-3 columns">
                <?php echo $this->Form->submit("Search", ['style' => 'margin-top: 18px', 'class'=>'button small info']);?>
            </div>

            <div class="large-3 columns">
                <h3>Count: <?=count($rows) ?></h3>
            </div>
        </div>
        <?php echo $this->Form->end();?>
    </div>
    
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th rowspan=2 width="120">Date</th>
                <th rowspan=2 width="80">Acc / Open</th>
                <th rowspan=2 width="80">Acc / Close</th>
                <th rowspan=2 width="80">Acc / Due, Interest</th>
                <th rowspan=2 width="80">Acc / Extra Amount</th>
                <th rowspan=2 width="280">Customer</th>
                <th style="text-align: center" colspan=2 width="150">Principal</th>
                <th style="text-align: center" width="75">Interest</th>
            </tr>
            <tr>
                <th style="text-align: center">Debit</th>
                <th style="text-align: center">Credit</th>
                <th style="text-align: center">Credit</th>
            </tr>
        </thead>
        <tbody style="max-height: 100px; overflow-y: auto;">
            <?php $pr_tot_debit = 0; $pr_tot_credit = 0; $in_tot_credit = 0; ?>
            <?php foreach ($rows as $row): ?>
            <tr>
                <td><?= $this->Time->format($row['date'], 'dd-MMM-yyyy') ?></td>
                <td><?= $row['ttype'] == 'loan' ? $row['ref_no'] : '-'; ?></td>
                <td><?= $row['ttype'] == 'settlement' ? $row['ref_no'] : '-'; ?></td>
                <td><?= $row['ttype'] == 'due' || $row['ttype'] == 'interest' ? $row['ref_no'] : '-'; ?></td>
                <td><?= $row['ttype'] == 'extra' ? $row['ref_no'] : '-'; ?></td>
                <td><?= $row['customer'];?></td>
                <td style="text-align: right; color: red;">
                <?php
                    if($row['ttype'] == 'loan' || $row['ttype'] == 'extra') { 
                        echo $this->Number->format($row['principal']);
                        $pr_tot_debit += $row['principal'];
                    } else {
                        echo "-";
                    }
                ?>
                </td>
                <td style="text-align: right; color: green;">
                <?php
                    if($row['ttype'] != 'loan' && $row['ttype'] != 'extra') { 
                        echo $this->Number->format($row['principal']);
                        $pr_tot_credit += $row['principal'];
                    } else {
                        echo "-";
                    }
                ?>
                </td>
                <td style="text-align: right; color: green;">
                <?php
                    if($row['ttype'] != 'loan' && $row['ttype'] != 'extra') { 
                        echo $this->Number->format($row['interest']);
                        $in_tot_credit += $row['interest'];
                    } else {
                        echo "-";
                    }
                ?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
        <tfoot>
            <th colspan="6"></th>
            <th style="text-align: right; color: red;"><?= $this->Number->format($pr_tot_debit);?></th>
            <th style="text-align: right; color: green;"><?= $this->Number->format($pr_tot_credit);?></th>
            <th style="text-align: right; color: green;"><?= $this->Number->format($in_tot_credit);?></th>
        </tfoot>
    </table>

    <div id="customerModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>

    <div id="accountModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
</div>

<script>
    $( function() {
        if($( "#start-date" ).val() == '') {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }

        if($( "#end-date" ).val() == '') {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }
    });
</script>