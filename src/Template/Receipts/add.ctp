<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Receipt $receipt
 */
?>
<div class="receipts form large-12 medium-12 columns content">
    
    <?= $this->Form->create($receipt, [
        'id' => 'receipt_form',
        'onsubmit' => "return confirm(\"Are you sure? \");"
    ]) ?>

    <fieldset>
        <legend>
            <?= 'Add Receipt' . " - Acc Ref No: " . $account->ajax_link ?>
            <?php if($account->status == 'opened') : ?>
                <span class="label default">Active</span>
            <?php else : ?>
                <span class="label alert">Closed</span>
            <?php endif;?>
            
            <span class="right">
                <?= $this->Html->link(("Back to view"), ['controller'=>'Accounts', 'action' => 'view', $account->id], []) ?>
            </span>
        </legend>

        <h3>Make Payment</h3>
        <div class="row">
            <div class="large-3 columns">
                <?php echo $this->Form->control('pdate', ['type'=>'date', 'label' => 'Payment Date']);?>
            </div>
            <div class="large-2 columns">
                <?php 
                    echo $this->Form->control('payment_type', [ 'type' => 'radio', 'value' => 'interest', 'options' => [
                        'interest' => 'Interest',
                        'due' => 'Due',
                        'extra' => 'Extra Amount (Loan)',
                        'settlement' => 'Settlement',
                    ]]);
                ?>
            </div>
            <div class="large-2 columns">
                <?php echo $this->Form->control('account_id', ['type' => 'hidden']);?>
                <?php echo $this->Form->control('interest_rate', ['id' => 'interest_rate', 'value' => $account->interest]);?>
            </div>
            <div class="large-1 columns">
                <?php echo $this->Form->control('duration', ['type'=>'text', 'readonly' => false]);?>
            </div>
            <div class="large-2 columns">
                <?php echo $this->Form->control('interest', ['type'=>'text', 'label' => 'Interest Amount']);?>
            </div>
            <div class="large-2 columns">
                <?php echo $this->Form->control('principal', ['type'=>'text']);?>
            </div>
            <div class="large-2 columns">
                <?php echo $this->Form->control('amount', ['type'=>'text', 'readonly'=>true]);?>
            </div>
            <div class="large-2 columns">
                <?= $this->Form->button('Save', ['class' => 'small', 'id'=>'submitBtn']) ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>

    <h3>Payables</h3>
    <table>
        <tr>
            <td>Loan Amount</td>
            <td><input type="text" id="td_loan_amount" readonly /></td>
            <td>Balance Principal</td>
            <td><input type="text" id="td_balance_principal" readonly /></td>
        </tr>
        <tr>
            <td>Duration</td>
            <td><input type="text" id="td_duration" readonly /></td>
            <td>Interest Amount <b>@ <?=$account->interest;?>%</b></td>
            <td><input type="text" id="td_interest" readonly /></td>
        </tr>
        <tr>
            <td>Paid Principal</td>
            <td><input type="text" id="td_paid_principal" readonly /></td>
            <td>Total Payables</td>
            <td><input type="text" id="td_total_payable" readonly /></td>
        </tr>
    </table>

    </table>

    <h3>Payment History</h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pdate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('payment_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('duration') ?></th>
                <th scope="col"><?= $this->Paginator->sort('interest') ?></th>
                <th scope="col"><?= $this->Paginator->sort('principal') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
                <th scope="col">Created</th>
                <th scope="col">Modified</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($receipts as $key => $receipt): ?>
            <tr>
                <td><?= $this->Number->format($receipt->id) ?></td>
                <td><?= $this->Time->format($receipt->pdate, 'dd-MMM-yyyy') ?></td>
                <td><?= h($receipt->payment_type) ?></td>
                <td><?= $this->Number->format($receipt->duration) ?></td>
                <td><?= $this->Number->format($receipt->interest) ?></td>
                <td><?= $this->Number->format($receipt->principal) ?></td>
                <td><?= $this->Number->format($receipt->amount) ?></td>
                <td><?= $this->Time->format($receipt->created, 'dd-MMM-yyyy') ?></td>
                <td><?= $this->Time->format($receipt->modified, 'dd-MMM-yyyy') ?></td>
                <td class="actions">
                    <?php if($user_role == 'admin') : ?>
                    <?php if($key == 0 && $receipt->payment_type != 'loan') : ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $receipt->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receipt->id)]) ?>
                    <?php endif;?>
                    <?php endif;?>
                </td>
                <td>
                    <?=$receipt->created_user ? $receipt->created_user->username : '';?>
                </td>
                <td>
                    <?=$receipt->modified_user ? $receipt->modified_user->username : '';?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div id="accountModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
</div>

<script type="text/javascript">
    $("select[name='pdate[year]'], select[name='pdate[month]'], select[name='pdate[day]'], input[name='payment_type']").on("change", calcInterest);

    $("#interest_rate, #duration").on("keyup", calcLocally);

    $("#principal, #interest").on('keyup', function(evt) {
        updateCalcValues();
    });

    function calcInterest() {
        var account_id = $("#account-id").val();
        var pdate = $("select[name='pdate[year]']").val() + "-" + $("select[name='pdate[month]']").val() + "-" + $("select[name='pdate[day]']").val();
        console.log(pdate);
        $.post("<?= \Cake\Routing\Router::url(array('controller' => 'Receipts', 'action' => 'calcInterest'));?>",
        {
            account_id: account_id,
            pdate: pdate
        },
        function(result, status) {
            var data = JSON.parse(result);
            $("#duration").val(data.duration);
            $("#interest").val(data.interest.toFixed(2));
            $("#principal").val(data.balance_principal.toFixed(2));
            displayProperFields();
            displayPayables(data);
        });
    }

    function updateCalcValues() {
        var principal = $("#principal").val();
        var interest = $("#interest").val();
        var amount = parseFloat(principal) + parseFloat(interest);
        $("#amount").val(amount.toFixed(2));
    }

    function displayProperFields() {
        $("#interest_rate").closest("div").show();
        $("#interest").closest("div").show();
        $("#duration").closest("div").show();
        $("#principal").closest("div").show();
        if($("#payment-type-interest").is(":checked")) {
            $("#principal").val("0");
            $("#principal").closest("div").hide();
        } else if($("#payment-type-due").is(":checked")) {
            //$("#interest").val("0");
            //$("#duration").val("0");
            $("#principal").val("0");

            //$("#interest_rate").closest("div").hide();
            //$("#interest").closest("div").hide();
            //$("#duration").closest("div").hide();
        } else if($("#payment-type-extra").is(":checked")) {
            $("#interest").val("0");
            $("#duration").val("0");
            $("#principal").val("0");

            $("#interest_rate").closest("div").hide();
            $("#interest").closest("div").hide();
            $("#duration").closest("div").hide();
        } else if($("#payment-type-settlement").is(":checked")) {
            
        }

        updateCalcValues();
    }

    function displayPayables(data) {
        $("#td_loan_amount").val(data.loan_amount.toFixed(2));
        $("#td_duration").val(data.duration);
        $("#td_paid_principal").val(data.paid_principal.toFixed(2));
        $("#td_balance_principal").val(data.balance_principal.toFixed(2));
        $("#td_interest").val(data.interest.toFixed(2));

        var total_payable = parseFloat(data.balance_principal) + parseFloat(data.interest);
        $("#td_total_payable").val(total_payable.toFixed(2));
    }

    function calcLocally() {
        var interest_rate = parseFloat($("#interest_rate").val());
        var duration = parseFloat($("#duration").val());
        var balance_principal = parseFloat($("#td_balance_principal").val());

        var interest = balance_principal * (interest_rate / 100)  * (duration / 30);
        $("#interest").val(interest.toFixed(2));
        updateCalcValues();
    }

    calcInterest();

    $(document).ready(function(){
        <?php if($account->status == 'closed') : ?>
            $("#receipt_form :input").prop("disabled", true);
        <?php endif;?>
    });
</script>