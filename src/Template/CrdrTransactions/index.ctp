<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CrdrTransaction[]|\Cake\Collection\CollectionInterface $crdrTransactions
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Crdr Transaction'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="crdrTransactions index large-10 medium-9 columns content">
    <h3><?= __('Crdr Transactions') ?></h3>

    <div class="filters">
        <?php echo $this->Form->create("Filter",array('class' => 'filter', 'type' => 'GET'));?>
        <div class="row">
            <div class="large-3 columns">
                <?php echo $this->Form->input("start_date", array('label' => 'From', 'value' => isset($_GET['start_date']) ? $_GET['start_date'] : ''));?>
            </div>

            <div class="large-3 columns">
                <?php echo $this->Form->input("end_date", array('label' => 'To', 'value' => isset($_GET['end_date']) ? $_GET['end_date'] : ''));?>
            </div>
            
            <div class="large-6 columns">
                <?php echo $this->Form->submit("Search", ['style' => 'margin-top: 18px', 'class'=>'button small info']);?>
            </div>
        </div>
        <?php echo $this->Form->end();?>
    </div>

    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Date</th>
                <th scope="col">Title</th>
                <th scope="col">Remarks</th>
                <th scope="col">Debit</th>
                <th scope="col">Credit</th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $debitSum = 0; ?>
            <?php $creditSum = 0; ?>
            <?php foreach ($crdrTransactions as $crdrTransaction): ?>
            <tr>
                <td><?= $this->Number->format($crdrTransaction->id) ?></td>
                <td><?= $this->Time->format($crdrTransaction->transaction_date, 'dd-MMM-yyyy') ?></td>
                <td><?= h($crdrTransaction->transaction_title) ?></td>
                <td><?= h($crdrTransaction->transaction_remarks) ?></td>
                <td style="color: red;"><?= ($crdrTransaction->transaction_type == 'debit') ? $this->Number->format($crdrTransaction->transaction_amount) : ''; ?></td>
                <td style="color: green;"><?= ($crdrTransaction->transaction_type == 'credit') ? $this->Number->format($crdrTransaction->transaction_amount) : ''; ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $crdrTransaction->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $crdrTransaction->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $crdrTransaction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $crdrTransaction->id)]) ?>
                </td>
            </tr>
            <?php $debitSum += ($crdrTransaction->transaction_type == 'debit') ? $crdrTransaction->transaction_amount : 0; ?>
            <?php $creditSum += ($crdrTransaction->transaction_type == 'credit') ? $crdrTransaction->transaction_amount : 0; ?>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col">Total</th>
                <th scope="col"><?= $this->Number->format($debitSum) ?></th>
                <th scope="col"><?= $this->Number->format($creditSum) ?></th>
                <th scope="col">Bal: <?= $this->Number->format($creditSum - $debitSum) ?></th>
            </tr>
        </tfoot>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<script>
    $( function() {
        if($( "#start-date" ).val() == '') {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }

        if($( "#end-date" ).val() == '') {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }
    });
</script>