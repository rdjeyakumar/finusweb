<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CrdrTransaction $crdrTransaction
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Crdr Transaction'), ['action' => 'edit', $crdrTransaction->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Crdr Transaction'), ['action' => 'delete', $crdrTransaction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $crdrTransaction->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Crdr Transactions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Crdr Transaction'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="crdrTransactions view large-9 medium-8 columns content">
    <h3><?= h($crdrTransaction->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Transaction Type') ?></th>
            <td><?= h($crdrTransaction->transaction_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Transaction Title') ?></th>
            <td><?= h($crdrTransaction->transaction_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Transaction Remarks') ?></th>
            <td><?= h($crdrTransaction->transaction_remarks) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($crdrTransaction->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Transaction Amount') ?></th>
            <td><?= $this->Number->format($crdrTransaction->transaction_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Transaction Date') ?></th>
            <td><?= h($crdrTransaction->transaction_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($crdrTransaction->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($crdrTransaction->modified) ?></td>
        </tr>
    </table>
</div>
