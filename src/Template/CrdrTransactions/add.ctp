<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CrdrTransaction $crdrTransaction
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Crdr Transactions'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="crdrTransactions form large-10 medium-9 columns content">
    <?= $this->Form->create($crdrTransaction) ?>
    <fieldset>
        <legend><?= __('Add Crdr Transaction') ?></legend>
        <?php
            echo $this->Form->control('transaction_date', ['empty' => true, 'type' => 'date']);
            echo $this->Form->control('transaction_type', ['options' => ['debit' => 'பற்று (Debit)', 'credit' => 'வரவு (Credit)']]);
            echo $this->Form->control('transaction_title');
            echo $this->Form->control('transaction_amount');
            echo $this->Form->control('transaction_remarks', ['type' => 'textarea']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script>
 $(function() {
    function fetchAutoCompleteData() {
        $.get("<?= \Cake\Routing\Router::url(array('controller' => 'CrdrTransactions', 'action' => 'autoCompleteData'));?>",
        function(result, status) {
            var data = JSON.parse(result);
            $( "#transaction-title" ).autocomplete({
               source: data.transaction_titleList
            });
        });
    }
    fetchAutoCompleteData();
 });
</script>