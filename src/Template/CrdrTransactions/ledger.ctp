<?php

?>

<div class="large-12 columns content">
	<h3><?= __('Ledger') ?></h3>

    <div class="filters">
        <?php echo $this->Form->create("Filter",array('class' => 'filter', 'type' => 'GET'));?>
        <div class="row">
            <div class="large-3 columns">
                <?php echo $this->Form->input("start_date", array('label' => 'From', 'value' => isset($_GET['start_date']) ? $_GET['start_date'] : ''));?>
            </div>

            <div class="large-3 columns">
                <?php echo $this->Form->input("end_date", array('label' => 'To', 'value' => isset($_GET['end_date']) ? $_GET['end_date'] : ''));?>
            </div>
            
            <div class="large-3 columns">
                <?php echo $this->Form->submit("Search", ['style' => 'margin-top: 18px', 'class'=>'button small info']);?>
            </div>

            <div class="large-3 columns">
                <h3>Count: <?=count($rows) ?></h3>
            </div>
        </div>
        <?php echo $this->Form->end();?>
    </div>

	<?php $i = 1; ?>
	<table cellpadding="0" cellspacing="0">
        <thead>
            <tr style="background-color: #ebebec;">
                <th>S.No</th>
                <th>Date</th>
                <th>Description</th>
                <th>Debit</th>
                <th>Credit</th>
                <th>Balance</th>
            </tr>
            <tr style="background-color: #ebebec;">
                <th><?= $i++ ?></th>
                <th><?= $this->Time->format($opening_balance['trdate'], 'dd-MMM-yyyy') ?></th>
                <th><?= h($opening_balance['description']) ?></th>
                <th style="color: red;"><?= $this->Number->format($opening_balance['debit']) ?></th>
                <th style="color: green;"><?= $this->Number->format($opening_balance['credit']) ?></th>
                <th><?= $this->Number->format($opening_balance['balance']) ?></th>
            </tr>
        </thead>
        <tbody>
        	<?php $balance = isset($opening_balance['balance']) && !empty($opening_balance['balance']) ? $opening_balance['balance'] : 0; ?>
            <?php foreach ($rows as $row): ?>
            	<tr>
	                <th><?= $i++ ?></th>
	                <th><?= $this->Time->format($row['trdate'], 'dd-MMM-yyyy') ?></th>
	                <th><?= h($row['description']) ?></th>
	                <th style="color: red;"><?= $this->Number->format($row['debit']) ?></th>
	                <th style="color: green;"><?= $this->Number->format($row['credit']) ?></th>
	                <?php $balance += $row['credit'] - $row['debit']; ?>
	                <th><?= $this->Number->format($balance) ?></th>
	            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr style="background-color: #ebebec;">
                <th><?= $i++ ?></th>
                <th><?= $this->Time->format($closing_balance['trdate'], 'dd-MMM-yyyy') ?></th>
                <th><?= h($closing_balance['description']) ?></th>
                <th style="color: red;"><?= $this->Number->format($closing_balance['debit']) ?></th>
                <th style="color: green;"><?= $this->Number->format($closing_balance['credit']) ?></th>
                <th><?= $this->Number->format($closing_balance['balance']) ?></th>
            </tr>
        </tfoot>
    </table>
</div>

<script>
    $( function() {
        if($( "#start-date" ).val() == '') {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#start-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }

        if($( "#end-date" ).val() == '') {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker('setDate', new Date());
        } else {
            $( "#end-date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }
    });
</script>