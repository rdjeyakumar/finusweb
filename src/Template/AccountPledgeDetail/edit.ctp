<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccountPledgeDetail $accountPledgeDetail
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $accountPledgeDetail->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $accountPledgeDetail->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Account Pledge Detail'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="accountPledgeDetail form large-9 medium-8 columns content">
    <?= $this->Form->create($accountPledgeDetail) ?>
    <fieldset>
        <legend><?= __('Edit Account Pledge Detail') ?></legend>
        <?php
            echo $this->Form->control('account_id', ['options' => $accounts, 'empty' => true]);
            echo $this->Form->control('pledge_id');
            echo $this->Form->control('pledge_date', ['empty' => true]);
            echo $this->Form->control('pledge_vendor');
            echo $this->Form->control('pledge_amount');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
