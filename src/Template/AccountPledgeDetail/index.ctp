<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccountPledgeDetail[]|\Cake\Collection\CollectionInterface $accountPledgeDetail
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Account Pledge Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="accountPledgeDetail index large-9 medium-8 columns content">
    <h3><?= __('Account Pledge Detail') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('account_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pledge_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pledge_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pledge_vendor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pledge_amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($accountPledgeDetail as $accountPledgeDetail): ?>
            <tr>
                <td><?= $this->Number->format($accountPledgeDetail->id) ?></td>
                <td><?= $accountPledgeDetail->has('account') ? $this->Html->link($accountPledgeDetail->account->id, ['controller' => 'Accounts', 'action' => 'view', $accountPledgeDetail->account->id]) : '' ?></td>
                <td><?= h($accountPledgeDetail->pledge_id) ?></td>
                <td><?= h($accountPledgeDetail->pledge_date) ?></td>
                <td><?= h($accountPledgeDetail->pledge_vendor) ?></td>
                <td><?= $this->Number->format($accountPledgeDetail->pledge_amount) ?></td>
                <td><?= h($accountPledgeDetail->created) ?></td>
                <td><?= h($accountPledgeDetail->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $accountPledgeDetail->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $accountPledgeDetail->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $accountPledgeDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountPledgeDetail->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
