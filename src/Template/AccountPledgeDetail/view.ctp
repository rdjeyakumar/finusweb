<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccountPledgeDetail $accountPledgeDetail
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Account Pledge Detail'), ['action' => 'edit', $accountPledgeDetail->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Account Pledge Detail'), ['action' => 'delete', $accountPledgeDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountPledgeDetail->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Account Pledge Detail'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Account Pledge Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="accountPledgeDetail view large-9 medium-8 columns content">
    <h3><?= h($accountPledgeDetail->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Account') ?></th>
            <td><?= $accountPledgeDetail->has('account') ? $this->Html->link($accountPledgeDetail->account->id, ['controller' => 'Accounts', 'action' => 'view', $accountPledgeDetail->account->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pledge Id') ?></th>
            <td><?= h($accountPledgeDetail->pledge_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pledge Vendor') ?></th>
            <td><?= h($accountPledgeDetail->pledge_vendor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($accountPledgeDetail->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pledge Amount') ?></th>
            <td><?= $this->Number->format($accountPledgeDetail->pledge_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pledge Date') ?></th>
            <td><?= h($accountPledgeDetail->pledge_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($accountPledgeDetail->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($accountPledgeDetail->modified) ?></td>
        </tr>
    </table>
</div>
