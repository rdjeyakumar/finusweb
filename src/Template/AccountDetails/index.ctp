<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccountDetail[]|\Cake\Collection\CollectionInterface $accountDetails
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Account Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Properties'), ['controller' => 'Properties', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Property'), ['controller' => 'Properties', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="accountDetails index large-9 medium-8 columns content">
    <h3><?= __('Account Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('account_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('property_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('net_weight') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_weight') ?></th>
                <th scope="col"><?= $this->Paginator->sort('property_value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($accountDetails as $accountDetail): ?>
            <tr>
                <td><?= $this->Number->format($accountDetail->id) ?></td>
                <td><?= $accountDetail->has('account') ? $this->Html->link($accountDetail->account->id, ['controller' => 'Accounts', 'action' => 'view', $accountDetail->account->id]) : '' ?></td>
                <td><?= $accountDetail->has('property') ? $this->Html->link($accountDetail->property->name, ['controller' => 'Properties', 'action' => 'view', $accountDetail->property->id]) : '' ?></td>
                <td><?= $this->Number->format($accountDetail->net_weight) ?></td>
                <td><?= $this->Number->format($accountDetail->total_weight) ?></td>
                <td><?= $this->Number->format($accountDetail->property_value) ?></td>
                <td><?= h($accountDetail->created) ?></td>
                <td><?= h($accountDetail->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $accountDetail->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $accountDetail->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $accountDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountDetail->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
