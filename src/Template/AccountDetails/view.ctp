<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccountDetail $accountDetail
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Account Detail'), ['action' => 'edit', $accountDetail->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Account Detail'), ['action' => 'delete', $accountDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountDetail->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Account Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Account Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Properties'), ['controller' => 'Properties', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Property'), ['controller' => 'Properties', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="accountDetails view large-9 medium-8 columns content">
    <h3><?= h($accountDetail->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Account') ?></th>
            <td><?= $accountDetail->has('account') ? $this->Html->link($accountDetail->account->id, ['controller' => 'Accounts', 'action' => 'view', $accountDetail->account->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Property') ?></th>
            <td><?= $accountDetail->has('property') ? $this->Html->link($accountDetail->property->name, ['controller' => 'Properties', 'action' => 'view', $accountDetail->property->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($accountDetail->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Net Weight') ?></th>
            <td><?= $this->Number->format($accountDetail->net_weight) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Weight') ?></th>
            <td><?= $this->Number->format($accountDetail->total_weight) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Property Value') ?></th>
            <td><?= $this->Number->format($accountDetail->property_value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($accountDetail->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($accountDetail->modified) ?></td>
        </tr>
    </table>
</div>
