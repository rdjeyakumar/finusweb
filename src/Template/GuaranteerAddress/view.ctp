<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GuaranteerAddres $guaranteerAddres
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Guaranteer Addres'), ['action' => 'edit', $guaranteerAddres->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Guaranteer Addres'), ['action' => 'delete', $guaranteerAddres->id], ['confirm' => __('Are you sure you want to delete # {0}?', $guaranteerAddres->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Guaranteer Address'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Guaranteer Addres'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="guaranteerAddress view large-9 medium-8 columns content">
    <h3><?= h($guaranteerAddres->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Customer') ?></th>
            <td><?= $guaranteerAddres->has('customer') ? $this->Html->link($guaranteerAddres->customer->id, ['controller' => 'Customers', 'action' => 'view', $guaranteerAddres->customer->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($guaranteerAddres->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mobile') ?></th>
            <td><?= h($guaranteerAddres->mobile) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($guaranteerAddres->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Door No') ?></th>
            <td><?= h($guaranteerAddres->door_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Street') ?></th>
            <td><?= h($guaranteerAddres->street) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Area') ?></th>
            <td><?= h($guaranteerAddres->area) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Village') ?></th>
            <td><?= h($guaranteerAddres->village) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City') ?></th>
            <td><?= h($guaranteerAddres->city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pincode') ?></th>
            <td><?= h($guaranteerAddres->pincode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($guaranteerAddres->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($guaranteerAddres->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($guaranteerAddres->modified) ?></td>
        </tr>
    </table>
</div>
