<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GuaranteerAddres[]|\Cake\Collection\CollectionInterface $guaranteerAddress
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Guaranteer Addres'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="guaranteerAddress index large-9 medium-8 columns content">
    <h3><?= __('Guaranteer Address') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('customer_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mobile') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('door_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('street') ?></th>
                <th scope="col"><?= $this->Paginator->sort('area') ?></th>
                <th scope="col"><?= $this->Paginator->sort('village') ?></th>
                <th scope="col"><?= $this->Paginator->sort('city') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pincode') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($guaranteerAddress as $guaranteerAddres): ?>
            <tr>
                <td><?= $this->Number->format($guaranteerAddres->id) ?></td>
                <td><?= $guaranteerAddres->has('customer') ? $this->Html->link($guaranteerAddres->customer->id, ['controller' => 'Customers', 'action' => 'view', $guaranteerAddres->customer->id]) : '' ?></td>
                <td><?= h($guaranteerAddres->name) ?></td>
                <td><?= h($guaranteerAddres->mobile) ?></td>
                <td><?= h($guaranteerAddres->phone) ?></td>
                <td><?= h($guaranteerAddres->door_no) ?></td>
                <td><?= h($guaranteerAddres->street) ?></td>
                <td><?= h($guaranteerAddres->area) ?></td>
                <td><?= h($guaranteerAddres->village) ?></td>
                <td><?= h($guaranteerAddres->city) ?></td>
                <td><?= h($guaranteerAddres->pincode) ?></td>
                <td><?= h($guaranteerAddres->created) ?></td>
                <td><?= h($guaranteerAddres->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $guaranteerAddres->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $guaranteerAddres->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $guaranteerAddres->id], ['confirm' => __('Are you sure you want to delete # {0}?', $guaranteerAddres->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
