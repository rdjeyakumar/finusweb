<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>

<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th width="80">Cust-ID</th>
            <th width="400">Customer</th>
            <th width="100">Acc History</th>
            <th width="50"></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($customers as $customer): ?>
        <tr>
            <td><?= $this->Number->format($customer->id) ?></td>
            <td>
                <b><?= h($customer->firstname); ?> <?= h($customer->lastname); ?></b> 
                <?php if($customer->customer_addres) : ?>
                <?= h($customer->customer_addres->mobile)?><br/>
                <?= h($customer->customer_addres->door_no)?>, 
                <?= h($customer->customer_addres->street)?>, 
                <?= h($customer->customer_addres->area)?>, 
                <?= h($customer->customer_addres->village)?>, 
                <?= h($customer->customer_addres->city)?>, 
                <?= h($customer->customer_addres->pincode)?>
                <?php endif; ?>
            </td>
            <td>
                <?php 
                    $histories = $customer->getCustomerHistory();
                    $totalHis = 0;
                    $oldestHis = 0;
                    foreach ($histories as $key => $history) {
                        if($history->is_oldest) {
                            $oldestHis++;
                        }
                        $totalHis++;
                    }
                ?>
                <?php if($oldestHis > 0) : ?>
                    <span class="label alert"><?=$oldestHis?> of <?=$totalHis?> Account(s)</span>
                <?php else : ?>
                    <span class="label info"><?=$totalHis?> Account(s)</span>
                <?php endif;?>
            </td>
            <td class="actions" style="text-align: center;">
                <?php echo $this->Form->submit('Click me', ['class'=>'customer_btn', 'data-id'=>$customer->id]);?>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>