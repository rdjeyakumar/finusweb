<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer $customer
 */
?>

<div class="customers form large-12 medium-12 columns content">
    <?= $this->Form->create($customer, [
        'type' => 'file',
        'horizontal' => true,
        'onsubmit' => "return confirm(\"Are you sure? \");"
    ]) ?>
    <fieldset>
        <legend><?= __('Add Customer') ?></legend>
        <div id="page1">
            <div class="row">
                <h4>Customer Details</h4>

                <div class="large-2 columns" id="fileDiv1">
                    <div id="imgDiv1" style="width: 150px; height: 175px; border: 1px solid #000;">
                        <!-- <img id="img1" src="" style="width: 100%; height: 100%" /> -->
                        <?=$this->Html->image('noimage.jpg', ['id' => 'img1', 'style' => 'width: 100%; height: 100%']);?>
                    </div>
                    <?php echo $this->Form->control('photo', ['type' => 'file']);?>
                </div>

                <div class="large-4 columns">
                    <?php echo $this->Form->control('id', ['type'=>'hidden']);?>
                    <?php echo $this->Form->control('firstname');?>
                    <?php echo $this->Form->control('lastname');?>
                    <div class="row">
                        <div class="large-6 columns">
                            <?php echo $this->Form->control('gender', ['options' => ['' => 'Select', 'M' => 'Male', 'F' => 'Female']]);?>
                        </div>
                        <div class="large-6 columns">
                            <?php echo $this->Form->control('father_or_spouse', ['label' => 'Relation', 'options' => ['' => 'Select', 'F' => 'Father', 'S' => 'Spouse']]);?>
                        </div>
                    </div>
                </div>

                <div class="large-4 columns">
                    <?php //echo $this->Form->control('identity_file', ['type' => 'file']);?>
                    <?php echo $this->Form->control('identity_type', ['options' => [
                        'Aadhaar'=>'Aadhaar',
                        'Bank Passbook'=>'Bank Passbook',
                        'Driving Licence'=>'Driving Licence',
                        'Voter ID'=>'Voter ID',
                        'Others'=>'Others'
                    ]]);?>
                    <?php echo $this->Form->control('identity_code');?>
                </div>

                <div class="large-2 columns" id="fileDiv1">
                    <div id="imgDiv2" style="width: 150px; height: 175px; border: 1px solid #000;">
                        <!-- <img id="img1" src="" style="width: 100%; height: 100%" /> -->
                        <?=$this->Html->image('noimage.jpg', ['id' => 'img2', 'style' => 'width: 100%; height: 100%']);?>
                    </div>
                    <?php echo $this->Form->control('identity_file', ['type' => 'file']);?>
                </div>
            </div>

            <div class="row">
                <h5>Address</h5>
                <div class="large-2 columns">
                    <?php echo $this->Form->control('customer_addres.door_no');?>
                </div>

                <div class="large-5 columns">
                    <?php echo $this->Form->control('customer_addres.street');?>
                </div>

                <div class="large-5 columns">
                    <?php echo $this->Form->control('customer_addres.area');?>
                </div>
            </div>

            <div class="row">
                <div class="large-5 columns">
                    <?php echo $this->Form->control('customer_addres.village');?>
                </div>

                <div class="large-5 columns">
                    <?php echo $this->Form->control('customer_addres.city');?>
                </div>

                <div class="large-2 columns">
                    <?php echo $this->Form->control('customer_addres.pincode');?>
                </div>
            </div>

            <div class="row">
                <div class="large-6 columns">
                    <?php echo $this->Form->control('customer_addres.mobile');?>
                </div>

                <div class="large-6 columns">
                    <?php echo $this->Form->control('customer_addres.phone', ['label' => 'Mobile 2']);?>
                </div>
            </div>
        </div>

        <h4 id="guaranteer_h4">Guaranteer Details 
            <a href="#" data-reveal-id="myModal1">Load Customer</a> OR 
            <a href="#" onclick="loadGuaranteerForm()">New Customer</a>
        </h4>

        <?= $this->element('guaranteer_form'); ?>

    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>

    <div id="myModal1" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <h2 id="modalTitle1">Customers</h2>
        <form onsubmit="return false;">
            <div class="row">
                <div class="small-6 columns">
                  <?php echo $this->Form->input("search1", ['label' => 'Search']);?>
                </div>
                <div class="small-4 columns">
                    <?php echo $this->Form->input("search_by1", ['label' => 'By', 'options' => [
                        '' => 'Any',
                        'Customers.id' => 'Customer ID',
                        'firstname' => 'First name',
                        'lastname' => 'Last name',
                        'idendity_code' => 'Identity Code',
                        'CustomerAddress.mobile' => 'Mobile',
                        'CustomerAddress.phone' => 'Phone',
                        'CustomerAddress.door_no' => 'Door No',
                        'CustomerAddress.street' => 'Street',
                        'CustomerAddress.area' => 'Area',
                        'CustomerAddress.village' => 'Village',
                        'CustomerAddress.City' => 'City',
                        'CustomerAddress.pincode' => 'Pin Code'
                    ]]);?>
                </div>
                <div class="small-2 columns">
                    <button id="search_btn1" style="margin-top: 12px;">GO</button>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns" id="search_results1">
                    <table>
                        <thead>
                            <tr>
                                <th width="80">Cust-ID</th>
                                <th width="50">Customer</th>
                                <th width="300"></th>
                                <th width="200">Acc History</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
</div>
<!-- GURANTEER FORM START -->
<script type="text/javascript">
    function showGuaranteer() {
        var opt = $('#show_guaranteer').is(':checked');
        if(opt) {
            loadGuaranteerForm();
        } else {
            $("#page2").empty();
        }
    }

    function fetchGuaranteer() {
        $.post("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'search'));?>",
        {
            search: $("#search1").val(),
            search_by: $("#search-by1").val()
        },
        function(data, status) {
            $("#search_results1").html(data);
        });
    }

    function loadGuaranteerForm() {
        $.get("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'guaranteer_form'));?>",
        function(data, status) {
            $("#page2").html(data);
        });
    }

    $("button#search_btn1").click(fetchGuaranteer);

    $("#search1").on('keyup', fetchGuaranteer);

    $('#myModal1').on('click', 'input.customer_btn', function() {
        var custId = $(this).attr('data-id');
        $("#page2").empty();
        $("#guaranteer_h4").show();
        $('#myModal1').foundation('reveal', 'close');

        $.get("<?= \Cake\Routing\Router::url(array('controller' => 'Customers', 'action' => 'loadview'));?>/"+custId,
        function(data, status) {
            $("#page2").html(data);
            $("#page2").prepend('<input type="hidden" name="guaranteer_id" id="guaranteer-id" value="' +custId+ '">');
        });
    });

    <?php if(!$customer->guaranteer) : ?>
        $("#page2").empty();
    <?php endif; ?>
</script>
<!-- GURANTEER FORM END -->

<!-- Preview Image -->
<script type="text/javascript">
    function readURL(input, previewDiv) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#'+previewDiv).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#photo").change(function() {
        readURL(this, "img1");
    });

    $("#identity-file").change(function() {
        readURL(this, "img2");
    });
</script>
<!-- Preview Image ends -->