<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer $customer
 */
?>

<div class="customers view large-12 medium-12 columns panel">
    <h3>Open Customer</h3>
    <?php $base_url = array('controller' => 'Customers', 'action' => 'view'); ?>
    <?php echo $this->Form->create("Filter",array('url' => $base_url, 'class' => 'filter', 'id'=>'customerOpenFrm', 'type' => 'GET'));?>
    <div class="row">
        <div class="large-3 columns">
            <div class="row collapse">
                <div class="small-2 columns">
                    <?php echo $this->Form->button("-", ['class'=>'button prefix', 'id'=>'prevBtn']);?>
                </div>
                <div class="small-8 columns">
                    <?php echo $this->Form->input("id", array('label' => false, 'placeholder' => "Customer ID", "value"=>isset($_GET['id']) ? $_GET['id'] : ""));?>
                </div>
                <div class="small-2 columns">
                    <?php echo $this->Form->button("+", ['class'=>'button postfix', 'id'=>'nextBtn']);?>
                </div>
            </div>
        </div>

        <div class="small-2 columns">
            <?php echo $this->Form->submit("Go", ['class'=>'button']);?>
        </div>

        <div class="large-7 columns">&nbsp;</div>
    </div>

    <?php if(!$customer) : ?>
        <p>No records found</p>
    <?php else : ?>
    <h3>
        <?= h($customer->firstname) ?> <span>(ID-<?= h($customer->id) ?>)</span> 
        <?= $this->Html->link(('<i class="fa fa-print"></i>' . ""), ['controller' => 'Customers', 'action' => 'print', $customer->id], ['escape'=>false, 'class'=>'label info', 'target' => '_new']) ?>
        <?= $this->Html->link('<i class="fa fa-pencil"></i>' . ' Edit', ['action' => 'edit', $customer->id], ['escape'=>false, 'class'=>'label success']) ?>
        <?php if($user_role == 'admin') : ?>
        <?= $this->Form->postLink('<i class="fa fa-trash-o"></i>' . ' Delete', ['action' => 'delete', $customer->id], ['escape'=>false, 'class'=>'label alert', 'confirm' => __('Are you sure you want to delete # {0}?', $customer->id)]) ?>
        <?php endif;?>
    </h3>
    <div class="row">
        <div class="large-2 columns">
            <table>
                <thead>
                    <tr><th width="100">Photo</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php if($customer->photo && file_exists("files/Customers/photo/" . $customer->photo)) : ?>
                                <?php echo $this->Html->image(
                                    "../files/Customers/photo/" . $customer->photo, [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php else : ?>
                                <?php echo $this->Html->image(
                                    "noimage.jpg", [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php endif;?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr><th width="100">Identity File</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php if($customer->identity_file && file_exists("files/Customers/identity_file/" . $customer->identity_file)) : ?>
                                <?php echo $this->Html->image(
                                    "../files/Customers/identity_file/" . $customer->identity_file, [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php else : ?>
                                <?php echo $this->Html->image(
                                    "noimage.jpg", [
                                    "class" => "thumbnail"
                                ]);?>
                            <?php endif;?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="large-5 columns">
            <table>
                <colgroup>
                    <col width="120">
                    <col>
                </colgroup>
                <thead>
                    <tr><th colspan="2">Customer</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ID</td>
                        <td><b><?=h($customer->id)?></b></td>
                    </tr>
                    <tr>
                        <td>FirstName</td>
                        <td><b><?=h($customer->firstname)?></b></td>
                    </tr>
                    <tr>
                        <td>LastName</td>
                        <td><b><?=h($customer->lastname)?></b></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td><b><?=($customer->gender == 'M') ? 'Male' : 'Female'?></b></td>
                    </tr>
                    <tr>
                        <td>Identity Type</td>
                        <td><b><?=h($customer->identity_type)?></b></td>
                    </tr>
                    <tr>
                        <td>Identity Code</td>
                        <td><b><?=h($customer->identity_code)?></b></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="large-5 columns">
            <table>
                <colgroup>
                    <col width="120">
                    <col>
                </colgroup>
                <thead>
                    <tr><th colspan="2">Customer Address</th></tr>
                </thead>
                <tbody>
                    <?php if($customer->customer_addres) : ?>
                    <tr>
                        <td>Mobile</td>
                        <td><b><?=h($customer->customer_addres->mobile)?></b></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><b><?=h($customer->customer_addres->phone)?></b></td>
                    </tr>
                    <tr>
                        <td>Door No</td>
                        <td><b><?=h($customer->customer_addres->door_no)?></b></td>
                    </tr>
                    <tr>
                        <td>Street</td>
                        <td><b><?=h($customer->customer_addres->street)?></b></td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td><b><?=h($customer->customer_addres->area)?></b></td>
                    </tr>
                    <tr>
                        <td>Village</td>
                        <td><b><?=h($customer->customer_addres->village)?></b></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><b><?=h($customer->customer_addres->city)?></b></td>
                    </tr>
                    <tr>
                        <td>Pincode</td>
                        <td><b><?=h($customer->customer_addres->pincode)?></b></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <?php if($customer->guaranteer) : ?>
        <div class="large-5 columns">
            <table>
                <colgroup>
                    <col width="120">
                    <col>
                </colgroup>
                <thead>
                    <tr><th colspan="2">Guaranteer Address</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ID</td>
                        <td><b><?=h($customer->guaranteer->id)?></b></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td><b><?=h($customer->guaranteer->firstname)?></b></td>
                    </tr>
                    <?php if($customer->guaranteer->customer_addres) : ?>
                    <tr>
                        <td>Mobile</td>
                        <td><b><?=h($customer->guaranteer->customer_addres->mobile)?></b></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><b><?=h($customer->guaranteer->customer_addres->phone)?></b></td>
                    </tr>
                    <tr>
                        <td>Door No</td>
                        <td><b><?=h($customer->guaranteer->customer_addres->door_no)?></b></td>
                    </tr>
                    <tr>
                        <td>Street</td>
                        <td><b><?=h($customer->guaranteer->customer_addres->street)?></b></td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td><b><?=h($customer->guaranteer->customer_addres->area)?></b></td>
                    </tr>
                    <tr>
                        <td>Village</td>
                        <td><b><?=h($customer->guaranteer->customer_addres->village)?></b></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><b><?=h($customer->guaranteer->customer_addres->city)?></b></td>
                    </tr>
                    <tr>
                        <td>Pincode</td>
                        <td><b><?=h($customer->guaranteer->customer_addres->pincode)?></b></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <?php endif; ?>

        <div class="large-12 columns">
            <div class="related">
                <h4><?= __('Related Accounts') ?></h4>
                <?php if (!empty($customer->accounts)): ?>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <th scope="col"><?= __('Ref No') ?></th>
                        <th scope="col"><?= __('Loan Date') ?></th>
                        <th scope="col"><?= __('Amount') ?></th>
                        <th scope="col"><?= __('Status') ?></th>
                        <th scope="col"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($customer->accounts as $account): ?>
                    <tr>
                        <td><?= h($account->ref_no) ?></td>
                        <td><?= $this->Time->format($account->loan_date, 'dd-MMM-yyyy') ?></td>
                        <td><?= h($account->amount) ?></td>
                        <td>
                            <?php if($account->status == 'opened') : ?>
                                <span class="label">Active</span>
                            <?php else : ?>
                                <span class="label alert">Closed</span>
                            <?php endif;?>
                        </td>
                        <td class="actions" style="text-align: center;">
                            <?= $this->Html->link(('<i class="fa fa-book"></i>' . " Receipts"), ['controller' => 'Receipts', 'action' => 'add', 'account_id' => $account->id], ['escape'=>false, 'class'=>'label info']) ?>
                            <?= $this->Html->link(('<i class="fa fa-eye"></i>' . " View"), ['controller'=>'Accounts','action' => 'view', $account->id], ['escape'=>false, 'class'=>'label']) ?>
                            <?php if($user_role == 'admin') : ?>
                            <?= $this->Html->link('<i class="fa fa-pencil"></i>' . ' Edit', ['controller'=>'Accounts', 'action' => 'edit', $account->id], ['escape'=>false, 'class'=>'label success']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-trash-o"></i>' . ' Delete', ['controller'=>'Accounts', 'action' => 'delete', $account->id], ['escape'=>false, 'class'=>'label alert', 'confirm' => __('Are you sure you want to delete # {0}?', $account->id)]) ?>
                            <?php endif?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php endif;?>
</div>

<script type="text/javascript">
    $('#id').on('keypress', function(e) {
        if (e.keyCode == 13) {
            $('#customerOpenFrm').submit();
            return false; // prevent the button click from happening
        }
    });
    $('#nextBtn').on('click', function(evt) {
        evt.preventDefault();
        var custId = $('#id').val();
        custId = custId == "" ? 0 : custId;
        if(Math.floor(custId) == custId && $.isNumeric(custId)) {
            custId++;
        }

        if(custId > 0) {
            $('#id').val(custId);
        }
        $('#customerOpenFrm').submit();
    });

    $('#prevBtn').on('click', function(evt) {
        evt.preventDefault();
        var custId = $('#id').val();
        custId = custId == "" ? 2 : custId;
        if(Math.floor(custId) == custId && $.isNumeric(custId)) {
            custId--;
        }

        if(custId > 0) {
            $('#id').val(custId);
        }
        $('#customerOpenFrm').submit();
    });
</script>