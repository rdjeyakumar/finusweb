<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>

<div class="customers index large-12 medium-12 columns content">
    <h3><?= __('Customers') ?></h3>

    <div class="filters">
        <?php echo $this->Form->create("Filter",array('class' => 'filter', 'type' => 'GET'));?>
        <div class="row">
            <div class="large-6 columns">
                <?php echo $this->Form->input("search", array('label' => 'Search', 'placeholder' => "Search...", 'value' => isset($_GET['search']) ? $_GET['search'] : ''));?>
            </div>

            <div class="large-4 columns">
                <?php echo $this->Form->input("search_by", ['label' => 'By', 'options' => [
                    '' => 'Any',
                    'Customers.id' => 'Customer ID',
                    'firstname' => 'First name',
                    'lastname' => 'Last name',
                    'idendity_code' => 'Identity Code',
                    'CustomerAddress.mobile' => 'Mobile',
                    'CustomerAddress.phone' => 'Phone',
                    'CustomerAddress.door_no' => 'Door No',
                    'CustomerAddress.street' => 'Street',
                    'CustomerAddress.area' => 'Area',
                    'CustomerAddress.village' => 'Village',
                    'CustomerAddress.City' => 'City',
                    'CustomerAddress.pincode' => 'Pin Code'
                ], 'value' => isset($_GET['search_by']) ? $_GET['search_by'] : '']);?>
            </div>
            
            <div class="large-2 columns">
                <?php echo $this->Form->submit("Search", ['style' => 'margin-top: 25px']);?>
            </div>
        </div>
        <?php echo $this->Form->end();?>
    </div>

    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th width="80"><?php echo $this->Paginator->sort('Customers.id', 'Cust-ID');?></th>
                <th width="280"><?php echo $this->Paginator->sort('Customers.firstname', 'Customer');?></th>
                <th width="150">History</th>
                <th width="150"><?php echo $this->Paginator->sort('Customers.created', 'Created');?></th>
                <th width="150"><?php echo $this->Paginator->sort('Customers.modified', 'Modified');?></th>
                <th width="200">Actions</th>
                <th width="135"><?php echo $this->Paginator->sort('CreatedUser.username', 'Created');?></th>
                <th width="135"><?php echo $this->Paginator->sort('ModifiedUser.username', 'Modified');?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customers as $customer): ?>
            <tr>
                <td><?= $this->Number->format($customer->id) ?></td>
                <td>
                    <?= $this->Html->link(($customer->min_detail), ['controller' => 'Customers', 'action' => 'view', $customer->id], ['escape'=>false, 'class'=>'', 'data-reveal-id' => 'customerModal', 'data-reveal-ajax' => 'true']) ?>
                </td>
                <td>
                    <?php 
                        $histories = $customer->getCustomerHistory();
                        $totalHis = 0;
                        $oldestHis = 0;
                        foreach ($histories as $key => $history) {
                            if($history->is_oldest) {
                                $oldestHis++;
                            }
                            $totalHis++;
                        }
                    ?>
                    <?php if($oldestHis > 0) : ?>
                        <span class="label alert"><?=$oldestHis?> of <?=$totalHis?> Account(s)</span>
                    <?php else : ?>
                        <span class="label info"><?=$totalHis?> Account(s)</span>
                    <?php endif;?>
                </td>
                <td><?=$this->Time->format($customer->created, 'dd-MMM-yyyy hh:mm a')?></td>
                <td><?=$this->Time->format($customer->modified, 'dd-MMM-yyyy hh:mm a')?></td>
                <td class="actions" style="text-align: center;">
                    <?= $this->Html->link(('<i class="fa fa-print"></i>' . ""), ['controller' => 'Customers', 'action' => 'print', $customer->id], ['escape'=>false, 'class'=>'label info', 'target' => '_new']) ?>
                    <?= $this->Html->link(('<i class="fa fa-eye"></i>' . " View"), ['action' => 'view', $customer->id], ['escape'=>false, 'class'=>'label']) ?>
                    <?= $this->Html->link('<i class="fa fa-pencil"></i>' . ' Edit', ['action' => 'edit', $customer->id], ['escape'=>false, 'class'=>'label success']) ?>
                    <?php if($user_role == 'admin') : ?>
                    <?= $this->Form->postLink('<i class="fa fa-trash-o"></i>' . ' Delete', ['action' => 'delete', $customer->id], ['escape'=>false, 'class'=>'label alert', 'confirm' => __('Are you sure you want to delete # {0}?', $customer->id)]) ?>
                    <?php endif;?>
                </td>

                <td>
                    <?=$customer->created_user ? $customer->created_user->username : '';?>
                </td>
                <td>
                    <?=$customer->modified_user ? $customer->modified_user->username : '';?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<div id="customerModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style="max-height: 500px; overflow-y: scroll;">
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>