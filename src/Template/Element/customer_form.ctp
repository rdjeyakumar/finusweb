<?php
    $model = (isset($isAccount) && $isAccount == true) ? 'customer.' : '';
    $elemId = (isset($isAccount) && $isAccount == true) ? 'customer-' : '';
?>
<div id="page1">
    <div class="row">

        <div class="large-2 columns" id="fileDiv1">
            <div id="imgDiv1" style="width: 150px; height: 175px; border: 1px solid #000;">
                <!-- <img id="img1" src="" style="width: 100%; height: 100%" /> -->
                <?=$this->Html->image('noimage.jpg', ['id' => 'img1', 'style' => 'width: 100%; height: 100%']);?>
            </div>
            <?php echo $this->Form->control($model . 'photo', ['type' => 'file']);?>
        </div>

        <div class="large-4 columns">
            <?php echo $this->Form->control($model . 'firstname');?>
            <?php echo $this->Form->control($model . 'lastname');?>
            <div class="row">
                <div class="large-6 columns">
                    <?php echo $this->Form->control($model . 'gender', ['options' => ['' => 'Select', 'M' => 'Male', 'F' => 'Female']]);?>
                </div>
                <div class="large-6 columns">
                    <?php echo $this->Form->control($model . 'father_or_spouse', ['label' => 'Relation', 'options' => ['' => 'Select', 'F' => 'Father', 'S' => 'Spouse']]);?>
                </div>
            </div>
        </div>

        <div class="large-4 columns">
            <?php // echo $this->Form->control($model . 'identity_file', ['type' => 'file']);?>
            <?php echo $this->Form->control($model . 'identity_type', ['options' => [
                'Aadhaar'=>'Aadhaar',
                'Bank Passbook'=>'Bank Passbook',
                'Driving Licence'=>'Driving Licence',
                'Voter ID'=>'Voter ID',
                'Others'=>'Others'
            ]]);?>
            <?php echo $this->Form->control($model . 'identity_code');?>
        </div>

        <div class="large-2 columns" id="fileDiv1">
            <div id="imgDiv2" style="width: 150px; height: 175px; border: 1px solid #000;">
                <!-- <img id="img1" src="" style="width: 100%; height: 100%" /> -->
                <?=$this->Html->image('noimage.jpg', ['id' => 'img2', 'style' => 'width: 100%; height: 100%']);?>
            </div>
            <?php echo $this->Form->control($model . 'identity_file', ['type' => 'file']);?>
        </div>
    </div>

    <div class="row">
        <h5>Address</h5>
        <div class="large-2 columns">
            <?php echo $this->Form->control($model . 'customer_addres.door_no');?>
        </div>

        <div class="large-5 columns">
            <?php echo $this->Form->control($model . 'customer_addres.street');?>
        </div>

        <div class="large-5 columns">
            <?php echo $this->Form->control($model . 'customer_addres.area');?>
        </div>
    </div>

    <div class="row">
        <div class="large-5 columns">
            <?php echo $this->Form->control($model . 'customer_addres.village');?>
        </div>

        <div class="large-5 columns">
            <?php echo $this->Form->control($model . 'customer_addres.city');?>
        </div>

        <div class="large-2 columns">
            <?php echo $this->Form->control($model . 'customer_addres.pincode');?>
        </div>
    </div>

    <div class="row">
        <div class="large-6 columns">
            <?php echo $this->Form->control($model . 'customer_addres.mobile');?>
        </div>

        <div class="large-6 columns">
            <?php echo $this->Form->control($model . 'customer_addres.phone', ['label' => 'Mobile 2']);?>
        </div>
    </div>
</div>

<!-- Preview Image -->
<script type="text/javascript">
    function readURL(input, previewDiv) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#'+previewDiv).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#customer-photo").change(function() {
        readURL(this, "img1");
    });

    $("#customer-identity-file").change(function() {
        readURL(this, "img2");
    });
</script>
<!-- Preview Image ends -->