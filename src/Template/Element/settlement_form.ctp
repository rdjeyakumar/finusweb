<?php if(count($payables) == 1) : ?>
<?= 
    $this->Form->create($settlement, [
        'id' => 'receipt_settlement_form',
        'onsubmit' => "return confirm(\"Are you sure? \");"
    ]);
?>
<table>
    <thead>
        <tr>
            <th width="200">To</th>
            <th width="100">Interest %</th>
            <th width="100">Interest Amount</th>
            <th width="100">Principal</th>
            <th width="100">Amount</th>
            <th width="100">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?php echo $this->Form->control('account_id', ['type' => 'hidden']);?>
                <?php echo $this->Form->control('payment_type', ['type' => 'hidden']);?>
                <?php echo $this->Form->control('pdate', ['type'=>'date', 'label' => false]); ?>
            </td>
            <td>
                <?php echo $this->Form->control('interest_rate', ['label' => false, 'type'=>'text']); ?>
            </td>
            <td>
                <?php echo $this->Form->control('interest', ['label' => false, 'type'=>'text']); ?>
            </td>
            <td>
                <?php echo $this->Form->control('principal', ['label' => false, 'type'=>'text']); ?>
            </td>
            <td>
                <?php echo $this->Form->control('amount', ['readonly'=>true, 'label' => false, 'type'=>'text']); ?>
            </td>
            <td style="text-align: center;">
                <?= $this->Form->button('Save', ['class' => 'small', 'id'=>'submitBtn', 'style'=>'float: left']) ?>
            </td>
        </tr>
    </tbody>
</table>
<?= $this->Form->end(); ?>
<?php else : ?>
    <div class="row">
        <div class="large-3 columns">&nbsp;</div>
        <div class="large-6 columns" style="text-align: center;">
            <div data-alert class="alert-box alert round">
                Please clear pending payable bills before checking settlement..
            </div>
        </div>
        <div class="large-3 columns">&nbsp;</div>
    </div>
<?php endif; ?>