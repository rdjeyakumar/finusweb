<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer $customer
 */
?>

<div class="customers view large-12 medium-12 columns content">
    <h3><?= h($customer->firstname) ?> <span>(ID-<?= h($customer->id) ?>)</span></h3>
    <table>
        <tr>
            <td>
                <div id="imgDiv1" style="border: 1px solid #000;">
                    <?php if($customer->photo && file_exists("files/Customers/photo/" . $customer->photo)) : ?>
                    <!-- <img id="img1" src style="width: 100%; height: 100%" /> -->
                        <?php echo $this->Html->image(
                            "../files/Customers/photo/" . $customer->photo, [
                            "class" => "thumbnail"
                        ]);?>
                    <?php else : ?>
                        <?php echo $this->Html->image(
                            "noimage.jpg", [
                            "class" => "thumbnail"
                        ]);?>
                    <?php endif;?>
                </div>
            </td>
            <td>
                <?= h($customer->firstname); ?> <br/>
                <?php
                    if($customer->gender == 'M') {
                        if($customer->father_or_spouse == 'F') {
                            echo "Son Of ";
                        }

                        if($customer->father_or_spouse == 'S') {
                            echo "Husband Of ";
                        }

                    } else if($customer->gender == 'F') {
                        if($customer->father_or_spouse == 'F') {
                            echo "Daughter Of ";
                        }

                        if($customer->father_or_spouse == 'S') {
                            echo "Wife Of ";
                        }
                    }
                ?>
                <?= h($customer->lastname); ?> <br/>

                <?php if($customer->has('customer_addres') && !empty($customer->customer_addres)) : ?>
                    <?= h($customer->customer_addres->door_no) ?> - 
                    <?= h($customer->customer_addres->street) ?> <br/>
                    <?= h($customer->customer_addres->area) ?> - 
                    <?= h($customer->customer_addres->village) ?> <br/>
                    <?= h($customer->customer_addres->city) ?> - 
                    <?= h($customer->customer_addres->pincode) ?>
                <?php endif; ?>
                
            </td>
            <td>
                <div id="imgDiv1" style="border: 1px solid #000;">
                    <?php if($customer->identity_file && file_exists("files/Customers/identity_file/" . $customer->identity_file)) : ?>
                    <!-- <img id="img1" src style="width: 100%; height: 100%" /> -->
                        <?php echo $this->Html->image(
                            "../files/Customers/identity_file/" . $customer->identity_file, [
                            "class" => "thumbnail"
                        ]);?>
                    <?php else : ?>
                        <?php echo $this->Html->image(
                            "noimage.jpg", [
                            "class" => "thumbnail"
                        ]);?>
                    <?php endif;?>
                </div>
            </td>
            <td>
                <?= h($customer->identity_type) ?> - <br/>
                <?= h($customer->identity_code) ?>
            </td>
            <td>
                <?php if($customer->has('guaranteer_addres') && !empty($customer->guaranteer_addres)) : ?>
                    <?= h($customer->guaranteer_addres->door_no) ?> - 
                    <?= h($customer->guaranteer_addres->street) ?> <br/>
                    <?= h($customer->guaranteer_addres->area) ?> - 
                    <?= h($customer->guaranteer_addres->village) ?> <br/>
                    <?= h($customer->guaranteer_addres->city) ?> - 
                    <?= h($customer->guaranteer_addres->pincode) ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Accounts') ?></h4>
        <?php if (!empty($customer->accounts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Account Type') ?></th>
                <th scope="col"><?= __('Customer Id') ?></th>
                <th scope="col"><?= __('Ref No') ?></th>
                <th scope="col"><?= __('Amount') ?></th>
                <th scope="col"><?= __('Interest') ?></th>
                <th scope="col"><?= __('Total Weight') ?></th>
                <th scope="col"><?= __('Net Weight') ?></th>
                <th scope="col"><?= __('Property Value') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($customer->accounts as $accounts): ?>
            <tr>
                <td><?= h($accounts->id) ?></td>
                <td><?= h($accounts->account_type) ?></td>
                <td><?= h($accounts->customer_id) ?></td>
                <td><?= h($accounts->ref_no) ?></td>
                <td><?= h($accounts->amount) ?></td>
                <td><?= h($accounts->interest) ?></td>
                <td><?= h($accounts->total_weight) ?></td>
                <td><?= h($accounts->net_weight) ?></td>
                <td><?= h($accounts->property_value) ?></td>
                <td><?= h($accounts->status) ?></td>
                <td><?= h($accounts->created) ?></td>
                <td><?= h($accounts->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Accounts', 'action' => 'view', $accounts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Accounts', 'action' => 'edit', $accounts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Accounts', 'action' => 'delete', $accounts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accounts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
