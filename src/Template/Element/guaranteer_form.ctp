<?php
    $model = (isset($isAccount) && $isAccount == true) ? 'customer.' : '';
?>
<div id="page2">
    <div class="row">
        <div id="guaranteer_form">
            <div class="large-4 columns">
                <?php echo $this->Form->control($model . 'guaranteer.firstname');?>
            </div>

            <div class="large-4 columns">
                <?php echo $this->Form->control($model . 'guaranteer.customer_addres.mobile');?>
            </div>

            <div class="large-4 columns">
                <?php echo $this->Form->control($model . 'guaranteer.customer_addres.phone');?>
            </div>
        
            <h5>Address</h5>
            <div class="large-2 columns">
                <?php echo $this->Form->control($model . 'guaranteer.customer_addres.door_no');?>
            </div>

            <div class="large-5 columns">
                <?php echo $this->Form->control($model . 'guaranteer.customer_addres.street');?>
            </div>

            <div class="large-5 columns">
                <?php echo $this->Form->control($model . 'guaranteer.customer_addres.area');?>
            </div>

            <div class="large-5 columns">
                <?php echo $this->Form->control($model . 'guaranteer.customer_addres.village');?>
            </div>

            <div class="large-5 columns">
                <?php echo $this->Form->control($model . 'guaranteer.customer_addres.city');?>
            </div>

            <div class="large-2 columns">
                <?php echo $this->Form->control($model . 'guaranteer.customer_addres.pincode');?>
            </div>
        </div>
    </div>
</div>