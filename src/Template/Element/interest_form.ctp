<?php
?>
<?php if(empty($payables)) : ?>
    No pending interests
<?php else : ?>
    <?php foreach ($payables as $key => $payable) : ?>
        <?= $this->Form->create($payable['model'], [
            'id' => 'receipt_interest_form_'.$key,
            'onsubmit' => "return confirm(\"Are you sure? \");"
        ]) ?>
        <table>
        <thead>
            <tr>
                <th width="100">Principal</th>
                <th width="100">From</th>
                <th width="200">To</th>
                <th width="100">Duration</th>
                <th width="100">Interest %</th>
                <th width="100">Interest Amount</th>
                <th width="100">Amount</th>
                <th width="100">Action</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <?php echo $this->Form->control('account_id', ['type' => 'hidden']);?>
                <?php echo $this->Form->control('payment_type', ['type' => 'hidden']);?>
                <b><?= $this->Number->format($payable['principal']);?></b>
            </td>
            <td>
                <b><?=$payable['date_from'];?></b>
            </td>
            <td>
                <?php $disabled = ($key !== (count($payables) - 1)); ?>
                <?php echo $this->Form->control('pdate', ['type'=>'date', 'label' => false, 'class' => $disabled]);?>
            </td>
            </td>
            <td>
                <?php echo $this->Form->control('duration', ['readonly'=>true, 'label' => false, 'type'=>'text']); ?>
            </td>
            <td>
                <?php echo $this->Form->control('interest_rate', ['label' => false, 'type'=>'text']); ?>
            </td>
            <td>
                <?php echo $this->Form->control('interest', ['label' => false, 'type'=>'text']); ?>
            </td>
            <td>
                <?php echo $this->Form->control('amount', ['readonly'=>true, 'label' => false, 'type'=>'text']); ?>
            </td>
            <td style="text-align: center;">
                <?= $this->Form->button('Save', ['class' => 'small', 'id'=>'submitBtn', 'style'=>'float: left']) ?>
            </td>
        </tr>
        </tbody>
    </table>
    <?= $this->Form->end() ?>
    <?php endforeach; ?>
<?php endif; ?>