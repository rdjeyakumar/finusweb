<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 *
 * @method \App\Model\Entity\Customer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['CustomerAddress', 'CreatedUser', 'ModifiedUser'],
            'order' => ['Customers.created' => 'desc'],
            'sortWhitelist' => [
                'Customers.id',
                'Customers.firstname',
                'Customers.created',
                'Customers.modified',
                'CreatedUser.username',
                'ModifiedUser.username'
            ],
        ];

        if ($this->request->is('get')) {
            // $searchData = $this->request->getData();
            $searchData = $this->request->getQueryParams();
            if(!isset($searchData['search_by'])) {
                $searchData['search_by'] = '';
            }
            if(!isset($searchData['search'])) {
                $searchData['search'] = '';
            }
            if($searchData['search_by'] == '') {

                $this->paginate['conditions'] = [
                    'OR' => [
                        'Customers.id' => $searchData['search'],
                        'firstname LIKE ' => '%' . $searchData['search'] . '%',
                        'lastname LIKE ' => '%' . $searchData['search'] . '%',
                        'identity_code LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.mobile LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.phone LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.door_no LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.street LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.area LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.village LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.city LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.pincode LIKE ' => '%' . $searchData['search'] . '%',
                    ]
                ];
            } else {
                if($searchData['search_by'] == "Customers.id")
                    $this->paginate['conditions'] = [
                        $searchData['search_by'] => $searchData['search']
                    ];
                else
                    $this->paginate['conditions'] = [
                        $searchData['search_by'] . ' LIKE ' => '%' . $searchData['search'] . '%'
                    ];
            }
        }

        $customers = $this->paginate($this->Customers);

        $this->set(compact('customers'));
    }

    /**
     * View method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $customer = [];
        if($id) {
            $customer = $this->Customers->get($id, [
                'contain' => ['Accounts', 'CustomerAddress', 'Guaranteers' => ['CustomerAddress']]
            ]);
            if($customer)
                $_GET['id'] = $customer->id;
        }

        if ($this->request->is('get')) {
            $searchData = $this->request->getQueryParams();
            if(isset($searchData['id']) && !empty($searchData['id'])) {
                $customer = $this->Customers->find('all')
                    ->where(['Customers.id'=>$searchData['id']])
                    ->contain(['Accounts', 'CustomerAddress', 'Guaranteers' => ['CustomerAddress']])
                    ->first();
            }
        }

        $this->set('customer', $customer);
    }

    public function loadview($id = null)
    {
        $customer = [];
        if($id) {
            $customer = $this->Customers->get($id, [
                'contain' => ['Accounts', 'CustomerAddress', 'Guaranteers' => ['CustomerAddress']]
            ]);
        }

        if ($this->request->is('post')) {
            $searchData = $this->request->getData();
            if(isset($searchData['id']) && !empty($searchData['id'])) {
                $customer = $this->Customers->find('all')
                    ->where(['Customers.id'=>$searchData['id']])
                    ->contain(['Accounts', 'CustomerAddress', 'Guaranteers' => ['CustomerAddress']])
                    ->first();
            }
        }

        $this->set('customer', $customer);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $customer = $this->Customers->newEntity();
        if ($this->request->is('post')) {
            $customer = $this->Customers->patchEntity($customer, $this->request->getData(), [
                'associated' => [
                    'Guaranteers' => [
                        'associated' => [
                            'CustomerAddress'
                        ]
                    ],
                    'CustomerAddress'
                ]
            ]);
            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('The customer has been saved.'));

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('The customer could not be saved. Please, try again.'));
        }
        $this->set(compact('customer'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customer = $this->Customers->get($id, [
            'contain' => ['CustomerAddress', 'Guaranteers' => ['CustomerAddress']]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customer = $this->Customers->patchEntity($customer, $this->request->getData(), [
                'associated' => [
                    'Guaranteers' => [
                        'associated' => [
                            'CustomerAddress'
                        ]
                    ],
                    'CustomerAddress'
                ]
            ]);
            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('The customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customer could not be saved. Please, try again.'));
        }
        $this->set(compact('customer'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customer = $this->Customers->get($id);
        if ($this->Customers->delete($customer)) {
            $this->Flash->success(__('The customer has been deleted.'));
        } else {
            $this->Flash->error(__('The customer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function search()
    {
        $this->paginate = [
            'contain' => ['CustomerAddress'],
            'order' => ['created' => 'desc'],
            'maxLimit' => 200,
            'limit' => 200
        ];

        if ($this->request->is('post')) {
            $searchData = $this->request->getData();
            if($searchData['search_by'] == '') {

                $this->paginate['conditions'] = [
                    'OR' => [
                        'Customers.id' => $searchData['search'],
                        'firstname LIKE ' => '%' . $searchData['search'] . '%',
                        'lastname LIKE ' => '%' . $searchData['search'] . '%',
                        'identity_code LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.mobile LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.phone LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.door_no LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.street LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.area LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.village LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.city LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.pincode LIKE ' => '%' . $searchData['search'] . '%',
                    ]
                ];
            } else {
                if($searchData['search_by'] == "Customers.id")
                    $this->paginate['conditions'] = [
                        $searchData['search_by'] => $searchData['search']
                    ];
                else
                    $this->paginate['conditions'] = [
                        $searchData['search_by'] . ' LIKE ' => '%' . $searchData['search'] . '%'
                    ];
            }
        }

        $customers = $this->paginate($this->Customers);

        $this->set(compact('customers'));
    }

    public function autoCompleteData()
    {
        $data = [];
        $fields = ['firstname', 'lastname'];
        foreach ($fields as $key => $field) {
            $tmpData = $this->Customers->find('all', [
                'fields' => 'DISTINCT Customers.' . $field,
                'order' => [$field => 'asc']
            ])->toArray();
            $list = array_column($tmpData, 'DISTINCT Customers');
            $list = array_column($list, $field);
            $data[$field . "List"] = $list;
        }

        $acc_det_model = $this->getTableLocator()->get('AccountDetails');
        $fields = ['property_name'];
        foreach ($fields as $key => $field) {
            $tmpData = $acc_det_model->find('all', [
                'fields' => 'DISTINCT AccountDetails.' . $field,
                'order' => [$field => 'asc']
            ])->toArray();
            $list = array_column($tmpData, 'DISTINCT AccountDetails');
            $list = array_column($list, $field);
            $data[$field . "List"] = $list;
        }

        $cusaddr_model = $this->getTableLocator()->get('CustomerAddress');
        $fields = ['street', 'area', 'village', 'city'];
        foreach ($fields as $key => $field) {
            $tmpData = $cusaddr_model->find('all', array(
                'fields' => 'DISTINCT CustomerAddress.' . $field,
                'conditions' => array('CustomerAddress.' .$field. ' !=' => ''),
                'order' => [$field => 'asc']
            ))->toArray();

            $list = array_column($tmpData, 'DISTINCT CustomerAddress');
            $list = array_column($list, $field);
            $data[$field . "List"] = $list;
        }
        echo json_encode($data);
        exit();
    }

    public function customerForm() {
        $isAccount = isset($_GET['isAccount']) && !empty($_GET['isAccount']) ? $_GET['isAccount'] : false;
        $this->autoRender = false;
        $this->viewPath = 'Element';
        $this->set(compact('isAccount'));
        $this->render('customer_form');
    }

    public function guaranteerForm() {
        $isAccount = isset($_GET['isAccount']) && !empty($_GET['isAccount']) ? $_GET['isAccount'] : false;
        $this->autoRender = false;
        $this->viewPath = 'Element';
        $this->set(compact('isAccount'));
        $this->render('guaranteer_form');
    }

    public function print($id = null)
    {
        $this->layout = '';
        $profile_model = $this->loadModel('Profile');
        $profile = $profile_model->get(1);
        
        $customer = null;
        if($id) {
            $customer = $this->Customers->get($id, [
                'contain' => ['Accounts' => ['AccountDetails'], 'CustomerAddress', 'Guaranteers' => ['CustomerAddress']]
            ]);
        }

        $this->set(compact('profile'));
        $this->set('customer', $customer);
    }
}
