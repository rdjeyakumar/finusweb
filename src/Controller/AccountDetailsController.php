<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AccountDetails Controller
 *
 * @property \App\Model\Table\AccountDetailsTable $AccountDetails
 *
 * @method \App\Model\Entity\AccountDetail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccountDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Accounts', 'Properties']
        ];
        $accountDetails = $this->paginate($this->AccountDetails);

        $this->set(compact('accountDetails'));
    }

    /**
     * View method
     *
     * @param string|null $id Account Detail id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $accountDetail = $this->AccountDetails->get($id, [
            'contain' => ['Accounts', 'Properties']
        ]);

        $this->set('accountDetail', $accountDetail);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $accountDetail = $this->AccountDetails->newEntity();
        if ($this->request->is('post')) {
            $accountDetail = $this->AccountDetails->patchEntity($accountDetail, $this->request->getData());
            if ($this->AccountDetails->save($accountDetail)) {
                $this->Flash->success(__('The account detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The account detail could not be saved. Please, try again.'));
        }
        $accounts = $this->AccountDetails->Accounts->find('list', ['limit' => 200]);
        $properties = $this->AccountDetails->Properties->find('list', ['limit' => 200]);
        $this->set(compact('accountDetail', 'accounts', 'properties'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Account Detail id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $accountDetail = $this->AccountDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $accountDetail = $this->AccountDetails->patchEntity($accountDetail, $this->request->getData());
            if ($this->AccountDetails->save($accountDetail)) {
                $this->Flash->success(__('The account detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The account detail could not be saved. Please, try again.'));
        }
        $accounts = $this->AccountDetails->Accounts->find('list', ['limit' => 200]);
        $properties = $this->AccountDetails->Properties->find('list', ['limit' => 200]);
        $this->set(compact('accountDetail', 'accounts', 'properties'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Account Detail id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $accountDetail = $this->AccountDetails->get($id);
        if ($this->AccountDetails->delete($accountDetail)) {
            $this->Flash->success(__('The account detail has been deleted.'));
        } else {
            $this->Flash->error(__('The account detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
