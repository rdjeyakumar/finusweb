<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Receipts Controller
 *
 * @property \App\Model\Table\ReceiptsTable $Receipts
 *
 * @method \App\Model\Entity\Receipt[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReceiptsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Accounts']
        ];
        $receipts = $this->paginate($this->Receipts);

        $this->set(compact('receipts'));
    }

    /**
     * View method
     *
     * @param string|null $id Receipt id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $receipt = $this->Receipts->get($id, [
            'contain' => ['Accounts']
        ]);

        $this->set('receipt', $receipt);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if(isset($_GET['account_id']) && !empty($_GET['account_id'])) {
            $account_id = $_GET['account_id'];
            $account_model = $this->loadModel('Accounts');
            $account = $account_model->get($account_id, ['contain' => ['Customers' => ['CustomerAddress', 'Guaranteers' => ['CustomerAddress']]]]);
            $this->paginate = [
                'contain' => ['Accounts' => ['Customers'=> ['CustomerAddress', 'Guaranteers'=> ['CustomerAddress']]], 'CreatedUser', 'ModifiedUser'],
                'conditions' => ['account_id' => $account_id] 
            ];
            $receipts = $this->paginate($this->Receipts, ['order'=>['pdate' => 'DESC', 'id' => 'DESC']]);
            $receipt = $this->Receipts->newEntity();
            $receipt->account_id = $account_id;
            
            if ($this->request->is('post')) {
                $receipt = $this->Receipts->patchEntity($receipt, $this->request->getData());
                if ($this->Receipts->save($receipt)) {

                    $this->Flash->success(__('The receipt has been saved.'));

                    return $this->redirect(['action' => 'add', 'account_id' => $account_id]);
                }
                $this->Flash->error(__('The receipt could not be saved. Please, try again.'));
            }
            $this->set(compact('receipts', 'receipt', 'account', 'payables', 'settlement'));
        } else {
            $this->Flash->error(__('Invalid account. Please, try again.'));
            $this->redirect(['controller' => 'Accounts']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Receipt id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $receipt = $this->Receipts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $receipt = $this->Receipts->patchEntity($receipt, $this->request->getData());
            if ($this->Receipts->save($receipt)) {
                $this->Flash->success(__('The receipt has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The receipt could not be saved. Please, try again.'));
        }
        $accounts = $this->Receipts->Accounts->find('list', ['limit' => 200]);
        $this->set(compact('receipt', 'accounts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Receipt id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $account_id = null;
        $this->request->allowMethod(['post', 'delete']);
        $receipt = $this->Receipts->get($id);
        $is_settlement = ($receipt->payment_type == 'settlement');
        if ($this->Receipts->delete($receipt)) {
            if($is_settlement) {
                $account_model = $this->loadModel('Accounts');
                $account = $account_model->get($receipt->account_id);
                $account->status = 'opened';
                $account_model->save($account);
            } else if($receipt->payment_type == 'extra') {
                $account_model = $this->loadModel('Accounts');
                $account = $account_model->get($receipt->account_id);
                $account->amount -= $receipt->amount;
                $account_model->save($account);
            }
            $this->Flash->success(__('The receipt has been deleted.'));
        } else {
            $this->Flash->error(__('The receipt could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add', 'account_id' => $receipt->account_id]);
    }

    public function calcInterest()
    {
        $account_id = $_POST['account_id'];
        $pdate = strtotime($_POST['pdate']);
        $account_model = $this->loadModel('Accounts');
        $account = $account_model->get($account_id);
        
        $receipts = $this->Receipts->find('all', [
            'conditions' => ['account_id' => $account_id],
            'contain' => 'Accounts'
        ]);
        $interest = 0;
        $paid_principal = 0;
        $loan_amount = $account->amount;
        $interest_rate = $account->interest;
        $balance_principal = 0;
        $duration = round(($pdate - strtotime($account->loan_date)) / (60 * 60 * 24));

        foreach ($receipts as $key => $receipt) {
            if($receipt->payment_type == 'loan' || $receipt->payment_type == 'extra') {
                $balance_principal += $receipt->amount;
            } else {

                if($receipt->principal) {
                    $balance_principal -= $receipt->principal;
                    $paid_principal += $receipt->principal;
                }

                if($receipt->duration) {
                    $duration -= $receipt->duration;
                }
            }
        }

        if($duration > 0) {
            $interest = $balance_principal * ($interest_rate / 100)  * ($duration / 30);
        }

        echo json_encode([
            'interest_rate' => $interest_rate,
            'duration' => $duration,
            'balance_principal' => $balance_principal,
            'interest' => $interest,
            'loan_amount' => $loan_amount,
            'paid_principal' => $paid_principal
        ]);

        exit();
    }

    public function report()
    {
        $this->paginate = [
            'contain' => ['Accounts' => ['Customers' => ['Guaranteers']]],
            'order' => ['pdate' => 'desc']
        ];

        $total = 0;

        $searchData = [];

        if ($this->request->is('get')) {
            // $searchData = $this->request->getData();
            $searchData = $this->request->getQueryParams();
            if(!isset($searchData['start_date'])) {
                $searchData['start_date'] = '';
            }
            if(!isset($searchData['end_date'])) {
                $searchData['end_date'] = '';
            }
        }
        if(isset($searchData['start_date']) && isset($searchData['end_date']) && !empty($searchData['start_date']) && !empty($searchData['end_date'])) {
            $start_date = $searchData['start_date'];
            $end_date = $searchData['end_date'];
        } else {
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d');
        }

        $cond = [
            'DATE(Receipts.pdate) >= ' => $start_date,
            'DATE(Receipts.pdate) <= ' => $end_date
        ];

        $this->paginate['conditions'] = $cond;

        $receipts = $this->paginate($this->Receipts);
        $total = $this->getTotal($cond);

        $this->set(compact('receipts'));
        $this->set(compact('total'));
    }

    public function bsheet()
    {

        $searchData = [];

        if ($this->request->is('post')) {
            $searchData = $this->request->getData();
        }
        if(isset($searchData['start_date']) && isset($searchData['end_date'])) {
            $start_date = $searchData['start_date'];
            $end_date = $searchData['end_date'];
        } else {
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d');
        }

        $cond = "WHERE DATE(date) >= '{$start_date}' AND DATE(date) <= '{$end_date}'";

        $conn = \Cake\Datasource\ConnectionManager::get('default');
        $sql = 'SELECT * FROM (SELECT 
            account_id AS acc_id,
            pdate AS date,
            payment_type AS ttype,
            interest,
            principal
        FROM receipts) t1 ' . $cond . ' ORDER BY date DESC';
        $stmt = $conn->execute($sql);
        $rows = $stmt->fetchAll('assoc');

        foreach ($rows as $key => $row) {
            $rows[$key]['customer'] = $this->getCustomer($row['acc_id']);
            $rows[$key]['ref_no'] = $this->getRefNo($row['acc_id']);
        }
        $this->set(compact('rows'));
    }

    public function getTotal($cond)
    {
        $query = $this->Receipts->find('all');
        $res = $query->select(['total_sum' =>$query->func()->sum('amount')])->where($cond)->first();
        $total = $res->total_sum;
        return $total;
    }

    public function getCustomer($account_id)
    {
        $customer_text = "-";
        try {
            $account_model = $this->loadModel('Accounts');
            $account = $account_model->find('all', ['conditions'=>['Accounts.id'=>$account_id], 'contain'=>['Customers'=>'CustomerAddress']])->first();
            
            if($account && $account->customer) {
                $customer_text .= "(" . $account->customer->id . ") ";
                $customer_text .= $account->customer->min_detail;
            }
        } catch(Cake\Datasource\Exception\RecordNotFoundException $e) {}
        return $customer_text;
    }

    public function getRefNo($account_id)
    {
        $account_model = $this->loadModel('Accounts');
        $account = $account_model->find('all', ['conditions'=>['Accounts.id'=>$account_id]])->first();
        $ref_no = "";
        if($account) {
            $ref_no = $account->ref_no;
        }
        return $ref_no;
    }
}
