<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * CrdrTransactions Controller
 *
 * @property \App\Model\Table\CrdrTransactionsTable $CrdrTransactions
 *
 * @method \App\Model\Entity\CrdrTransaction[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CrdrTransactionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['transaction_date' => 'desc']
        ];

        $searchData = [];

        if ($this->request->is('get')) {
            // $searchData = $this->request->getData();
            $searchData = $this->request->getQueryParams();
            if(!isset($searchData['start_date'])) {
                $searchData['start_date'] = '';
            }
            if(!isset($searchData['end_date'])) {
                $searchData['end_date'] = '';
            }
        }
        if(isset($searchData['start_date']) && isset($searchData['end_date']) && !empty($searchData['start_date']) && !empty($searchData['end_date'])) {
            $start_date = $searchData['start_date'];
            $end_date = $searchData['end_date'];
        } else {
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d');
        }

        $cond = [
            'DATE(CrdrTransactions.transaction_date) >= ' => $start_date,
            'DATE(CrdrTransactions.transaction_date) <= ' => $end_date
        ];

        $this->paginate['conditions'] = $cond;

        $crdrTransactions = $this->paginate($this->CrdrTransactions);

        $this->set(compact('crdrTransactions'));
    }

    /**
     * View method
     *
     * @param string|null $id Crdr Transaction id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $crdrTransaction = $this->CrdrTransactions->get($id, [
            'contain' => []
        ]);

        $this->set('crdrTransaction', $crdrTransaction);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $crdrTransaction = $this->CrdrTransactions->newEntity();
        if ($this->request->is('post')) {
            $crdrTransaction = $this->CrdrTransactions->patchEntity($crdrTransaction, $this->request->getData());
            if ($this->CrdrTransactions->save($crdrTransaction)) {
                $this->Flash->success(__('The crdr transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The crdr transaction could not be saved. Please, try again.'));
        } else {
            $crdrTransaction->transaction_date = date('Y-m-d');
        }
        $this->set(compact('crdrTransaction'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Crdr Transaction id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $crdrTransaction = $this->CrdrTransactions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $crdrTransaction = $this->CrdrTransactions->patchEntity($crdrTransaction, $this->request->getData());
            if ($this->CrdrTransactions->save($crdrTransaction)) {
                $this->Flash->success(__('The crdr transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The crdr transaction could not be saved. Please, try again.'));
        }
        $this->set(compact('crdrTransaction'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Crdr Transaction id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $crdrTransaction = $this->CrdrTransactions->get($id);
        if ($this->CrdrTransactions->delete($crdrTransaction)) {
            $this->Flash->success(__('The crdr transaction has been deleted.'));
        } else {
            $this->Flash->error(__('The crdr transaction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function autoCompleteData()
    {
        $data = [];
        $fields = ['transaction_title'];
        foreach ($fields as $key => $field) {
            $tmpData = $this->CrdrTransactions->find('all', array(
                'fields' => 'DISTINCT CrdrTransactions.' . $field,
                'conditions' => array('CrdrTransactions.' .$field. ' !=' => ''),
                'order' => [$field => 'asc']
            ))->toArray();

            $list = array_column($tmpData, 'DISTINCT CrdrTransactions');
            $list = array_column($list, $field);
            $data[$field . "List"] = $list;
        }
        echo json_encode($data);
        exit();
    }

    public function ledger()
    {
        $conn = ConnectionManager::get('default');

        $searchData = [];

        if ($this->request->is('get')) {
            // $searchData = $this->request->getData();
            $searchData = $this->request->getQueryParams();
            if(!isset($searchData['start_date'])) {
                $searchData['start_date'] = '';
            }
            if(!isset($searchData['end_date'])) {
                $searchData['end_date'] = '';
            }
        }
        if(isset($searchData['start_date']) && isset($searchData['end_date']) && !empty($searchData['start_date']) && !empty($searchData['end_date'])) {
            $start_date = $searchData['start_date'];
            $end_date = $searchData['end_date'];
        } else {
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d');
        }
        
        // Opening Balance SQL
        $sql = "SELECT 
            '{$start_date}' AS trdate,
            'Opening Balance' AS description,
            SUM(t1.debit) AS debit,
            SUM(t1.credit) AS credit,
            (SUM(t1.credit) - SUM(t1.debit)) AS balance
        FROM
        (SELECT 
            SUM(IF((payment_type = 'loan' OR payment_type = 'extra'), principal, 0)) AS debit,
            SUM(IF((payment_type = 'interest' OR payment_type = 'due' OR payment_type = 'settlement'), (principal + interest), 0)) AS credit
        FROM receipts
        WHERE DATE(pdate) < '{$start_date}'
        UNION ALL
        SELECT
            SUM(IF(transaction_type = 'debit', transaction_amount, 0)) AS debit,
            SUM(IF(transaction_type = 'credit', transaction_amount, 0)) AS credit
        FROM crdr_transactions 
        WHERE DATE(transaction_date) < '{$start_date}') t1;";

        $stmt = $conn->execute($sql);
        $opening_balance = $stmt->fetch('assoc');

        // Closing Balance SQL
        $sql = "SELECT 
            '{$end_date}' AS trdate,
            'Closing Balance' AS description,
            SUM(t1.debit) AS debit,
            SUM(t1.credit) AS credit,
            (SUM(t1.credit) - SUM(t1.debit)) AS balance
        FROM
        (SELECT 
            SUM(IF((payment_type = 'loan' OR payment_type = 'extra'), principal, 0)) AS debit,
            SUM(IF((payment_type = 'interest' OR payment_type = 'due' OR payment_type = 'settlement'), (principal + interest), 0)) AS credit
        FROM receipts
        WHERE DATE(pdate) <= '{$end_date}'
        UNION ALL
        SELECT
            SUM(IF(transaction_type = 'debit', transaction_amount, 0)) AS debit,
            SUM(IF(transaction_type = 'credit', transaction_amount, 0)) AS credit
        FROM crdr_transactions 
        WHERE DATE(transaction_date) <= '{$end_date}') t1;";

        $stmt = $conn->execute($sql);
        $closing_balance = $stmt->fetch('assoc');

        // Ledger
        $sql = "SELECT t1.* FROM (SELECT 
            DATE(pdate) AS trdate,
            concat('ACC-', accounts.ref_no, ' ', payment_type) AS description, 
            IF((payment_type = 'loan' OR payment_type = 'extra'), receipts.principal, 0) AS debit,
            IF((payment_type = 'interest' OR payment_type = 'due' OR payment_type = 'settlement'), (receipts.principal + receipts.interest), 0) AS credit
        FROM receipts
        LEFT JOIN accounts ON receipts.account_id = accounts.id
        WHERE DATE(pdate) >= '{$start_date}' AND DATE(pdate) <= '{$end_date}'
        UNION ALL
        SELECT
            DATE(transaction_date) AS trdate,
            CONCAT(transaction_title) AS description, 
            IF(transaction_type = 'debit', transaction_amount, 0) AS debit,
            IF(transaction_type = 'credit', transaction_amount, 0) AS credit
        FROM crdr_transactions 
        WHERE DATE(transaction_date) >= '{$start_date}' AND DATE(transaction_date) <= '{$end_date}') t1
        ORDER BY t1.trdate ASC;";

        $stmt = $conn->execute($sql);
        $rows = $stmt->fetchAll('assoc');

        $this->set(compact('opening_balance', 'rows', 'closing_balance'));
    }
}
