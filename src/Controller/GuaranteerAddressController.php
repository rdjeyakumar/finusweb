<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GuaranteerAddress Controller
 *
 * @property \App\Model\Table\GuaranteerAddressTable $GuaranteerAddress
 *
 * @method \App\Model\Entity\GuaranteerAddres[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GuaranteerAddressController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Customers']
        ];
        $guaranteerAddress = $this->paginate($this->GuaranteerAddress);

        $this->set(compact('guaranteerAddress'));
    }

    /**
     * View method
     *
     * @param string|null $id Guaranteer Addres id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $guaranteerAddres = $this->GuaranteerAddress->get($id, [
            'contain' => ['Customers']
        ]);

        $this->set('guaranteerAddres', $guaranteerAddres);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $guaranteerAddres = $this->GuaranteerAddress->newEntity();
        if ($this->request->is('post')) {
            $guaranteerAddres = $this->GuaranteerAddress->patchEntity($guaranteerAddres, $this->request->getData());
            if ($this->GuaranteerAddress->save($guaranteerAddres)) {
                $this->Flash->success(__('The guaranteer addres has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The guaranteer addres could not be saved. Please, try again.'));
        }
        $customers = $this->GuaranteerAddress->Customers->find('list', ['limit' => 200]);
        $this->set(compact('guaranteerAddres', 'customers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Guaranteer Addres id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $guaranteerAddres = $this->GuaranteerAddress->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $guaranteerAddres = $this->GuaranteerAddress->patchEntity($guaranteerAddres, $this->request->getData());
            if ($this->GuaranteerAddress->save($guaranteerAddres)) {
                $this->Flash->success(__('The guaranteer addres has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The guaranteer addres could not be saved. Please, try again.'));
        }
        $customers = $this->GuaranteerAddress->Customers->find('list', ['limit' => 200]);
        $this->set(compact('guaranteerAddres', 'customers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Guaranteer Addres id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $guaranteerAddres = $this->GuaranteerAddress->get($id);
        if ($this->GuaranteerAddress->delete($guaranteerAddres)) {
            $this->Flash->success(__('The guaranteer addres has been deleted.'));
        } else {
            $this->Flash->error(__('The guaranteer addres could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
