<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AccountPledgeDetail Controller
 *
 * @property \App\Model\Table\AccountPledgeDetailTable $AccountPledgeDetail
 *
 * @method \App\Model\Entity\AccountPledgeDetail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccountPledgeDetailController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Accounts', 'Pledges']
        ];
        $accountPledgeDetail = $this->paginate($this->AccountPledgeDetail);

        $this->set(compact('accountPledgeDetail'));
    }

    /**
     * View method
     *
     * @param string|null $id Account Pledge Detail id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $accountPledgeDetail = $this->AccountPledgeDetail->get($id, [
            'contain' => ['Accounts', 'Pledges']
        ]);

        $this->set('accountPledgeDetail', $accountPledgeDetail);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $accountPledgeDetail = $this->AccountPledgeDetail->newEntity();
        if ($this->request->is('post')) {
            $accountPledgeDetail = $this->AccountPledgeDetail->patchEntity($accountPledgeDetail, $this->request->getData());
            if ($this->AccountPledgeDetail->save($accountPledgeDetail)) {
                $this->Flash->success(__('The account pledge detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The account pledge detail could not be saved. Please, try again.'));
        }
        $accounts = $this->AccountPledgeDetail->Accounts->find('list', ['limit' => 200]);
        $pledges = $this->AccountPledgeDetail->Pledges->find('list', ['limit' => 200]);
        $this->set(compact('accountPledgeDetail', 'accounts', 'pledges'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Account Pledge Detail id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $accountPledgeDetail = $this->AccountPledgeDetail->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $accountPledgeDetail = $this->AccountPledgeDetail->patchEntity($accountPledgeDetail, $this->request->getData());
            if ($this->AccountPledgeDetail->save($accountPledgeDetail)) {
                $this->Flash->success(__('The account pledge detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The account pledge detail could not be saved. Please, try again.'));
        }
        $accounts = $this->AccountPledgeDetail->Accounts->find('list', ['limit' => 200]);
        $pledges = $this->AccountPledgeDetail->Pledges->find('list', ['limit' => 200]);
        $this->set(compact('accountPledgeDetail', 'accounts', 'pledges'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Account Pledge Detail id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $accountPledgeDetail = $this->AccountPledgeDetail->get($id);
        if ($this->AccountPledgeDetail->delete($accountPledgeDetail)) {
            $this->Flash->success(__('The account pledge detail has been deleted.'));
        } else {
            $this->Flash->error(__('The account pledge detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
