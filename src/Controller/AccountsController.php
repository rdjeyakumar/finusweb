<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 *
 * @method \App\Model\Entity\Account[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccountsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Customers' => ['CustomerAddress'], 'CreatedUser', 'ModifiedUser'],
            'order' => ['CAST(Accounts.ref_no AS UNSIGNED)' => 'desc'],
            'sortWhitelist' => [
                'Customers.id',
                'Customers.firstname',
                'CreatedUser.username',
                'ModifiedUser.username',
                'CAST(Accounts.ref_no AS UNSIGNED)',
                'Accounts.amount',
                'Accounts.loan_date',
                'Accounts.status'
            ],
        ];

        if ($this->request->is('get')) {
            $conditions = [];
            $searchData = $this->request->getQueryParams();
            
            if(!isset($searchData['search_by'])) {
                $searchData['search_by'] = '';
            }
            if(!isset($searchData['search'])) {
                $searchData['search'] = '';
            }
            
            if($searchData['search_by'] == '') {
                $conditions = [
                    'OR' => [
                        'ref_no LIKE' => '%' . $searchData['search'] . '%',
                        'Customers.id' => $searchData['search'],
                        'Customers.firstname LIKE ' => '%' . $searchData['search'] . '%',
                        'Customers.lastname LIKE ' => '%' . $searchData['search'] . '%',
                        'Customers.identity_code LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.mobile LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.phone LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.door_no LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.street LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.area LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.village LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.city LIKE ' => '%' . $searchData['search'] . '%',
                        'CustomerAddress.pincode LIKE ' => '%' . $searchData['search'] . '%',
                    ]
                ];
            } else {
                if($searchData['search_by'] == "Customers.id")
                    $conditions = [
                        $searchData['search_by'] => $searchData['search']
                    ];
                else
                $conditions = [
                    $searchData['search_by'] . ' LIKE ' => '%' . $searchData['search'] . '%'
                ];
            }
            
            if(isset($searchData['status']) && !empty($searchData['status'])) {
                $conditions['AND'] = [
                    'status' => $searchData['status']
                ];
            }

            $this->paginate['conditions'] = $conditions;
        }

        $accounts = $this->paginate($this->Accounts);

        $this->set(compact('accounts'));
        $this->set(compact('filter'));
    }

    /**
     * View method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $account = null;
        if($id) {
            $account = $this->Accounts->get($id, [
                'contain' => ['Customers' => ['Accounts', 'CustomerAddress', 'Guaranteers' => ['CustomerAddress']], 'AccountDetails', 'AccountPledgeDetail']
            ]);
            if($account)
                $_GET['ref_no'] = $account->ref_no;
        }

        if ($this->request->is('get')) {
            $searchData = $this->request->getQueryParams();
            if(isset($searchData['ref_no']) && !empty($searchData['ref_no'])) {
                $account = $this->Accounts->find('all')
                    ->where(['ref_no'=>$searchData['ref_no']])
                    ->contain(['Customers' => ['Accounts', 'CustomerAddress', 'Guaranteers' => ['CustomerAddress']], 'AccountDetails', 'AccountPledgeDetail'])
                    ->first();
            }
        }

        $this->set('account', $account);
    }

    public function ajaxview($id = null)
    {
        $account = null;
        if($id) {
            $account = $this->Accounts->get($id, [
                'contain' => ['Customers' => ['CustomerAddress', 'Guaranteers' => ['CustomerAddress']], 'AccountDetails', 'AccountPledgeDetail']
            ]);
        }

        if ($this->request->is('post')) {
            $searchData = $this->request->getData();
            if(isset($searchData['ref_no']) && !empty($searchData['ref_no'])) {
                $account = $this->Accounts->find('all')
                    ->where(['ref_no'=>$searchData['ref_no']])
                    ->contain(['Customers' => ['CustomerAddress', 'Guaranteers' => ['CustomerAddress']], 'AccountDetails', 'AccountPledgeDetail'])
                    ->first();
            }
        }

        $this->set('account', $account);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $next_id_query = "SELECT MAX(CAST(ref_no AS UNSIGNED)) AS next_id FROM accounts";
        $db = ConnectionManager::get('default');
        $stmt = $db->query($next_id_query);
        $row = $stmt->fetch('assoc');
        $next_id = isset($row['next_id']) && !empty($row['next_id']) ? $row['next_id'] : 0;
        
        $account = $this->Accounts->newEntity();
        $account->ref_no = $next_id + 1;

        if ($this->request->is('post')) {
            $account = $this->Accounts->patchEntity($account, $this->request->getData(), [
                'associated' => [
                    'Customers' => [
                        'associated' => [
                            'CustomerAddress',
                            'Guaranteers' => [
                                'associated' => 'CustomerAddress'
                            ]
                        ]
                    ],
                    'AccountDetails'
                ]
            ]);
            if ($this->Accounts->save($account)) {
                $this->Flash->success(__('The account has been saved.'));

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('The account could not be saved. Please, try again.'));
        }

        $properties = $this->Accounts->AccountDetails->find()
            ->select('property_name')
            ->distinct('property_name')
            ->order(['property_name'=>'ASC']);

        $this->set(compact('account', 'properties'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $account = $this->Accounts->get($id, [
            'contain' => [
                'Customers' => [
                    'CustomerAddress',
                    'Guaranteers' => [
                        'CustomerAddress'
                    ]
                ],
                'AccountDetails',
                'AccountPledgeDetail'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //print_r($this->request->getData());exit();
            $account = $this->Accounts->patchEntity($account, $this->request->getData(), [
                'associated' => [
                    'AccountDetails',
                    'AccountPledgeDetail'
                ]
            ]);
            if ($this->Accounts->save($account)) {
                $receipt_model = TableRegistry::get('Receipts');
                $receipts = $receipt_model->find()->where(['account_id' => $account->id, 'payment_type' => 'loan']);
                foreach ($receipts as $key => $receipt) {
                    if($receipt->amount != $account->amount) {
                        $receipt->pdate = $account->loan_date;
                        $receipt->payment_type = 'loan';
                        $receipt->principal = $account->amount;
                        $receipt->interest = 0;
                        $receipt->amount = $account->amount;
                        $receipt->created = $account->loan_date;
                        $receipt->modified = $account->loan_date;
                        $receipt_model->save($receipt);
                    }
                }
                $this->Flash->success(__('The account has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The account could not be saved. Please, try again.'));
        }

        $properties = $this->Accounts->AccountDetails->find()
            ->select('property_name')
            ->distinct('property_name')
            ->order(['property_name'=>'ASC']);
        
        $this->set(compact('account', 'properties'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $account = $this->Accounts->get($id);
        if ($this->Accounts->delete($account)) {
            $this->Flash->success(__('The account has been deleted.'));
        } else {
            $this->Flash->error(__('The account could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function report()
    {
        $this->paginate = [
            'contain' => ['Customers' => ['CustomerAddress', 'Guaranteers' => ['CustomerAddress']]],
            'order' => ['CAST(Accounts.ref_no AS UNSIGNED)' => 'ASC'],
            'sortWhitelist' => [
                'Customers.id',
                'Customers.firstname',
                'CAST(Accounts.ref_no AS UNSIGNED)',
                'Accounts.amount',
                'Accounts.loan_date',
                'Accounts.status'
            ],
        ];

        $total = 0;

        $searchData = [];

        if ($this->request->is('get')) {
            //$searchData = $this->request->getData();
            $searchData = $this->request->getQueryParams();
            if(!isset($searchData['start_date'])) {
                $searchData['start_date'] = '';
            }
            if(!isset($searchData['end_date'])) {
                $searchData['end_date'] = '';
            }
            if(!isset($searchData['status'])) {
                $searchData['status'] = '';
            }
        }
        if(isset($searchData['start_date']) && isset($searchData['end_date']) && !empty($searchData['start_date']) && !empty($searchData['end_date'])) {
            $start_date = $searchData['start_date'];
            $end_date = $searchData['end_date'];
        } else {
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d');
        }

        $cond = [
            'DATE(Accounts.loan_date) >= ' => $start_date,
            'DATE(Accounts.loan_date) <= ' => $end_date
        ];

        if(isset($searchData['status']) && !empty($searchData['status'])) {
            $cond['status'] = $searchData['status'];
        }

        $this->paginate['conditions'] = $cond;
        $all = $this->Accounts->find('all', array('conditions'=> $cond))->count();
        $this->paginate['limit'] = $all;

        $accounts = $this->paginate($this->Accounts);
        $total = $this->getTotal($cond);

        $this->set(compact('accounts'));
        $this->set(compact('total'));
    }

    public function print($id = null)
    {
        $this->layout = '';
        $profile_model = $this->loadModel('Profile');
        $profile = $profile_model->get(1);
        
        $account = null;
        if($id) {
            $account = $this->Accounts->get($id, [
                'contain' => ['Customers' => ['Accounts', 'CustomerAddress', 'Guaranteers' => ['CustomerAddress']], 'AccountDetails']
            ]);
        }

        $this->set(compact('profile'));
        $this->set('account', $account);
    }

    public function export()
    {
        $this->layout = '';
        $searchData = [];
        $accounts = [];
        if ($this->request->is('get')) {
            $searchData = $this->request->getQueryParams();
            if(isset($searchData['select_chk'])) {
                $select_chk = $searchData['select_chk'];
                $select_chk_arr = explode(',', $select_chk);
                $accounts = $this->Accounts->find('all')
                    ->where(['Accounts.id IN '=>$select_chk_arr])
                    ->contain(['Customers' => ['CustomerAddress'], 'AccountDetails'])
                    ->order(['CAST(Accounts.ref_no AS UNSIGNED)' => 'ASC']);
            }
        }

        $this->set(compact('accounts'));
        $this->set(compact('total'));
    }

    public function exportregpost()
    {
        $this->layout = '';
        $searchData = [];
        $accounts = [];
        $profile_model = $this->loadModel('Profile');
        $profile = $profile_model->get(1);

        $post_date = date('Y-m-d');
        $auction_date = date('Y-m-d');
        if ($this->request->is('get')) {
            $searchData = $this->request->getQueryParams();
            $post_date = $searchData['post_date'];
            $auction_date = $searchData['auction_date'];
            if(isset($searchData['select_chk'])) {
                $select_chk = $searchData['select_chk'];
                $select_chk_arr = explode(',', $select_chk);
                $accounts = $this->Accounts->find('all')
                    ->where(['Accounts.id IN '=>$select_chk_arr])
                    ->contain(['Customers' => ['CustomerAddress'], 'AccountDetails'])
                    ->order(['CAST(Accounts.ref_no AS UNSIGNED)' => 'desc']);
            }
        }
        $this->set(compact('post_date'));
        $this->set(compact('auction_date'));
        $this->set(compact('profile'));
        $this->set(compact('accounts'));
        $this->set(compact('total'));
    }

    public function getTotal($cond)
    {
        $query = $this->Accounts->find('all');
        $res = $query->select(['total_sum' =>$query->func()->sum('amount')])->where($cond)->first();
        $total = $res->total_sum;
        return $total;
    }

}
