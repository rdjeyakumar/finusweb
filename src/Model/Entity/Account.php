<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Account Entity
 *
 * @property int $id
 * @property string $account_type
 * @property int $customer_id
 * @property string $ref_no
 * @property float $amount
 * @property float $interest
 * @property float $total_weight
 * @property float $net_weight
 * @property float $property_value
 * @property string $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\AccountDetail[] $account_details
 */
class Account extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_type' => true,
        'customer_id' => true,
        'ref_no' => true,
        'amount' => true,
        'interest' => true,
        'collect_first_month_interest' => true,
        'total_weight' => true,
        'net_weight' => true,
        'property_value' => true,
        'status' => true,
        'created' => true,
        'loan_date' => true,
        'modified' => true,
        'customer' => true,
        'account_details' => true,
        'account_pledge_detail' => true
    ];

    protected function _getDuration()
    {
        $now = time(); // or your date as well
        $loanDate = strtotime($this->loan_date);
        $datediff = $now - $loanDate;

        $days = 0;
        if($datediff > 0)
            $days = round($datediff / (60 * 60 * 24));
        return $days;
    }

    protected function _getIsOldest()
    {
        return ($this->duration >= 180);
    }

    protected function _getAjaxLink()
    {
        return "<a href=\"".
            \Cake\Routing\Router::url(['controller' => 'Accounts', 'action' => 'ajaxview']) ."/".
            $this->id."\" data-reveal-id=\"accountModal\" data-reveal-ajax=\"true\">" . $this->ref_no . "</a>"; 
    }
}
