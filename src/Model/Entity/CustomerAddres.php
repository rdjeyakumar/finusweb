<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CustomerAddres Entity
 *
 * @property int $id
 * @property int $customer_id
 * @property string $name
 * @property string $mobile
 * @property string $phone
 * @property string $door_no
 * @property string $street
 * @property string $area
 * @property string $village
 * @property string $city
 * @property string $pincode
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Customer $customer
 */
class CustomerAddres extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'customer_id' => true,
        'name' => true,
        'mobile' => true,
        'phone' => true,
        'door_no' => true,
        'street' => true,
        'area' => true,
        'village' => true,
        'city' => true,
        'pincode' => true,
        'created' => true,
        'modified' => true,
        'customer' => true
    ];
}
