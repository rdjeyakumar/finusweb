<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CrdrTransaction Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $transaction_date
 * @property string $transaction_type
 * @property string $transaction_title
 * @property float $transaction_amount
 * @property string $transaction_remarks
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class CrdrTransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'transaction_date' => true,
        'transaction_type' => true,
        'transaction_title' => true,
        'transaction_amount' => true,
        'transaction_remarks' => true,
        'created' => true,
        'modified' => true
    ];
}
