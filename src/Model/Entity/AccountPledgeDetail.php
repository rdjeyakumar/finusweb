<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AccountPledgeDetail Entity
 *
 * @property int $id
 * @property int $account_id
 * @property string $pledge_id
 * @property \Cake\I18n\FrozenTime $pledge_date
 * @property string $pledge_vendor
 * @property float $pledge_amount
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Pledge $pledge
 */
class AccountPledgeDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'pledge_id' => true,
        'pledge_date' => true,
        'pledge_vendor' => true,
        'pledge_amount' => true,
        'created' => true,
        'modified' => true,
        'account' => true,
        'pledge' => true
    ];
}
