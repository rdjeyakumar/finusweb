<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Customer Entity
 *
 * @property int $id
 * @property string $firstname
 * @property string $father_or_spouse
 * @property string $lastname
 * @property string $gender
 * @property string $photo
 * @property string $identity_type
 * @property string $identity_code
 * @property string $identity_file
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Account[] $accounts
 */
class Customer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'firstname' => true,
        'father_or_spouse' => true,
        'lastname' => true,
        'gender' => true,
        'photo' => true,
        'identity_type' => true,
        'identity_code' => true,
        'identity_file' => true,
        'created' => true,
        'modified' => true,
        'customer_addres' => true,
        'guaranteer_addres' => true,
        'guaranteer' => true,
        'guaranteer_id' => true
    ];

    public function getCustomerHistory()
    {
        $account_model = TableRegistry::get('Accounts');
        $accounts = $account_model->find('all', [
            'conditions' => ['customer_id' => $this->id, 'status' => 'opened']
        ]);
        return $accounts;
    }

    protected function _getMinDetail()
    {
        $full_detail = "";
        $full_detail .= $this->firstname . ", ";
        if($this->gender == 'M') {
            if($this->father_or_spouse == 'F') {
                $full_detail .= "S/O, ";
            } else if($this->father_or_spouse == 'S') {
                $full_detail .= "H/O, ";
            }
        } else if($this->gender == 'F') {
            if($this->father_or_spouse == 'F') {
                $full_detail .= "D/O, ";
            } else if($this->father_or_spouse == 'S') {
                $full_detail .= "W/O, ";
            }
        }
        $full_detail .= $this->lastname;
        return $full_detail;
    }

    protected function _getFullDetail()
    {
        $full_detail = "";
        $full_detail .= $this->firstname . ", ";
        $pre_txt = "";
        if($this->gender == 'M') {
            if($this->father_or_spouse == 'F') {
                $full_detail .= "S/O, ";
                $pre_txt = "திரு. ";
            } else if($this->father_or_spouse == 'S') {
                $full_detail .= "H/O, ";
                $pre_txt = "திரு. ";
            }
        } else if($this->gender == 'F') {
            if($this->father_or_spouse == 'F') {
                $full_detail .= "D/O, ";
                $pre_txt = "செல்வி. ";
            } else if($this->father_or_spouse == 'S') {
                $full_detail .= "W/O, ";
                $pre_txt = "திருமதி. ";
            }
        }
        $full_detail .= $this->lastname;
        if($this->customer_addres) {
            $full_detail .= "<br/>";
            $full_detail .= $this->customer_addres->door_no . ", ";
            $full_detail .= $this->customer_addres->street . "<br/>";
            $full_detail .= $this->customer_addres->area . ", ";
            $full_detail .= $this->customer_addres->village . "<br/>";
            $full_detail .= $this->customer_addres->city . ", ";
            $full_detail .= $this->customer_addres->pincode . "<br/>";
            $full_detail .= $this->customer_addres->mobile . ", ";
            $full_detail .= $this->customer_addres->phone;
        }
        return $pre_txt . $full_detail;
    }
}
