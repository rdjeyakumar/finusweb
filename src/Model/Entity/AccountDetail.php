<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AccountDetail Entity
 *
 * @property int $id
 * @property int $account_id
 * @property string $property_name
 * @property int $qty
 * @property float $gross_weight
 * @property float $net_weight
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Property $property
 */
class AccountDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'property_name' => true,
        'qty' => true,
        'gross_weight' => true,
        'net_weight' => true,
        'created' => true,
        'modified' => true,
        'account' => true
    ];
}
