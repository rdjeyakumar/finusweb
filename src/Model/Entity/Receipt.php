<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Receipt Entity
 *
 * @property int $id
 * @property int $account_id
 * @property \Cake\I18n\FrozenTime $pdate
 * @property int $duration
 * @property string $payment_type
 * @property float $interest
 * @property float $principal
 * @property float $amount
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Account $account
 */
class Receipt extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'pdate' => true,
        'duration' => true,
        'payment_type' => true,
        'interest' => true,
        'principal' => true,
        'amount' => true,
        'created' => true,
        'modified' => true,
        'account' => true
    ];

}
