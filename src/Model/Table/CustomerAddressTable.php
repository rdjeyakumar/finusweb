<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CustomerAddress Model
 *
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 *
 * @method \App\Model\Entity\CustomerAddres get($primaryKey, $options = [])
 * @method \App\Model\Entity\CustomerAddres newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CustomerAddres[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CustomerAddres|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CustomerAddres|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CustomerAddres patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CustomerAddres[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CustomerAddres findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CustomerAddressTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customer_address');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->allowEmpty('name');

        $validator
            ->scalar('mobile')
            ->maxLength('mobile', 50)
            ->allowEmpty('mobile');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 50)
            ->allowEmpty('phone');

        $validator
            ->scalar('door_no')
            ->maxLength('door_no', 50)
            ->allowEmpty('door_no');

        $validator
            ->scalar('street')
            ->maxLength('street', 150)
            ->allowEmpty('street');

        $validator
            ->scalar('area')
            ->maxLength('area', 150)
            ->allowEmpty('area');

        $validator
            ->scalar('village')
            ->maxLength('village', 150)
            ->allowEmpty('village');

        $validator
            ->scalar('city')
            ->maxLength('city', 150)
            ->allowEmpty('city');

        $validator
            ->scalar('pincode')
            ->maxLength('pincode', 50)
            ->allowEmpty('pincode');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }
}
