<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CrdrTransactions Model
 *
 * @method \App\Model\Entity\CrdrTransaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\CrdrTransaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CrdrTransaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CrdrTransaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CrdrTransaction|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CrdrTransaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CrdrTransaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CrdrTransaction findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CrdrTransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('crdr_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('transaction_date')
            ->notEmpty('transaction_date');

        $validator
            ->scalar('transaction_type')
            ->maxLength('transaction_type', 100)
            ->notEmpty('transaction_type');

        $validator
            ->scalar('transaction_title')
            ->maxLength('transaction_title', 100)
            ->notEmpty('transaction_title');

        $validator
            ->decimal('transaction_amount')
            ->notEmpty('transaction_amount');

        $validator
            ->scalar('transaction_remarks')
            ->maxLength('transaction_remarks', 300)
            ->allowEmpty('transaction_remarks');

        return $validator;
    }
}
