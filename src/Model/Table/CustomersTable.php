<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Customers Model
 *
 * @property \App\Model\Table\AccountsTable|\Cake\ORM\Association\HasMany $Accounts
 * @property |\Cake\ORM\Association\HasMany $Addresses
 *
 * @method \App\Model\Entity\Customer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Customer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Customer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Customer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Customer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Customer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CustomersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'photo' => [
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    return time() . strtolower($data['name']);
                },
                'keepFilesOnDelete' => false,
            ],
            'identity_file' => [
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    return time() . strtolower($data['name']);
                },
                'keepFilesOnDelete' => false,
            ]
        ]);

        $this->hasMany('Accounts', [
            'foreignKey' => 'customer_id'
        ]);

        $this->belongsTo('Guaranteers', [
            'foreignKey' => 'guaranteer_id',
            'className' => 'Customers'
        ]);

        $this->hasOne('CustomerAddress', [
            'foreignKey' => 'customer_id'
        ]);

        $this->belongsTo('CreatedUser', [
                'className' => 'Users'
            ])
            ->setForeignKey('created_by')
            ->setProperty('created_user');

        $this->belongsTo('ModifiedUser', [
                'className' => 'Users'
            ])
            ->setForeignKey('modified_by')
            ->setProperty('modified_user');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 50)
            ->notEmpty('firstname');

        $validator
            ->scalar('father_or_spouse')
            ->notEmpty('father_or_spouse');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 50)
            ->notEmpty('lastname');

        $validator
            ->scalar('gender')
            ->notEmpty('gender');

        $validator
            //->scalar('photo')
            //->maxLength('photo', 250)
            ->allowEmpty('photo');

        $validator
            ->scalar('identity_type')
            ->maxLength('identity_type', 50)
            ->allowEmpty('identity_type');

        $validator
            ->scalar('identity_code')
            ->maxLength('identity_code', 50)
            ->allowEmpty('identity_code');

        $validator
            //->scalar('identity_file')
            //->maxLength('identity_file', 250)
            ->allowEmpty('identity_file');

        return $validator;
    }

    public function beforeSave($event, $entity, $options) {
        $userId = isset($_SESSION['Auth']['User']['id']) ? $_SESSION['Auth']['User']['id'] : 0;
        if($entity->isNew()) {
            $entity->created_by = $userId;
        } else {
            $entity->modified_by = $userId;
        }
    }
}
