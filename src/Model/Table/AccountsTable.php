<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

/**
 * Accounts Model
 *
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 * @property \App\Model\Table\AccountDetailsTable|\Cake\ORM\Association\HasMany $AccountDetails
 *
 * @method \App\Model\Entity\Account get($primaryKey, $options = [])
 * @method \App\Model\Entity\Account newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Account[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Account|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Account|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Account patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Account[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Account findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id'
        ]);

        $this->hasMany('AccountDetails', [
            'foreignKey' => 'account_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasOne('AccountPledgeDetail', [
            'foreignKey' => 'account_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Receipts', [
            'foreignKey' => 'account_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->belongsTo('CreatedUser', [
                'className' => 'Users'
            ])
            ->setForeignKey('created_by')
            ->setProperty('created_user');

        $this->belongsTo('ModifiedUser', [
                'className' => 'Users'
            ])
            ->setForeignKey('modified_by')
            ->setProperty('modified_user');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('account_type')
            ->notEmpty('account_type');

        $validator
            ->scalar('ref_no')
            ->maxLength('ref_no', 50)
            ->notEmpty('ref_no');

        $validator->add(
            'ref_no', 
            ['unique' => [
                'rule' => 'validateUnique', 
                'provider' => 'table', 
                'message' => 'Already exists']
            ]
        );

        $validator
            ->date('loan_date')
            ->requirePresence('loan_date', 'create')
            ->notEmpty('loan_date');

        $validator
            ->decimal('amount')
            ->notEmpty('amount');

        $validator
            ->decimal('interest')
            ->notEmpty('interest');

        $validator
            ->decimal('total_weight')
            ->notEmpty('total_weight');

        if(env('NET_WEIGHT_REQUIRED') == "true") {
            $validator
                ->decimal('net_weight')
                ->notEmpty('net_weight');
        } else {
            $validator
                ->decimal('net_weight')
                ->allowEmpty('net_weight');
        }

        $validator
            ->decimal('property_value')
            ->notEmpty('property_value');

        $validator
            ->scalar('status')
            ->maxLength('status', 50)
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }

    public function beforeFind($query) {
        $loggedUser = Router::getRequest()->getSession()->read('Auth.User');
        if($loggedUser) {
            if($loggedUser['role'] != 'admin') {
                //$query['conditions']['account_type'] = 'direct';
            }
        }
        return $query;
    }

    public function beforeSave($event, $entity, $options) {
        $userId = isset($_SESSION['Auth']['User']['id']) ? $_SESSION['Auth']['User']['id'] : 0;
        if($entity->isNew()) {
            $entity->created_by = $userId;
        } else {
            $entity->modified_by = $userId;
        }
    }

    public function afterSave($event, $entity, $options) {
        if($entity->isNew()) {
            $receipt_model = TableRegistry::get('Receipts');
            $receipt = $receipt_model->newEntity();
            $receipt->account_id = $entity->id;
            $receipt->pdate = $entity->loan_date;
            $receipt->payment_type = 'loan';
            $receipt->principal = $entity->amount;
            $receipt->interest = 0;
            $receipt->amount = $entity->amount;
            $receipt->created = $entity->loan_date;
            $receipt->modified = $entity->loan_date;
            $receipt_model->save($receipt);
            
            if($entity->collect_first_month_interest == 1) {
                $duration = 30;
                $interest = $entity->amount * ($entity->interest / 100)  * ($duration / 30);
                $receipt = $receipt_model->newEntity();
                $receipt->account_id = $entity->id;
                $receipt->pdate = $entity->loan_date;
                $receipt->duration = $duration;
                $receipt->payment_type = 'interest';
                $receipt->interest = $interest;
                $receipt->principle = 0;
                $receipt->amount = $interest;
                $receipt->created = $entity->loan_date;
                $receipt->modified = $entity->loan_date;
                $receipt_model->save($receipt);
            }
        }
    }
}
