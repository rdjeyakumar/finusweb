<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AccountPledgeDetail Model
 *
 * @property \App\Model\Table\AccountsTable|\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\PledgesTable|\Cake\ORM\Association\BelongsTo $Pledges
 *
 * @method \App\Model\Entity\AccountPledgeDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\AccountPledgeDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AccountPledgeDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AccountPledgeDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AccountPledgeDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AccountPledgeDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AccountPledgeDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AccountPledgeDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AccountPledgeDetailTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('account_pledge_detail');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('pledge_date')
            ->allowEmpty('pledge_date');

        $validator
            ->scalar('pledge_vendor')
            ->maxLength('pledge_vendor', 100)
            ->allowEmpty('pledge_vendor');

        $validator
            ->decimal('pledge_amount')
            ->allowEmpty('pledge_amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));

        return $rules;
    }
}
