<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * Receipts Model
 *
 * @property \App\Model\Table\AccountsTable|\Cake\ORM\Association\BelongsTo $Accounts
 *
 * @method \App\Model\Entity\Receipt get($primaryKey, $options = [])
 * @method \App\Model\Entity\Receipt newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Receipt[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Receipt|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Receipt|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Receipt patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Receipt[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Receipt findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReceiptsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('receipts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id'
        ]);

        $this->belongsTo('CreatedUser', [
                'className' => 'Users'
            ])
            ->setForeignKey('created_by')
            ->setProperty('created_user');

        $this->belongsTo('ModifiedUser', [
                'className' => 'Users'
            ])
            ->setForeignKey('modified_by')
            ->setProperty('modified_user');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('account_id');

        $validator
            ->date('pdate')
            ->requirePresence('pdate', 'create')
            ->notEmpty('pdate');

        $validator
            ->integer('duration')
            ->allowEmpty('duration');

        $validator
            ->scalar('payment_type')
            ->notEmpty('payment_type');

        $validator
            ->decimal('interest')
            ->allowEmpty('interest');

        $validator
            ->decimal('principal')
            ->allowEmpty('principal');

        $validator
            ->decimal('amount')
            ->notEmpty('amount');

        $validator->add('amount', 'custom', [
            'rule' => function ($value, $context) {
                if (!$value) {
                    return false;
                }

                if ($value <= 0) {
                    return 'Amount should be greater than 0.';
                }

                return true;
            },
            'message' => 'Invalid Amount.'
        ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options) {
        $userId = isset($_SESSION['Auth']['User']['id']) ? $_SESSION['Auth']['User']['id'] : 0;
        if($entity->isNew()) {
            $entity->created_by = $userId;
        } else {
            $entity->modified_by = $userId;
        }
    }

    public function afterSave($event, $entity, $options) {
        $account_model = TableRegistry::get('Accounts');
        $account = $account_model->get($entity->account_id);
        if($account) {
            if($entity->payment_type == 'settlement') {
                $account->status = 'closed';
                $account_model->save($account);
            }
            if($entity->payment_type == 'extra') {
                $account->amount += (float) $entity->amount;
                $account_model->save($account);
            }
        }
    }
}
